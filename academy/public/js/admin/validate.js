$(document).ready(function(){
	$(".disable").click(function(e){
			alert("The domain of this user is inactive");
			e.preventDefault();
		});
	$("#ContractAdminViewForm").submit(function(){
		var val = $("#ContractAdminstatus").val();
		if ( val != "None") {
			if ( confirm("Do you really mark the dispute as "+ val) ) {
			} else {
				return false;
			}
		}
	});
	
	/* remove flash message when we retype password input*/
	$('#AdminCurrentpassword').change(function(){
		$('#flashMessage').hide();
	});
	
	$('#AdminCurrentpassword,#AdminNewpassword,#AdminConfirmpassword').bind("cut copy paste",function(e) {
	  e.preventDefault();
	});
	
	/* 
	 * @below block of code is used to validate admin login page
	 * @Error messages EMPTYUSERMESSAGE & EMPTYPASSWORDMESSAGE are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
	$("#AdminLoginForm").validate({
			
		rules:{
			'data[Admin][username]'	:{
				required	:	true,
				email	:	true
			},
			'data[Admin][password]'	:{
				required	: true,
				minlength	: 5
			}
		},
		messages:{
			'data[Admin][username]'	:{
				required	:	EMPTYUSERMESSAGE,
				email		:	VALIDEMAILMESSAGE
			},
			'data[Admin][password]'	:{
				required	:	EMPTYPASSWORDMESSAGE,
				minlength	:	PASSWORD5LENGTHMESSAGE
			}
		}
	});
	
	$("#UserAdminEditForm").validate({
		rules : {
			'data[User][txtpassword]'	:	{
				required	:	false,
				minlength	:	5,
				maxlength	:	15
			},
			'data[User][txtCpassword]'	:	{
				required	:	false,
				equalTo		: 	"#UserTxtpassword"
			}
		},
		messages : {
			'data[User][txtpassword]'	:	{
				required	:	EMPTYNEWPASSWORD,
				minlength	:	PASSWORD5LENGTHMESSAGE,
				maxlength	:	PASSWORDMAX15MESSAGE
			},
			'data[User][txtCpassword]'	:	{
				required	: 	EMPTYCONFIRMPASSWORD,
				equalTo		:	PASSWORDMISMATCHMESSAGE
			}
		}
	});
	
	$("#DisputenoteAdminViewnotesForm").validate({
		rules : {
			'data[Disputenote][notes]' : {
				required : true
			}
		},
		messages : {
			'data[Disputenote][notes]' : {
				required : "Please enter notes"
			}
		}
	});
	
	
	/* end of block */
	
	/* 
	 * @below block of code is used to validate change password page in forgot password process
	 * @Error messages are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
	$("#AdminResetpasswordForm").validate({
		
		rules		:{
					'data[Admin][newpassword]'		:	{
						required	:	true,
						minlength	:	6,
						maxlength	:	15
					},
					'data[Admin][confirmpassword]'	:	{
						required	:	true,
						equalTo		: 	"#AdminNewpassword"
					}
		},
		messages	:{
					'data[Admin][newpassword]'		:	{
						required	:	EMPTYNEWPASSWORD,
						minlength	:	PASSWORD6LENGTHMESSAGE,
						maxlength	:	PASSWORDMAX15MESSAGE
					},
					'data[Admin][confirmpassword]'	:	{
						required	: 	EMPTYCONFIRMPASSWORD,
						equalTo		:	PASSWORDMISMATCHMESSAGE
					}
		}
	});

/*To validate password strength and display strength meter*/
	$.validator.addMethod("passwordStrength", function(password,element) {
		var desc = new Array();
		desc[0] = "Your password is Very Weak";
		desc[1] = "Your password is Weak";
		desc[2] = "Better";
		desc[3] = "Medium";
		desc[4] = "Strong";
		desc[5] = "Strongest";

		var score   = 0;

		//if password bigger than 6 give 1 point
		if (password.length > 6) score++;

		//if password has both lower and uppercase characters give 1 point	
		if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

		//if password has at least one number give 1 point
		if (password.match(/\d+/)) score++;

		//if password has at least one special caracther give 1 point
		if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	score++;

		//if password bigger than 12 give another 1 point
		if (password.length > 12) score++;

		 document.getElementById("passwordStrength").innerHTML = desc[score];
		 document.getElementById("passwordStrength").className = "strength" + score;
		 if(score<=2) return false;
		 return true;
	}, "");
	
	/* 
	 * @below block of code is used to validate change password page in admin panel
	 * @Error messages are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
		
	$("#AdminChangepasswordForm").validate({
		
		rules		:{
					'data[Admin][currentpassword]'	:	{
						required	:	true
					},
					'data[Admin][newpassword]'		:	{
						required	:	true,
					//	passwordStrength : true,
						minlength	:	6,
						maxlength	:	15
					},
					'data[Admin][confirmpassword]'	:	{
						required	:	true,
						equalTo		: 	"#AdminNewpassword"
					}
		},
		messages	:{
					'data[Admin][currentpassword]'	:	{
						required	:	EMPTYOLDPASSWORD,
					},
					'data[Admin][newpassword]'		:	{
						required	:	EMPTYNEWPASSWORD,
						minlength	:	PASSWORD6LENGTHMESSAGE,
						maxlength	:	PASSWORDMAX15MESSAGE
					},
					'data[Admin][confirmpassword]'	:	{
						required	: 	EMPTYCONFIRMPASSWORD,
						equalTo		:	PASSWORDMISMATCHMESSAGE
					}
		}
	});
	/* end of block */
	
	
	/* 
	 * @below block of code is used to validate change password page in admin panel
	 * @Error messages are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
	$("#IndustryAdminAddForm,#IndustryAdminEditForm").validate({
			rules	: {
				'data[Industry][heading]' : {
					required : true
				}
			},
			messages: {
				'data[Industry][heading]' : {
					required : "Please enter industry name"
				}
			}
	});
	
	$("#MediaoutletAdminAddForm,#MediaoutletAdminEditForm").validate({
			rules	: {
				'data[Mediaoutlet][heading]' : {
					required : true
				}
			},
			messages: {
				'data[Mediaoutlet][heading]' : {
					required : "Please enter heading"
				}
			}
	});
	/* end of block */
	
	/* 
	 * @below block of code is used to validate change password page in admin panel
	 * @Error messages are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
	$("#AdminAddForm").validate({
		
		rules		:{
					'data[Admin][username]'	:	{
						required	:	true,
						email		:	true
					},
					'data[Admin][password]'		:	{
						required	:	true,
						minlength	:	5,
						maxlength	:	15
					},
					'data[Admin][domain]'		:	{
						required	:	true
					},
					'data[Admin][confirm password]': {
						required	:	true,
						equalTo		: 	"#AdminPassword"
					}
		},
		messages	:{
					'data[Admin][username]'	:	{
						required	:	EMPTYUSERMESSAGE,
						email		:	VALIDEMAILMESSAGE
					},
					'data[Admin][password]'		:	{
						required	:	EMPTYPASSWORDMESSAGE,
						minlength	:	PASSWORD5LENGTHMESSAGE,
						maxlength	:	PASSWORDMAX15MESSAGE
					},
					'data[Admin][domain]'	:	{
						required	: 	'Enter domain',
					},
					'data[Admin][confirm password]': {
						required	: 	EMPTYCONFIRMPASSWORD,
						equalTo		:	PASSWORDMISMATCHENTER
					}
		}
	});
	/* end of block */
	
	/* 
	 * @below block of code is used to validate change password page in admin panel
	 * @Error messages are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
	$("#AdminEditForm").validate({
		
		rules		:{
					'data[Admin][username]'	:	{
						required	:	true,
						email		:	true
					},
					'data[Admin][domain]'		:	{
						required	:	true
					}
		},
		messages	:{
					'data[Admin][username]'	:	{
						required	:	EMPTYUSERMESSAGE,
						email		:	VALIDEMAILMESSAGE
					},
					'data[Admin][domain]'	:	{
						required	: 	'Enter domain',
					}
		}
	});
	/* end of block */
	
	
	$('.email').change(function(){
		$(this).val( $.trim($(this).val())) ;
	});
	/* 
	 * @below block of code is used to validate forgot password page in admin panel
	 * @Error messages are defined in validationmessages.js file in app/webroot/js
	 * @common function removeSpaces is declared and defined in common_functions.js file in app/webroot/js
	*/
	$("#AdminForgotpasswordForm").validate({
		rules:{
			'data[Admin][email]'	:{
				required	:	true,
				email		:	true
			}
		},
		messages:{
			'data[Admin][email]'	:{
				required	:	EMPTYEMAILMESSAGE,
				email		:	VALIDEMAILMESSAGE
			}
		}
	});
	
	/* end of block */
	
	/*below block of code is used to validate page form while adding and updating */
	$("#CmspageAdminAddForm,#CmspageAdminEditForm").validate({
		rules		:{
					'data[Cmspage][name]'		:	{
						required	:	true
					},
					'data[Cmspage][metatitle]'	:	{
						required	:	true
					},
					'data[Cmspage][seourl]'	:	{
						required	:	true
					},
					'data[Cmspage][metadesc]'	:	{
						required	:	true
					},
					'data[Cmspage][metakeyword]'	:	{
						required	:	true
					}
		},
		messages	:{
					'data[Cmspage][name]'	:	{
						required	:	PAGETITLEERRMESSAGE
					},
					'data[Cmspage][metatitle]'	:	{
						required	:	PAGESEOTITLERRMESSAGE
					},
					'data[Cmspage][seourl]'	:	{
						required	:	PAGESEOURLERRMESSAGE
					},
					'data[Cmspage][metadesc]'	:	{
						required	:	PAGEMETADESCERRMESSAGE
					},
					'data[Cmspage][metakeyword]'	:	{
						required	:	PAGEMETAKEYERRMESSAGE
					}
		}
	});
	/* end here */
	
	
	
	/*below block of code is used to validate email template form while adding and updating */
	$("#CmsemailAdminAddForm,#CmsemailAdminEditForm").validate({
		
		rules		:{
					'data[Cmsemail][mailfrom]'		:	{
						required	: true,
						email		: true
					},
					'data[Cmsemail][mailsubject]'		:	{
						required	:	true
					}					
		},
		messages	:{
					'data[Cmsemail][mailfrom]'		:	{
						required	:	EMPTYEMAILMESSAGE,
						email		:	VALIDEMAILMESSAGE
					},
					'data[Cmsemail][mailsubject]'		:	{
						required	:	EMPTYSUBJECTMESSAGE
					}					
		}
	});
	/* end here */
	
	/* for toggeling the left menus in admin panel */
	
	$(".loc").click(function(e){
		$(".hide").slideUp("slow");
		var val =removeSpaces($(this).next(".sublist-menu1").attr("style"));
		if(val == 'display:block;'){
			$(this).next(".sublist-menu1").slideUp("slow");
		} else if(val == '') {
			$(this).next(".sublist-menu1").slideUp("slow");
		} else {
			$(".sublist-menu1").slideUp("slow");
			$(this).next(".sublist-menu1").slideDown("slow");
		}
	});
	/*$(".loc1").click(function(e){
		$(".hide").slideUp("slow");
		var val =removeSpaces($(".sublist-menu1").attr("style"));
		if(val == 'display:block;'){
			$(".sublist-menu1").slideUp("slow");
		} else if(val == '') {
			$(".sublist-menu1").slideUp("slow");
		} else {
			$(".sublist-menu1").slideDown("slow");
		}
	});*/
	$(".admintoggel").click(function(){
		$(".hide0").slideUp("slow");
		var clas = $(this).parent("ul").attr("class");
		if(clas != 'sublist-menu1'){
			$(".sublist-menu1").slideUp("slow");
		}
		if ((removeSpaces($(this).next().attr("style"))) == 'display:none;') {
			($(this).next()).slideDown("slow");
		} else if ((removeSpaces($(this).next().attr("style"))) == 'display:block;'){
			($(this).next()).slideDown("slow");
		}
		
	});
	
	$(".hide").hide();
	$(".hide0").hide();
	$(".hide1").addClass("hide0");
	
	
	/* end here */
	
	
	
	/*below block of code is used to validate country form while adding and updating */
	
	$("#CountryAdminAddForm,#CountryAdminEditForm").validate({
		
		rules		:{
					'data[Country][name]'		:	{
						required	:	true
					},
					'data[Country][code]'		:	{
						required	:	true
					}
		},
		messages	:{
					'data[Country][name]'		:	{
						required	:	COUNTRYERRMESSAGE
					},
					'data[Country][code]'		:	{
						required	:	COUNTRYCODMESSAGE
					}
		}
	});
	/* end here */
	
	
	/*below block of code is used to validate state form while adding and updating */
	$("#StateAdminAddForm,#StateAdminEditForm").validate({
		
		rules		:{
					'data[State][name]'		:	{
						required	:	true
					},
					'data[State][country_id]':	{
						required	: true
					},
					'data[State][code]':	{
						required	: true
					}
		},
		messages	:{
					'data[State][name]'		:	{
						required	:	STATEERRMESSAGE
					},
					'data[State][country_id]'		:	{
						required	:	STATECOUNTRYERRMESSAGE
					},
					'data[State][code]'		:	{
						required	:	STATECODEMESSAGE
					}
		}
	});
	/* end here */
		
	/*below block of code is used to validate state form while adding and updating */
	$("#CityAdminAddForm,#CityAdminEditForm").validate({
		
		rules		:{
					'data[City][name]'		:	{
						required	:	true
					},
					'data[City][state_id]'		:	{
						required	:	true
					},
					'data[City][code]'		:	{
						required	:	true
					}
		},
		messages	:{
					'data[City][name]'		:	{
						required	:	CITYERRMESSAGE
					},
					'data[City][state_id]'		:	{
						required	:	CITYSTATEMESSAGE
					},
					'data[City][code]'		:	{
						required	:	CITYCODMESSAGE
					}
		}
	});
	/* end here */
	
	/*below block of code is used to validate category form while adding and updating */
	$("#CategoryAdminAddForm,#CategoryAdminEditForm").validate({
		
		rules		:{
					'data[Category][name]'		:	{
						required	:	true
					}
		},
		messages	:{
					'data[Category][name]'		:	{
						required	:	CATEGORYERRMESSAGE
					}
		}
	});
	/* end here */
	
	
	/*below block of code is used to validate subcategory form while adding and updating */
	$("#SubcategoryAdminAddForm,#SubcategoryAdminAddForm").validate({
		
		rules		:{
					'data[Subcategory][name]'		:	{
						required	:	true
					}
		},
		messages	:{
					'data[Subcategory][name]'		:	{
						required	:	CATEGORYERRMESSAGE
					}
		}
	});
	/* end here */
	
	
	/*below block of code is used to validate subcategory form while adding and updating */
	$("#AttributeAdminAddForm,#AttributeAdminEditForm").validate({
		
		rules		:{
					'data[Attribute][subcategory_id]'		:	{
						required	:	true
					},
					'data[Attribute][name]'		:	{
						required	:	true
					}
		},
		messages	:{
					'data[Attribute][subcategory_id]'		:	{
						required	:	'Please add subcategory first.'
					},
					'data[Attribute][name]'		:	{
						required	:	'Please enter attribute.'
					}
		}
	});
	/* end here */
	
	$("#AccountAdminAddForm,#AccountAdminEditForm").validate({
		rules		:{
					'data[Account][domain]'		:	{
						required	:	true
					},
					'data[Account][company]'		:	{
						required	:	true
					},
					'data[Account][company_logo]'		:	{
						require		: false,
						accept		:	'jpeg|jpg|png|gif'
					}
		},
		messages	:{
					'data[Account][domain]'		:	{
						required	:	'Please enter domain'
					},
					'data[Account][company]'		:	{
						required	:	'Please enter company name'
					},
					'data[Account][company_logo]'		:	{
						accept		:	IMAGEVALIDMESSAGE
					}
		}
	});
	
	$("#PackageAdminAddForm,#PackageAdminEditForm").validate({
		rules		:{
					'data[Package][heading]'		:	{
						required	:	true
					},
					'data[Package][price]'		:	{
						required	:	true
					},
					'data[Package][days]'		:	{
						required	: true,
					}
		},
		messages	:{
					'data[Package][heading]'		:	{
						required	:	'Please enter heading'
					},
					'data[Package][price]'		:	{
						required	:	'Please enter price'
					},
					'data[Package][days]'		:	{
						required		:	'Please enter days'
					}
		}
	});
	
	/* below code is to perform  functionality */
	$("#checkall").click(function(){
		
		$(".chk").attr("checked",this.checked);
	});
	
	$(".chk").click(function(){
	
		if($(".chk").length == $(".chk:checked").length){
			
			$("#checkall").attr("checked","checked");
		}else{
			$("#checkall").removeAttr("checked");
		}
	});
	/* end here */
	
	var searchButton 	= '';
	
	$(".submitsearch").click(function() {
		
		searchButton = $(this).attr('attr');
		
	});
	
	$(document.body).on("keypress","#PackagePrice,#PackageDays",function(e){
		var AllowableCharacters='1234567890.';
		var k = document.all?parseInt(e.keyCode): parseInt(e.which);
		if (k!=13 && k!=8 && k!=0){
			if ((e.ctrlKey==false) && (e.altKey==false)) {
				return (AllowableCharacters.indexOf(String.fromCharCode(k))!=-1);
			} else {
				return true;
			}
		} else {
			return true;
		}
	});
	
	/* below code is to validate checkall functionality for every page on which we perform delete multiple or update multiple functionalioty*/
	$("#CmspageAdminIndexForm,#CmsemailAdminIndexForm,#CountryAdminIndexForm,#StateAdminIndexForm,#CityAdminIndexForm,#UserAdminIndexForm,#CategoryAdminIndexForm,#LanguageAdminIndexForm, #CourseAdminIndexForm,#BackupdbAdminIndexForm,#EnterpriseAdminIndexForm,#UserAdminViewmanagersForm").submit(function(){ if(searchButton == ''){ return validatemultipleaction(); }else{ $(".chk").removeAttr("checked"); return true; } });
	/* end here */
	
	
	
	//$("#btn").click($('body').addClass('folded'));
	
	/* to change country state city */
	
	$(document.body).on("change","#AdmindetailCountry,#AdmindetailState",function(){
		var option = $(this).attr("id");
		option = (option == 'AdmindetailCountry')?'Country':'State';
		$.ajax({
			url: $("#link").attr("href"),
			type: 'post',
			data  : "id="+$(this).val()+"&opt="+option, 
			success: function(data) {
				var cont = (option == 'Country')?'AdmindetailState':'AdmindetailCity';
				var str = (option == 'Country')?'Select Your State':'Select Your City';
				if(cont == 'AdmindetailState'){
					
				}else{
					
				}
				$("#"+cont).children("option").each(function(){
					if($(this).val() != ''){
						$(this).remove();
					}
				});
				$("#"+cont).append(data);
			},
			error : function(err, req) {
				alert("Your browser broke!");
			}
		});
	});
	
	$(document.body).on("click",".add_course_ent",function(){
		var id = $(this).val();
		var entid = $("#EnterpriseCourseEnterpriseId").val();
		
		if(entid.length == 0 ) {
			alert("Please select an enterprise before selecting course.");
			$(this).attr("checked",false);
			$("#EnterpriseCourseEnterpriseId").focus();
		} else if($(this).is(":checked")) {
			if(confirm("Do you really want to add course "+$(this).attr('val')+" to "+$("#EnterpriseCourseEnterpriseId>option:selected").html())) {
				$.ajax({
					url: BASE_URL+"admin/enterprises/addenterprisecourse",
					type: 'post',
					data  : "course_id="+$(this).val()+"&enterprise_id="+$("#EnterpriseCourseEnterpriseId").val(), 
					success: function(data) {
						window.location.reload();
					},
					error : function(err, req) {
						alert("Your browser broke!");
					}
				});
			} else {
				$(this).attr("checked",false);
			}
		} else {
			if(confirm("Do you really want to remove course "+$(this).attr('val')+" to "+$("#EnterpriseCourseEnterpriseId>option:selected").html())) {
				$.ajax({
					url: BASE_URL+"admin/enterprises/addenterprisecourse",
					type: 'post',
					data  : "course_id="+$(this).val()+"&enterprise_id="+$("#EnterpriseCourseEnterpriseId").val()+"&delflag=1", 
					success: function(data) {
						window.location.reload();
					},
					error : function(err, req) {
						alert("Your browser broke!");
					}
				});
			} else {
				$(this).attr("checked",true);
			}
		}
	});

    $(document.body).on("change","#EnterpriseCourseEnterpriseId",function(){
		var url = BASE_URL+"admin/enterprises/managecourse/"+$(this).val();
		location.href = url;
	});

    $(document.body).on("change","#AccountCountry,#AccountState",function(){
		var option = $(this).attr("id");
		option = (option == 'AccountCountry')?'Country':'State';
		$.ajax({
			url: $("#link").attr("href"),
			type: 'post',
			data  : "id="+$(this).val()+"&opt="+option, 
			success: function(data) {
				var cont = (option == 'Country')?'AccountState':'AccountCity';
				var str = (option == 'Country')?'Select Your State':'Select Your City';
				if(cont == 'AdmindetailState'){
					
				}else{
					
				}
				$("#"+cont).children("option").each(function(){
					//if($(this).val() != ''){
						$(this).remove();
					//}
				});
				$("#"+cont).append(data);
			},
			error : function(err, req) {
				alert("Your browser broke!");
			}
		});
	});
	
	/* end here */

    $(document.body).on("change","#ProductCategoryId",function(){
		var option = $(this).attr("id");
		option = 'category';
		$.ajax({
			url: $("#link").attr("href"),
			type: 'post',
			data  : "id="+$(this).val()+"&opt="+option, 
			success: function(data) {
				var cont = "ProductSubcategoryId";
				var str = (option == 'Country')?'Select Your State':'Select Your City';
				if(cont == 'AdmindetailState'){
					
				}else{
					
				}
				$("#"+cont).children("option").each(function(){
					if($(this).val() != ''){
						$(this).remove();
					}
				});
				$("#"+cont).append(data);
			},
			error : function(err, req) {
				alert("Your browser broke!");
			}
		});
	});
	
});

	/*
	 * @function name	: validatemultipleaction
	 * @purpose			: validate if any checkbox checked before changing status or deleting with it also validate if there is any data to be prossesed or not
	 * @arguments		: none
	 * @return			: none 
	 * @created by		: shivam sharma
	 * @created on		: 10th oct 2012
	 * @description		: NA
	*/
	function validatemultipleaction(){
		
		var count		= $(".chk:checked").length;
		var counter		= $(".chk").length;
		var PageOptions	= $(".options").val();
		var appmessage  = " "+count+" records?";
		if(PageOptions == ''){
			$("#checkerr").html(CHECKBLANKERROR);
			$("#checkerr").show();
			$(".options").focus();
			return false;
		}
		
		if(counter < 1){
			$("#checkerr").html(CHECKMULTIPLENONEERROR);
			$("#checkerr").show();
			return false;
		}
		
		if(count < 1){
			$("#checkerr").html(CHECKMULTIPLEERROR);
			$("#checkerr").show();
			return false;
		}
		
		
		
		if(PageOptions == 'Delete'){
			if(confirm(DELETEALERTMESSAGE+appmessage)){
				
			}else{
				return false;
			}
			
		}
		
		if(PageOptions == 'Active'){
			if(confirm(__n(ACTIVEALERTMESSAGE,[count]))){
				
			}else{
				return false;
			}
			
		}
		
		if(PageOptions == 'Inactive'){
			
			if(confirm(__n(INACTIVEALERTMESSAGE,[count]))){
				
			}else{
				return false;
			}
			
		}
	}
	/*end here*/
	
	function __n(string,data){
		$.each(data,function(k,v){
			string = string.replace('%s',v);
	    });
	    return string ;
	}
	/*
	 * @function name	: hidepanel
	 * @purpose			: show and hide right panel in admin module
	 * @arguments		: none
	 * @return			: none 
	 * @created by		: shivam sharma
	 * @created on		: 15th oct 2012
	 * @description		: NA
	*/
	function hidepanel(){
		if($("body").attr('class') == 'folded'){
			$("#btn").attr("title","Click here to hide panel");
			$("body").removeClass("folded");
		}else{
			$("#btn").attr("title","Click here to show panel");
			$("body").addClass("folded");
		}
	}
	/*end here*/
	
	
	
