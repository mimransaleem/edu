<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('language_id')->nullable();
            $table->string('name', 255)->nullable();
            $table->longText('content')->nullable(false);
            $table->string('metatitle', 255)->nullable();
            $table->string('seourl', 255)->nullable();
            $table->text('metadesc')->nullable();
            $table->text('metakeyword')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('showinfooter')->default(false);
            $table->boolean('showinleft')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pages');
    }
}
