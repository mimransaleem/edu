<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',3)->nullable(false)->unique();
            $table->string('iso',2)->nullable(false)->unique();
            $table->string('title',255)->nullable(false)->unique();
            $table->enum('dir',['ltr','rtl'])->default('ltr');
            $table->tinyInteger('status')->default(0);
            $table->string('dateformat',50)->default(0);
            $table->string('timeformat',50)->default(0);
            $table->string('datetimeformat',50)->default(0);
            $table->integer('lang_order')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::connection()->getPdo()->exec(<<<EOF
INSERT INTO `languages` VALUES (1,'ara','ar','العربية','rtl',1,'d-m-Y','HH:MM','m-d-Y HH:MM',2,'2016-01-20 13:38:08','2016-01-29 11:20:04',NULL);
INSERT INTO `languages` VALUES (2,'eng','en','English','ltr',1,'m-d-Y','HH:MM','d-m-Y HH:MM',1,'2016-01-20 13:38:09','2017-12-04 15:34:57',NULL);
INSERT INTO `languages` VALUES (3,'urd','ur','اُردُو','rtl',1,'m-d-Y','HH:MM','d-m-Y HH:MM',3,NULL,'2018-05-04 13:01:14',NULL);
EOF
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
