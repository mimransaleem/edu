<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CmsPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            ['Page Name', 'Page Content', 'page_title'],
            ['Page Name 2', 'Page Content 2', 'page_title2'],
        ];

        foreach ($pages as $page) {

            \App\CmsPage::updateOrCreate(
                [
                    'name' => $page[0]
                ],
                [
                    'name' => $page[0],
                    'content' => $page[1],
                    'metatitle' => $page[2],
                    'seourl' => '',
                    'metadesc' => $page[2],
                    'metakeyword' => '',

                    'status' => 1,
                    'showinfooter' => 0,
                    'showinleft' => 0,
                ]);
        }
    }
}
