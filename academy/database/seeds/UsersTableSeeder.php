<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            'raheeq@nadrus.com',
            'bashar@nadrus.com',
            'mohammed@nadrus.com',
            'content@nadrus.com',
            'zaher@nadrus.com',
            'nareg@nadrus.com',
            'hisham@nadrus.com',
            'ahmad@nadrus.com'
        ];

        foreach ($users as $user) {

            DB::table('users')->updateOrInsert(
                [
                    'name' => $user,
                    'email' => $user
                ],
                [
                    'name' => $user,
                    'email' => $user,
                    'password' => bcrypt('123456'),
                    'created_at' => now()
                ]);
        }
    }
}
