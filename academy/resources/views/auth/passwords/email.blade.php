@extends('layouts.login')

@section('content')
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="col-md-5 order-md-1 col-12 order-1 bg-white align-items-stretch">
                <div class="row h-100 justify-content-center align-items-center">
                    <div class="col-12 col-sm-12 col-md-8">
                        <div class="text-center mb-4">
                            <img title="<?php echo __('SITE_NAME'); ?>" src="{{ asset('images/nadrus_logo.png') }}"/>
                        </div>
                        <div class="card card-light my-3 rounded-0">
                            <div class="card-header">
                                <h5 class="mb-0"><?php echo __('labels.FORGOT_YOUR_PASSWORD?'); ?></h5>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <div class="form-group">

                                        <label for="AdminUsername"> {{ __('labels.EMAIL') }}</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block grediant-overlay">
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </form>
                                <hr>
                                <a href="{{ route('login') }}">LOGIN</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 order-md-2 col-12 order-2 px-0 d-none d-md-block align-items-stretch">
                <div class="access-bg grediant-overlay text-white w-100 h-100">
                    <div class="col-lg-10">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('vendors/jquery.backstretch/jquery.backstretch.min.js') }}"></script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            $(".access-bg").backstretch(
                [ '/images/bgs/pexels-photo-220326.jpeg' ],
                {fade:1e3,duration:8e3,alignX: 'center',alignY: 'center'}
            );
        })();
    </script>
@endsection
