@extends('layouts.admin')

@section('content')

    <div class="col-12">
        <div class="card card-light border-light my-3 border-0 rounded-0">
            <div class="card-header">
                <h4 class="mb-0"><?php echo __('labels.EDIT_FEATURE_SETTING'); ?></h4>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ @route('feature-settings.update', $featureSetting->id) }}{{ $page ? '?page='.$page : '' }}">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="featureName"><?php echo __('labels.NAME'); ?></label>
                                <input id="featureName" type="text" maxlength="50" class="form-control" readonly name="domain" value="{{ $featureSetting ? $featureSetting->name : old('name') }}" required >

                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="featureDesc"><?php echo __('labels.DESC'); ?></label>
                                <textarea id="featureDesc" type="text" maxlength="7168" class="form-control" readonly name="description" >{{ $featureSetting ? $featureSetting->description : old('description') }}</textarea>

                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="canViewStatus">{{  __('labels.CAN_VIEW') }}</label>
                                <input type="checkbox" id="can_view" name="can_view" value="1" {{ ($featureSetting->can_view == 1) ? 'checked' : '' }}/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="editStatus">{{  __('labels.CAN_EDIT') }}</label>

                                <input type="checkbox" id="can_edit" name="can_edit" value="1" {{ ($featureSetting->can_edit == 1) ? 'checked' : '' }} />
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-10">
                            {{  __('labels.IS_HIDDEN') }}
                            <input type="checkbox" id="is_hidden_for_customer" name="is_hidden_for_customer" value="1" {{ ($featureSetting->is_hidden_for_customer == 1) ? 'checked' : '' }}/>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary">
                        {{ __('labels.SUBMIT') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script >
        $(document).ready(function () {
            $('#can_view').bootstrapToggle();
            $('#can_edit').bootstrapToggle();
            $('#is_hidden_for_customer').bootstrapToggle();
        });
    </script>
@endsection