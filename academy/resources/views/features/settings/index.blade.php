@extends('layouts.admin')

@section('content')

    <div class="heading col-12 bg-section mb-3 py-3">
        <form id="adminSearchForm" method="GET" action="/feature-settings" >
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">
                        {{ __('labels.ADN_FEATURE_SETTINGS') }}
                        {{--<a href="/features/create" class="btn btn-primary btn-sm">{{ __('labels.ADN_ADD') }}</a>--}}
                    </h4>
                </div>
                <div class="col-md-6">
                    {{--<div class="search-box form-inline float-left">
                        <input id="AdminSearchval" type="text" maxlength="50" placeholder="{{ __('labels.ADN_SEARCH_NAME_AND_TEXT') }}"
                               class="form-control form-control-sm mb-2 ml-sm-2 mb-sm-0" name="searchval" value="{{ old('searchval') }}" >

                        <input type="submit" id="submitbtn" class="btn btn-outline-primary btn-sm nomargin submitsearch" attr="search" value="{{ __('labels.ADN_SEARCH') }}">
                    </div>--}}
                </div>
            </div>
        </form>
    </div>

    <form id="adminUsersForm" method="POST" action="/feature-settings/update" >
        @csrf
        <div id="checkstatus" class="col-6 hide">
            <div class="mb-3 form-inline">
                <select name="data[Admin][options]" class="options form-control form-control-sm" style="" id="AdminOptions">
                    <option value="">{{ __('labels.SELECT_ACTION') }}</option>
                    <option value="Active">{{ __('labels.ACTIVE') }}</option>
                    <option value="Delete">{{ __('labels.DELETE') }}</option>
                    <option value="Inactive">{{ __('labels.INACTIVE') }}</option>
                </select>
                <input id="submitbtn" class="submit btn btn-outline-primary btn-sm mb-2 mr-sm-2 mb-sm-0" type="submit" value="{{ __('labels.SUBMIT') }}">
                <label class="error" id="checkerr" style="float: left; margin: -10px 0 0;"></label>
                <div class="clear"></div>
            </div>
        </div>

        <div class="col-12">
            <table class="table table-sm table-hover bg-white">
                <thead class="thead-light">
                <tr>
                    <th scope="col">{{ __('labels.NAME') }}</th>
                    <th scope="col">{{ __('labels.DESC') }}</th>
                    <th scope="col">{{ __('labels.TYPE') }}</th>
                    <th scope="col">{{ __('labels.DEFAULT_VALUE') }}</th>
                    <th scope="col">{{ __('labels.REQUIRED') }}</th>
                    <th scope="col">{{ __('labels.CAN_VIEW') }}</th>
                    <th scope="col">{{ __('labels.CAN_EDIT') }}</th>
                    <th scope="col">{{ __('labels.IS_HIDDEN') }}</th>
                    <th scope="col">{{ __('labels.ADN_ACTIONS') }}</th>
                </tr>
                </thead>
                @foreach ($featureSettings as $setting)
                <tr>

                    <td>{{ $setting->name }}</td>
                    <td>{{ $setting->description }}</td>
                    <td>{{ $setting->type }}</td>
                    <td>{{ $setting->default_value }}</td>
                    <td>{{ @($setting->required) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($setting->can_view) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($setting->can_edit) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($setting->is_hidden_for_customer) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>
                        <a href="{{ route('feature-settings.edit', $setting->id) }}{{ $page ? '?page='.$page : '' }}" class="btn btn-light btn-sm edit-user"
                           title="Edit" ><i class="ion-edit"></i></a>

                    </td>
                </tr>
                @endforeach
            </table>

            <div class="paging">
                {{ $featureSettings->onEachSide(5)->links() }}
            </div>
        </div>
    </form>
    <script >
        (function() {
            $('.delete-user').on('click', function () {
                var confirmMessage = $(this).data('message');
                var id = $(this).data('id');
                if(confirm(confirmMessage)){
                    $('#AdminOptions').find('option[value="Delete"]').attr('selected', 'selected');
                    $('#id'+id).attr('checked', 'checked');
                    $('#adminUsersForm').submit();
                } else {
                    console.log('invalid_data');
                }
            });
        })();

    </script>
@endsection