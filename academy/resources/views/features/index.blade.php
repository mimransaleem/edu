@extends('layouts.admin')

@section('content')

    <div class="heading col-12 bg-section mb-3 py-3">
        <form id="adminSearchForm" method="GET" action="/features" >
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">
                        {{ __('labels.ADN_FEATURES') }}
                        {{--<a href="/features/create" class="btn btn-primary btn-sm">{{ __('labels.ADN_ADD') }}</a>--}}
                    </h4>
                </div>
                <div class="col-md-6">
                    {{--<div class="search-box form-inline float-left">
                        <input id="AdminSearchval" type="text" maxlength="50" placeholder="{{ __('labels.ADN_SEARCH_NAME_AND_TEXT') }}"
                               class="form-control form-control-sm mb-2 ml-sm-2 mb-sm-0" name="searchval" value="{{ old('searchval') }}" >

                        <input type="submit" id="submitbtn" class="btn btn-outline-primary btn-sm nomargin submitsearch" attr="search" value="{{ __('labels.ADN_SEARCH') }}">
                    </div>--}}
                </div>
            </div>
        </form>
    </div>

    <form id="adminUsersForm" method="POST" action="/features/update" >
        @csrf
        <div id="checkstatus" class="col-6 hide">
            <div class="mb-3 form-inline">
                <select name="data[Admin][options]" class="options form-control form-control-sm" style="" id="AdminOptions">
                    <option value="">{{ __('labels.SELECT_ACTION') }}</option>
                    <option value="Active">{{ __('labels.ACTIVE') }}</option>
                    <option value="Delete">{{ __('labels.DELETE') }}</option>
                    <option value="Inactive">{{ __('labels.INACTIVE') }}</option>
                </select>
                <input id="submitbtn" class="submit btn btn-outline-primary btn-sm mb-2 mr-sm-2 mb-sm-0" type="submit" value="{{ __('labels.SUBMIT') }}">
                <label class="error" id="checkerr" style="float: left; margin: -10px 0 0;"></label>
                <div class="clear"></div>
            </div>
        </div>

        <div class="col-12">
            <table class="table table-sm table-hover bg-white">
                <thead class="thead-light">
                <tr>
                    <th scope="col">{{ __('labels.TITLE') }}</th>
                    <th scope="col">{{ __('labels.DESC') }}</th>
                    <th scope="col">{{ __('labels.STATUS') }}</th>
                    <th scope="col">{{ __('labels.ADN_CREATED') }}</th>
                    <th scope="col">{{ __('labels.ADN_ACTIONS') }}</th>
                </tr>
                </thead>
                @foreach ($features as $feature)
                <tr>

                    <td>{{ $feature->name }}</td>
                    <td>{{ $feature->description }}</td>
                    <td>{{ @($feature->status) ? __('labels.ACTIVE') : __('labels.INACTIVE')}}</td>
                    <td>{{ $feature->created_at }}</td>
                    <td>
                        <a href="{{ route('features.edit', $feature->id) }}{{ $page ? '?page='.$page : '' }}" class="btn btn-light btn-sm edit-user"
                           title="Edit" ><i class="ion-edit"></i></a>

                        <a href="{{ route('features.settings', $feature->id) }}" class="btn btn-light btn-sm "
                           title="Setting"
                        ><i class="ion-ios-gear"></i></a>
                    </td>
                </tr>
                @endforeach
            </table>

            <div class="paging">
                {{ $features->onEachSide(5)->links() }}
            </div>
        </div>
    </form>
    <script >
        (function() {
            $('.delete-user').on('click', function () {
                var confirmMessage = $(this).data('message');
                var id = $(this).data('id');
                if(confirm(confirmMessage)){
                    $('#AdminOptions').find('option[value="Delete"]').attr('selected', 'selected');
                    $('#id'+id).attr('checked', 'checked');
                    $('#adminUsersForm').submit();
                } else {
                    console.log('invalid_data');
                }
            });
        })();

    </script>
@endsection