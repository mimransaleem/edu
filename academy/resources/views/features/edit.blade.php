@extends('layouts.admin')

@section('content')

    <div class="col-12">
        <div class="card card-light border-light my-3 border-0 rounded-0">
            <div class="card-header">
                <h4 class="mb-0"><?php echo __('labels.EDIT_FEATURE'); ?></h4>
            </div>
            <div class="card-body">
                <form method="POST" action="/features/{{ $feature->id }}{{ $page ? '?page='.$page : '' }}">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="featureName"><?php echo __('labels.NAME'); ?></label>
                                <input id="featureName" type="text" maxlength="50" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $feature ? $feature->name : old('name') }}" required autocomplete="name" >
                                @error('name')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="featureDesc"><?php echo __('labels.DESC'); ?></label>
                                <textarea id="featureDesc" type="text" maxlength="7168" class="form-control @error('description') is-invalid @enderror" name="description" >{{ $feature ? $feature->description : old('description') }}</textarea>
                                @error('description')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">
                        {{ __('labels.SUBMIT') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection