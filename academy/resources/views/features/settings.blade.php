@extends('layouts.admin')

@section('content')

    <div class="heading col-12 bg-section mb-3 py-3">
        <form id="adminSearchForm" method="GET" action="/features" >
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">
                        {{ __('labels.ADN_FEATURE_SETTINGS') }}
                        {{--<a href="/features/create" class="btn btn-primary btn-sm">{{ __('labels.ADN_ADD') }}</a>--}}
                    </h4>
                </div>
                <div class="col-md-6">
                    {{--<div class="search-box form-inline float-left">
                        <input id="AdminSearchval" type="text" maxlength="50" placeholder="{{ __('labels.ADN_SEARCH_NAME_AND_TEXT') }}"
                               class="form-control form-control-sm mb-2 ml-sm-2 mb-sm-0" name="searchval" value="{{ old('searchval') }}" >

                        <input type="submit" id="submitbtn" class="btn btn-outline-primary btn-sm nomargin submitsearch" attr="search" value="{{ __('labels.ADN_SEARCH') }}">
                    </div>--}}
                </div>
            </div>
        </form>
    </div>

    <form id="adminUsersForm" method="POST" action="/features/update" >
        @csrf
        <div id="checkstatus" class="col-6 hide">
            <div class="mb-3 form-inline">
                <select name="data[Admin][options]" class="options form-control form-control-sm" style="" id="AdminOptions">
                    <option value="">{{ __('labels.SELECT_ACTION') }}</option>
                    <option value="Active">{{ __('labels.ACTIVE') }}</option>
                    <option value="Delete">{{ __('labels.DELETE') }}</option>
                    <option value="Inactive">{{ __('labels.INACTIVE') }}</option>
                </select>
                <input id="submitbtn" class="submit btn btn-outline-primary btn-sm mb-2 mr-sm-2 mb-sm-0" type="submit" value="{{ __('labels.SUBMIT') }}">
                <label class="error" id="checkerr" style="float: left; margin: -10px 0 0;"></label>
                <div class="clear"></div>
            </div>
        </div>

        <div class="col-12">

            <div class="row">
                <div class="col-md-6">
                    <label for="" >{{ __('labels.NAME') }}</label>
                    <div class="form-control">{{ $feature->name }}</div>
                </div>
                <div class="col-md-6"></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="" >{{ __('labels.DESC') }}</label>
                    <div class="form-control height-auto">{{ $feature->description }}</div>
                </div>
            </div>

            <table class="table table-sm table-hover bg-white mt-3">
                <thead class="thead-light">
                <tr>
                    <th scope="col">{{ __('labels.NAME') }}</th>
                    <th scope="col">{{ __('labels.DESC') }}</th>
                    <th scope="col">{{ __('labels.TYPE') }}</th>
                    <th scope="col">{{ __('labels.DEFAULT_VALUE') }}</th>
                    <th scope="col">{{ __('labels.REQUIRED') }}</th>
                    <th scope="col">{{ __('labels.CAN_VIEW') }}</th>
                    <th scope="col">{{ __('labels.CAN_EDIT') }}</th>
                    <th scope="col">{{ __('labels.IS_HIDDEN') }}</th>
                    <th></th>
                </tr>
                </thead>
                @foreach ($settings as $setting)
                <tr>
                    <td>{{ $setting->name }}</td>
                    <td>{{ $setting->description }}</td>
                    <td>{{ $setting->type }}</td>
                    <td>{{ $setting->default_value }}</td>
                    <td>{{ @($setting->required) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($setting->can_view) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($setting->can_edit) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($setting->is_hidden_for_customer) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>
                        <a href="#" class="btn btn-light btn-sm update-feature-setting-modal"
                                data-toggle="modal" data-target="#updateFeatureSettingModal"
                                data-id="{{ $setting->id }}"
                                data-can_view="{{ $setting->can_view }}"
                                data-can_edit="{{ $setting->can_edit }}"
                                data-is_hidden="{{ $setting->is_hidden_for_customer }}"
                        ><i class="ion-edit"></i></a>
                    </td>

                </tr>
                @endforeach
            </table>

            <div class="paging">

            </div>
        </div>
    </form>

    <div class="modal fade" id="updateFeatureSettingModal" tabindex="-1" role="dialog" aria-labelledby="updateFeatureSettingModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php  echo __('labels.UPDATE_FEATURE_SETTING');?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="loading" >Loading...</div>
                    <form id="updateFeatureSettingForm" method="post" action="{{ route('features.update-setting', $feature->id) }}">
                        @csrf
                    <input type="hidden" id="id" name="id" value="" />
                    <div class="row data-rows hide">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="canViewStatus">{{  __('labels.CAN_VIEW') }}</label>
                                <input type="checkbox" id="can_view" name="can_view" value="1"/>
                            </div>
                        </div>
                    </div>
                    <div class="row data-rows hide">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="editStatus">{{  __('labels.CAN_EDIT') }}</label>

                                <input type="checkbox" id="can_edit" name="can_edit" value="1" checked />
                            </div>
                        </div>
                    </div>

                    <div class="row data-rows hide">

                        <div class="col-md-10">
                            {{  __('labels.IS_HIDDEN') }}

                            <input type="checkbox" id="is_hidden_for_customer" name="is_hidden_for_customer" value="1" />

                        </div>

                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light px-4" data-dismiss="modal">
                        <?php  echo __('labels.CANCEL'); ?>
                    </button>&nbsp;
                    <button type="button" class="btn btn-light update-feature-setting">
                        <?php  echo __('labels.SUBMIT');?>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-script')
    <script src="{{ asset('js/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script >
        $(document).ready(function () {
            $('#can_view').bootstrapToggle();
            $('#can_edit').bootstrapToggle();
            $('#is_hidden_for_customer').bootstrapToggle();

            $('.update-feature-setting').on('click', function (e) {
                var id = $('#id').val();
                if (id != '' && id > 0) {
                    $('#updateFeatureSettingForm').submit();
                }
            });

            $('.update-feature-setting-modal').on('click', function (e) {
                var canView = $(this).data("can_view");
                var canEdit = $(this).data("can_edit");
                var isHidden = $(this).data("is_hidden");
                var featureSettingId = $(this).data("id");

                $('#can_view').bootstrapToggle('off');
                $('#can_edit').bootstrapToggle('off');
                $('#is_hidden_for_customer').bootstrapToggle('off');

                if(canView == '1')
                    $('#can_view').bootstrapToggle('on');
                if(canEdit == '1')
                    $('#can_edit').bootstrapToggle('on');

                if(isHidden == '1')
                    $('#is_hidden_for_customer').bootstrapToggle('on');

                $('#id').val(featureSettingId);

                $('.data-rows').attr('style', '');
                $('#loading').attr('style', 'display:none;');
            });
        });

    </script>
@endsection