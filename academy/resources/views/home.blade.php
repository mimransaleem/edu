@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        @if ( isset(Auth::user()->role))
                            You are logged in as

                            @can('isAdmin')
                                {{ __('labels.ADMIN') }}
                            @elsecan('isAuthor')
                                {{ __('labels.AUTHOR') }}
                            @else
                                {{ __('labels.REGISTERED') }}
                            @endcan
                            {{--@foreach(Auth::user()->roles as $role)
                            {{ $role->name }}
                            @endforeach--}}
                            User!
                        @else
                            {{ 'No role assigned' }}
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
