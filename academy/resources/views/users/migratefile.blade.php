@extends('layouts.admin')

@section('content')

    <form id="adminUsersForm" method="GET" action="" >
        @csrf
        <div class="col-12">
            <div class="card card-light border-light my-3 border-0 rounded-0">
                <div class="card-header">
                    <h4 class="mb-0"><?php echo __('labels.ADN_MIGRATION');?></h4>
                </div>
                <div class="card-body">
                    <code>
                        @if ($command)
                        {!! "<strong>".__('labels.ADN_CONFIGURATIONS')."</strong><br /> $command <br /><br />" !!}
                        {!! "<strong>".__('labels.ADN_OUTPUT')."</strong>" !!}
                        {!!  implode("<br />",$outputAndErrors) !!}
                        <br><b><a href="{{route('migratefile')}}">Back to Migrations</a></b>
                        @else
                        @foreach ($migrationFiles as $migrationFile=>$mTime)

                            <a href="{{route('migrate-file', $migrationFile)}}"
                               data-toggle="tooltip" data-placement="bottom"
                               title = "Run Migration: {{$migrationFile}}"
                               class="btn btn-default btn-icon-only btn-circle" onclick="return confirm('Are you sure want to Migrate This File: {{ $migrationFile }}')"
                            ><i class="fa fa-database"></i></a>
                        {!! date("Y/m/d H:i:s",$mTime). " ----- <b>".$migrationFile."</b>" !!}
                        <br>
                        @endforeach
                        @endif
                    </code>
                </div>
            </div>
        </div>
    </form>
    <script >
        (function() {
            $('.delete-user').on('click', function () {
                var confirmMessage = $(this).data('message');
                var id = $(this).data('id');
                if(confirm(confirmMessage)){
                    $('#AdminOptions').find('option[value="Delete"]').attr('selected', 'selected');
                    $('#id'+id).attr('checked', 'checked');
                    $('#adminUsersForm').submit();
                } else {
                    console.log('invalid_data');
                }
            });
        })();

    </script>
@endsection
