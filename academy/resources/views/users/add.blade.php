@extends('layouts.admin')

@section('content')

    <div class="col-12">
        <div class="card card-light border-light my-3 border-0 rounded-0">
            <div class="card-header">
                <h4 class="mb-0"><?php echo __('labels.ADD_USER'); ?></h4>
            </div>
            <div class="card-body">
                <form method="POST" action="/users">
                    @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="UserdetailFirstName"><?php echo __('labels.FIRST_NAME'); ?></label>
                            <input id="UserdetailFirstName" type="text" maxlength="50" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" >
                            @error('name')
                            <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="UserdetailLastName"><?php echo __('labels.LAST_NAME'); ?></label>
                            <input id="UserdetailLastName" type="text" maxlength="50" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" autocomplete="last_name" >
                            @error('last_name')
                            <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="UserUsername"><?php echo __('labels.USERNAME'); ?></label>
                            <input id="UserUsername" type="email" maxlength="150" required="required" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                            @error('email')
                            <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="UserPassword"><?php echo __('labels.PASSWORD'); ?></label>
                            <input id="UserPassword" type="password" maxlength="15" required="required" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">
                    {{ __('labels.SUBMIT') }}
                </button>

                </form>
            </div>
        </div>
    </div>
@endsection