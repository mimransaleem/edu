@extends('layouts.admin')

@section('content')
<div class="col-12">
    <div class="card card-light border-light my-3 border-0 rounded-0">
        <div class="card-header">
            <h4 class="mb-0"><?php echo __('labels.CHANGE_PASSWORD'); ?></h4>
        </div>
        <div class="card-body">
            <form id="Admin" method="POST" action="/users">
                @csrf
            <div class="form-group">
                <label for="AdminCurrentpassword"><?php echo __('labels.ADN_CURRENT_PASSWORD'); ?></label>
                <input id="AdminCurrentpassword" type="password" maxlength="50" class="form-control form-control-lg @error('currentpassword') is-invalid @enderror" name="currentpassword" value="{{ old('currentpassword') }}" required autocomplete="currentpassword" autofocus >
            </div>

            <div class="form-group">
                <label for="AdminNewpassword"><?php echo __('labels.NEW_PASSWORD'); ?></label>
                <input id="AdminNewpassword" type="password" maxlength="50" class="form-control form-control-lg @error('newpassword') is-invalid @enderror" name="newpassword" value="{{ old('newpassword') }}" required autocomplete="newpassword" />
            </div>

            <div class="form-group">
                <label for="AdminConfirmpassword"><?php echo __('labels.CONFIRM_PASSWORD'); ?></label>
                <input id="AdminConfirmpassword" type="password" maxlength="50" class="form-control form-control-lg @error('confirmpassword') is-invalid @enderror" name="confirmpassword" value="{{ old('confirmpassword') }}" required autocomplete="confirmpassword" />
            </div>

            <div class="form-group">
                <input type="hidden" name="id" value="{{ @Auth()->user()->id }}">

                <button type="submit" class="btn btn-primary btn-lg">
                    {{ __('labels.SUBMIT') }}
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('page-script')
    <script >
        $(document).ready(function () {

            /* code to diable copy paste in change password */
            $('#UserCurrentpassword,#UserNewpassword').bind("cut copy paste",function(e) {
            e.preventDefault();
            });
            /* end here */

        });
    </script>
@endsection