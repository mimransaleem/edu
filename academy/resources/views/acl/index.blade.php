@extends('layouts.admin')

@section('content')
    <?php if ( Auth::user() ) { ?>
    <div id="cohort-listing">

        <div>
            <div class="heading col-12 bg-section mb-3 py-3">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-0"><?php echo __('labels.ADD_ROLE'); ?></h4>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <ul class="nav nav-tabs" id="aclmanage-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="newrole-tab" data-toggle="tab" href="#newrole" role="tab" aria-controls="newrole" aria-selected="true">
                            <?php echo __('labels.ROLES'); ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="newability-tab" data-toggle="tab" href="#newability" role="tab" aria-controls="newability" aria-selected="false">
                            <?php echo __('labels.PRIVILEGES'); ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="new-role-ability-tab" data-toggle="tab" href="#new-role-ability" role="tab" aria-controls="new-role-ability" aria-selected="false">
                            <?php echo __('labels.ASSIGN_PRIVILEGES_TO_ROLES'); ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="new-user-role-tab" data-toggle="tab" href="#new-user-role" role="tab" aria-controls="new-user-role" aria-selected="false">
                            <?php echo __('labels.ASSIGN_ROLES_TO_ADMINS'); ?>
                        </a>
                    </li>
                </ul>

                <div id="loading-div"></div>
                <div id="add-role-error-div" class="d-none bg-white pt-3 px-3 pb-1 border border-top-0 border-bottom-0">
                    <div class="alert alert-danger d-flex">
                        <i class="fa fa-times-circle fa-lg mt-1"></i>
                        <ul class="mb-1 w-100"></ul>
                    </div>
                </div>

                <div class="tab-content" id="newcoht-tabs-panes">

                    @include('acl.role_acl_partial')
                    @include('acl.ability_acl_partial')
                    @include('acl.role_ability_acl_partial')
                    @include('acl.user_role_acl_partial')

                </div>

            </div>

        </div>
    </div>
    <div class="modal fade" id="confirmDelRoleAbilityModal" tabindex="-1" role="dialog" aria-labelledby="confirmDelRoleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php  echo __('labels.DELETE_ROLE_PRIVILEGES');?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning d-flex px-2">
                        <i class="fa fa-info-circle fa-lg py-2 px-1"></i>
                        <span><?php  echo __('labels.DELETE_ROLE_PRIVILEGES_CONFIRM_MSG');?> </span>
                        <span id="selected_ability" ></span>
                        <input type="hidden" id="rolename" value="0" />
                        <input type="hidden" id="abilityname" value="0" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light px-4" data-dismiss="modal">
                        <?php  echo __('labels.CANCEL'); ?>
                    </button>
                    <button type="button" class="btn btn-light remove-ability">
                        <?php  echo __('labels.YES_SURE');?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmDelAdminRoleModal" tabindex="-1" role="dialog" aria-labelledby="confirmDelRoleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php  echo __('labels.DELETE_ADMIN_ROLE');?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning d-flex px-2">
                        <i class="fa fa-info-circle fa-lg py-2 px-1"></i>
                        <span><?php  echo __('labels.DELETE_ADMIN_ROLE_CONFIRM_MSG');?> </span>
                        <span id="selected_rolename" ></span>
                        <input type="hidden" id="adminrole" value="0" />
                        <input type="hidden" id="adminid" value="0" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light px-4" data-dismiss="modal">
                        <?php  echo __('labels.CANCEL'); ?>
                    </button>
                    <button type="button" class="btn btn-light remove-role">
                        <?php  echo __('labels.YES_SURE');?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="{{ asset('control/vendors/chosen/chosen.css') }}">
@endsection

@section('page-script')
    <script src="{{ asset('control/vendors/chosen/chosen.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".chosen-select").chosen();

            $("#select-abilities").next().css( "width", "50%" );

            $('.nav-tabs a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            });

            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
            }

            $('.save-role').on('click',function (e) {

                if($(this).data('roleid'))
                    EditRole($(this).data('roleid'));
                else
                    AddRole();

                return false;
            });

            $('.remove-ability-modal').on('click', function (e) {
                var rolename = $(this).data("rolename");
                var abilityname = $(this).data("abilityname");
                var abilityLabel = $(this).data("abilitylabel");
                $('#rolename').val(rolename);
                $('#abilityname').val(abilityname);
                $('#selected_ability').html(abilityLabel);
            })

            $('.remove-ability').on('click',function (e) {
                e.preventDefault();
                RemoveRoleAbility(
                    $('#rolename').val(),
                    $('#abilityname').val()
                );
                return false;
            });


            $('.remove-role-modal').on('click', function (e) {
                var rolename = $(this).data("rolename");
                var adminId = $(this).data("adminid");
                $('#adminrole').val(rolename);
                $('#adminid').val(adminId);
                $('#selected_rolename').html(rolename);
            })

            $('.remove-role').on('click',function (e) {
                e.preventDefault();
                RemoveUserRole(
                    $('#adminrole').val(),
                    $('#adminid').val()
                );
                return false;
            });

            $('.save-role-ability').on('click',function (e) {
                AddRoleAbility();
                return false;
            });

            $('.save-user-role').on('click',function (e) {
                AddUserRole();
                return false;
            });

            $('.del-modal').on('click', function (e) {
                var rolename = $(this).data("rolename");
                $('#role_id').val($(this).attr("id"));
                $('#selected_role').html(rolename);
            })

            $('.role-btn-del').on('click',function (e) {
                e.preventDefault();
                var roleId = $('#role_id').val();
                RemoveRole(roleId);
            });

            $('.role-btn-edit').on('click',function (e) {
                e.preventDefault();
                var roleId = $(this).attr("id");
                $("#name").val($.trim($('#name-'+roleId).text()));
                $("#title").val($.trim($('#title-'+roleId).text()));
                $("#slug").val($.trim($('#slug-'+roleId).text()));
                $(".save-role")
                    .html('<?php echo __('labels.ADN_EDIT')?>')
                    .attr('data-roleid', roleId);
                $('#reset').removeClass('fade');
            });

            $('#reset').on('click',function (e) {
                e.preventDefault();
                $("#name").val('');
                $("#title").val('');
                $("#slug").val('');
                $(this).addClass('fade');
                $('.save-role')
                    .html('<?php echo __('labels.ADD')?>')
                    .removeAttr('data-roleid');
                return false;
            });

        });

        function RemoveRole(roleId) {
            $.ajax({
                url: BASE_URL+"acl/removeRole",
                type: "POST",
                data: {
                    'id' : roleId
                },
                
                success: function(data) {
                    location.reload();
                },
                error : function(err) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function AddUserRole() {

            $('#add-role-error-div .alert-danger ul').empty();

            var adminId = $('#adminId').val();
            var role = $('#adminRoleName').val();
            if(adminId == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SELECT_ADMIN')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }
            if(role == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SELECT_ROLE')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }

            $.ajax({
                url: BASE_URL+"acl/addUserRole",
                type: "POST",
                data: {
                    'role' : role,
                    'adminId' : adminId
                },
                
                success: function(data) {
                    location.reload();
                },
                error : function(err) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function AddRoleAbility() {

            $('#add-role-error-div .alert-danger ul').empty();

            var abilities = $("#select-abilities").val();

            if(abilities.length <= 0){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SELECT_ABILITY')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }

            var role = $('#roleName').val();
            if(role == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SELECT_ROLE')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }

            $.ajax({
                url: BASE_URL+"acl/addRoleAbilities",
                type: "POST",
                data: {
                    'role' : role,
                    'abilities' : abilities
                },
                
                success: function(data) {
                    location.reload();
                },
                error : function(err) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function AddRole() {

            $('#add-role-error-div .alert-danger ul').empty();

            var name = $("#name").val();
            var title = $("#title").val();
            var slug = $("#slug").val();

            if(name == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.ROLE_NAME')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }
            if(title == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.ROLE_DESCRIPTION')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }
            if(slug == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SLUG_ERROR')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }

            $.ajax({
                url: BASE_URL+"acl/addRole",
                type: "POST",
                data: {
                    'name' : name,
                    'title' : title,
                    'slug' : slug,
                    // '_token' : $('input[name="_token"]').val()
                },
                success: function(result) {

                    location.reload();
                },
                error : function(err) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function EditRole(roleId) {

            $('#add-role-error-div .alert-danger ul').empty();

            var name = $("#name").val();
            var title = $("#title").val();
            var slug = $("#slug").val();

            if(name == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.ROLE_NAME')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }
            if(title == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.ROLE_DESC')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }
            if(slug == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SLUG_ERROR')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }


            $.ajax({
                url: BASE_URL+"acl/editRole",
                type: "POST",
                data: {
                    '_method' : 'PATCH',
                    'name' : name,
                    'title' : title,
                    'slug' : slug,
                    'id' : roleId,
                },
                dataType: 'json',
                success: function(data) {

                    location.reload();
                },
                error : function(err) {

                    showErrorMessage(err);
                }
            });
            return false;
        }

        function RemoveRoleAbility(roleName,abilityName) {

            $('#add-role-error-div .alert-danger ul').empty();

            $.ajax({
                url: BASE_URL+"acl/removeRoleAbility",
                type: "POST",
                data: {
                    'roleName' : roleName,
                    'abilityName' : abilityName
                },
                
                success: function(data) {

                        location.reload();
                },
                error : function(err, req) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function RemoveUserRole(roleName,adminId) {

            $('#add-role-error-div .alert-danger ul').empty();

            $.ajax({
                url: BASE_URL+"acl/removeUserRole",
                type: "POST",
                data: {
                    'role' : roleName,
                    'adminId' : adminId
                },
                
                success: function(data) {
                    location.reload();
                },
                error : function(err) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function showErrorMessage(response) {
            $errors = response.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><ul>';

            $.each( $errors.errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>';
            });
            errorsHtml += '</ul></div>';

            $('#add-role-error-div').html( errorsHtml );
            $('#add-role-error-div').addClass('d-block').removeClass('d-none');

        }

    </script>

    <?php } else {

        /*echo $this->element('../Errors/permission_denied',
            [
                'message' => __d('messages','NO_PERMISSION_TO_ACCESS')
            ]);*/

    } ?>

    <script>
        $(document).ready(function () {
            $("#aclmanage-tabs .nav-link").click(function(){
                setTimeout(function(){
                    window.scrollTo(0,0);
                }, 0);
            });

        });
    </script>
@endsection