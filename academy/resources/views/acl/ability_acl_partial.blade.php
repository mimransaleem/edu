<div class="tab-pane" id="newability" role="tabpanel" aria-labelledby="newrole-tab">
    <form id="form-newcoht-basics" class="row" method="post" action="#">
        <div class="col-sm-12">
            <div class="form-group col">
                <table class="table table-sm table-striped table-hover bg-white td1-fluid mt-3">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col"><?php echo __('labels.PRIVILEGE_TITLE'); ?></th>
                        <th scope="col"><?php echo __('labels.ADN_DESC'); ?></th>
                        <th scope="col"><?php echo __('labels.SLUG_NAME'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($abilities as $ability):?>
                    <tr>
                        <td><?php echo __($ability->name)?></td>
                        <td class="text-light"><?php echo $ability->title?></td>
                        <td class="text-light"><?php echo str_replace(' ','-',strtolower($ability->title)) ?></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>