<div class="tab-pane" id="new-user-role" role="tabpanel" aria-labelledby="new-user-role-tab">
    <form method="post" action="#">
        <div class="p-3">
            <div class="form-row mxw-lg">
                <div class="form-group col">
                    <label><b><?php echo __('labels.THE_ADMIN'); ?></b></label>
                    <select class="form-control" id="adminId">
                        <?php foreach($admins as $admin):?>
                        <option value="<?php echo $admin['id']?>">
                            <?php echo $admin['email']?>
                        </option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group col">
                    <label><b><?php echo __('labels.ROLE'); ?></b></label>
                    <select class="form-control" id="adminRoleName">
                        <?php foreach($roles as $role):?>
                        <option value="<?php echo $role['name']?>">
                            <?php echo $role->name ?>
                        </option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="invisible d-block"> - </label>
                    <button type="submit" class="btn btn-primary save-user-role px-4">
                        <?php echo __('labels.SAVE'); ?>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card border border-dark">
                <div class="card-header bg-dark text-white">
                    <h5 class="mb-0"><?php echo __('labels.ADMIN_ROLES'); ?></h5>
                </div>
                <div class="card-body py-0">
                    <div class="row">
                        <table class="table table-sm td1-fluid mb-1 mt-1">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col"><?php echo __('labels.THE_ADMIN'); ?></th>
                                <th scope="col"><?php echo __('labels.ROLES'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($admins as $admin):?>
                            <tr>
                                <td>
                                    <h5 class="px-3 py-2"><?php echo $admin->email?></h5>
                                </td>
                                <td class="pt-2 pb-3">
                                    <?php foreach ($admin->getRoles() as $role):?>
                                    <span class="badge badge-light py-1 m-1 border">
                                        <span class="mx-1"><?php echo $role ?></span>
                                        <button type="button"
                                                data-toggle="modal" data-target="#confirmDelAdminRoleModal"
                                                data-roleName="<?php echo $role?>"
                                                data-adminId="<?php echo $admin->id?>"
                                                class="btn btn-sm btn-danger text-white border-0 p-0 mb-0 remove-role-modal"
                                                style="width: 20px;margin-bottom: 0 !important;"
                                        >
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                    <?php endforeach;?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>