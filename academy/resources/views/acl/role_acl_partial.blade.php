<div class="tab-pane show active" id="newrole" role="tabpanel" aria-labelledby="newrole-tab">
    <form id="form-newcoht-basics" class="row" method="post" action="#">
        @csrf
        <div class="col-lg-4 pt-3">
            <h5 class="col-12 pt-2 pb-3 mt-2 mb-4 bg-light font-weight-bold">
                <div class="pt-1">
                    <?php echo __('labels.ADD_NEW_ROLE'); ?>
                </div>
            </h5>
            <div class="form-group">
                <div class="form-group col-md-12">
                    <label><?php echo __('labels.ROLE_NAME'); ?></label>
                    <input type="text" class="form-control error-remove" id="name" name="name">
                </div>
                <div class="form-group col-md-12">
                    <label><?php echo __('labels.ROLE_DESCRIPTION'); ?></label>
                    <textarea type="text" class="form-control error-remove" id="title" name="title" rows="3" maxlength="200"></textarea>
                </div>
                <div class="form-group col-md-12">
                    <label><?php echo __('labels.SLUG_NAME'); ?></label>
                    <input type="text" class="form-control error-remove" id="slug" name="slug">
                </div>
            </div>
            <div>
                <div class="form-group col-md-6">
                    <button type="button" class="btn btn-primary save-role"><?php echo __('labels.ADD'); ?></button>
                    <button id="reset" class="btn fade"><?php echo __('labels.CLEAR_FIELDS'); ?></button>
                </div>
            </div>
        </div>

        <div class="col-lg-8">
            <div class="my-4">
                <div class="card border border-dark">
                    <div class="card-header bg-dark text-white">
                        <h5 class="mb-0"><?php echo __('labels.ROLES'); ?></h5>
                    </div>
                    <div class="card-body py-0">
                        <div class="row">
                            <table class="table table-striped mb-1">
                                <thead>
                                <tr>
                                    <th scope="col" style="width: 40%;"><?php echo __('labels.ROLE_NAME'); ?></th>
                                    <th scope="col" style="width: 40%;"><?php echo __('labels.ROLE_DESCRIPTION'); ?></th>
                                    <th scope="col" style="width: 40%;"><?php echo __('labels.SLUG_NAME'); ?></th>
                                    <th scope="col"></th>
                                    <th scope="col" style="width: 20%;"><?php echo __('labels.ACTIONS'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($roles as $role):?>
                                <tr>
                                    <td>
                                        <div id="name-<?php echo $role->id?>">
                                            <?php echo $role->name ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="title-<?php echo $role->id?>">
                                            <?php echo $role->title ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="slug-<?php echo $role->id?>" class="">
                                            <?php echo $role->slug ?>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td class="text-nowrap">
                                        <button type="button" id="<?php echo $role->id?>" class="role-btn-edit btn btn-sm border" data-toggle="tooltip" data-placement="bottom" title="<?php echo __('labels.ADN_EDIT'); ?>" style="width: 35px;">
                                            <i class="ion-edit fa-lg"></i>
                                        </button>
                                        <button type="button" id="<?php echo $role->id?>" data-rolename="<?php echo $role->name?>" class="btn btn-sm btn-danger border-0 del-modal" data-toggle="modal" data-target="#confirmDelRoleModal" data-toggle="tooltip" data-placement="bottom" title="<?php echo __('labels.DEL'); ?>"style="width: 35px;">
                                            <i class="fa fa-times fa-lg"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix" ></div>
</div>

<!-- Modal: Delete Role -->
<div class="modal fade" id="confirmDelRoleModal" tabindex="-1" role="dialog" aria-labelledby="confirmDelRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php  echo __('labels.DELETE_ROLE');?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning d-flex px-2">
                    <i class="fa fa-info-circle fa-lg py-2 px-1"></i>
                    <span><?php  echo __('labels.DELETE_ROLE_CONFIRM_MSG');?> </span>
                    <span id="selected_role" ></span>
                    <input type="hidden" id="role_id" value="0" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light px-4" data-dismiss="modal">
                    <?php  echo __('labels.CANCEL'); ?>
                </button>&nbsp;

                <button type="button" class="btn btn-light role-btn-del">
                    <?php  echo __('labels.YES_SURE');?>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="clearfix" ></div>