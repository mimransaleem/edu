<style>
    .chosen-container-multi {
        width: 100% !important;
    }
    .chosen-container-multi .chosen-choices {
        padding: .375rem .75rem !important;
        border: 1px solid #ced4da !important;
        border-radius: 5px !important;
        box-shadow: 0px 5px 2px white inset !important;
    }
    .chosen-container-multi .chosen-choices li {
        float: right !important;
    }
    .chosen-container-multi .chosen-choices li.search-choice {
        font-size: 10pt !important;
        padding: 6px 20px 7px 12px !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        font-size: 10pt !important;
        font-family: "Cairo",Arial,Tahoma,sans-serif !important;
    }
    .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
        top: 8px !important;
    }
</style>

<div class="tab-pane" id="new-role-ability" role="tabpanel" aria-labelledby="new-role-ability-tab">
    <form method="post" action="#">
        <div class="p-3">
            <div class="form-row mxw-lg">
                <div class="form-group col">
                    <label><b><?php echo __('labels.ROLE'); ?></b></label>
                    <select class="form-control" id="roleName">
                        <?php foreach($roles as $role):?>
                        <option value="<?php echo $role['name']?>">
                            <?php echo $role->name ?>
                        </option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group col">
                    <label><b><?php echo __('labels.PRIVILEGES'); ?></b></label>
                    <select
                            id="select-abilities"
                            data-placeholder="<?php echo __('labels.SELECT_ABILITIES'); ?>"
                            class="chosen-select"
                            multiple style="width:100% !important;"
                    >
                        <?php foreach($abilities as $ability):?>
                        <option value="<?php echo $ability['name']?>">
                            <?php echo __($ability['name'])?>
                        </option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="invisible d-block"> - </label>
                    <button type="submit" class="btn btn-primary save-role-ability px-4">
                        <?php echo __('labels.SAVE'); ?>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card border border-dark">
                <div class="card-header bg-dark text-white">
                    <h5 class="mb-0"><?php echo __('labels.ROLE_PRIVILEGES'); ?></h5>
                </div>
                <div class="card-body py-0">
                    <div class="row">
                        <table class="table table-sm td1-fluid mb-1 mt-1">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col"><?php echo __('labels.ROLE'); ?></th>
                                <th scope="col"><?php echo __('labels.PRIVILEGES'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($roles as $role):?>
                            <tr>
                                <td>
                                    <h5 class="px-3 py-2"><?php echo $role->name ?></h5>
                                </td>
                                <td class="pb-4">
                                    <?php foreach ($role->getAbilities() as $ability){?>
                                    <span class="badge badge-light py-1 m-1 border">
                                        <span class="mx-1"><?php echo __($ability->name)?></span>
                                        <button type="button"
                                                data-toggle="modal" data-target="#confirmDelRoleAbilityModal"
                                                data-roleName="<?php echo $role->name?>"
                                                data-abilityName="<?php echo $ability->name?>"
                                                data-abilityLabel="<?php echo __($ability->name);?>"
                                                class="btn btn-sm btn-danger text-white border-0 p-0 mb-0 remove-ability-modal"
                                                style="width: 20px;margin-bottom: 0 !important;"
                                        >
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                                    <?php }?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $('.remove-ability-modal').bind('click', function(){
            // $(this).html('<img src="/images/spinner.gif" style="width: 18px; height: 19px; border-radius: 2px; margin-top: -3px;">');
            var rolename = $(this).data("rolename");
            var abilityname = $(this).data("abilityname");
            $('#rolename').val(rolename);
            $('#abilityname').val(abilityname);
            $('#selected_ability').html(abilityname);
        });
    });
</script>