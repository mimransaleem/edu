@extends('layouts.admin')

@section('content')

    <div class="col-12">
        <div class="card card-light border-light my-3 border-0 rounded-0">
            <div class="card-header">
                <h4 class="mb-0"><?php echo __('labels.ADN_STATIC_PAGE_ADD'); ?></h4>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('cmspages.store') }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="packageName"><?php echo __('labels.ADN_EDIT_PAGE'); ?></label>
                                <input id="packageName" type="text" maxlength="50" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" >
                                @error('name')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="content"><?php echo __('labels.ADN_CONTENT'); ?></label>
                                <textarea id="content" class="form-control ckeditor @error('content') is-invalid @enderror" name="content" >{{ old('content') }}</textarea>
                                @error('content')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="metatitle"><?php echo __('labels.ADN_META_TITLE'); ?></label>
                                <input id="metatitle" type="text" maxlength="255" class="form-control @error('metatitle') is-invalid @enderror" name="metatitle" value="{{ old('metatitle') != null ? old('metatitle') : '' }}" >
                                @error('metatitle')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="seourl"><?php echo __('labels.ADN_SEO_URL'); ?></label>
                                <input id="seourl" type="text" maxlength="255" class="form-control @error('seourl') is-invalid @enderror" name="seourl" value="{{ old('seourl') != null ? old('seourl') : '' }}" >
                                @error('seourl')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="metadesc"><?php echo __('labels.ADN_MET_DESC'); ?></label>
                                <textarea id="metadesc" rows="2" class="form-control @error('metadesc') is-invalid @enderror" name="metadesc" >{{ old('metadesc') != null ? old('metadesc') : '' }}</textarea>
                                @error('metadesc')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="metakeyword"><?php echo __('labels.ADN_META_KEYWORD'); ?></label>
                                <textarea id="metakeyword" rows="2" class="form-control @error('metakeyword') is-invalid @enderror" name="metakeyword" >{{ old('metakeyword') != null ? old('metakeyword') : '' }}</textarea>
                                @error('metakeyword')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">

                                <label for="CategoryStatus">{{  __('labels.ACTIVE') }}</label>
                                <input type="checkbox" name="status" class="class-checkbox" checked="checked" value="1" id="status">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                                <label for="showinfooter">{{  __('labels.ADN_SHOW_FOOTER') }}</label>
                                <input type="checkbox" name="showinfooter" class="class-checkbox" value="0" id="showinfooter">

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                                <label for="showinleft">{{  __('labels.ADN_SHOW_LEFT_PANEL') }}</label>
                                <input type="checkbox" name="showinleft" class="class-checkbox" value="0" id="showinleft">

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="language_id"><?php echo __('labels.LANGUAGE'); ?></label>
                                {{ Form::select("language_id", $languages, null, ['class' => 'form-control class-select']) }}
                                @error('language_id')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">
                        {{ __('labels.SUBMIT') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('page-script')
    <script src="{{ asset('js/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script >
        $(document).ready(function () {
            $('.class-checkbox').bootstrapToggle();
        });</script>
@endsection
