@extends('layouts.admin')

@section('content')

    <div class="heading col-12 bg-section mb-3 py-3">
        <form id="adminSearchForm" method="GET" action="/cmspages" >
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">
                        {{ __('labels.ADN_STATIC_PAGES') }}
                        <a href="{{ route('cmspages.add') }}" class="btn btn-primary btn-sm">{{ __('labels.ADN_ADD') }}</a>
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="search-box form-inline float-right">
                        <input id="AdminSearchval" type="text" maxlength="50" placeholder="{{ __('labels.ADN_SEARCH_NAME_AND_TEXT') }}"
                               class="form-control form-control-sm mb-2 ml-sm-2 mb-sm-0" name="searchval" value="{{ old('searchval') }}" >

                        <input type="submit" id="submitbtn" class="btn btn-outline-primary btn-sm nomargin submitsearch" attr="search" value="{{ __('labels.ADN_SEARCH') }}">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="col-12">

        <div id="loading-div"></div>

        @include('cmspages.listing')

    </div>

    <link rel="stylesheet" href="{{ asset('control/vendors/chosen/chosen.css') }}">
@endsection

@section('page-script')
    <script src="{{ asset('control/vendors/chosen/chosen.js') }}"></script>

    <script >
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".chosen-select").chosen();

            $('.delete-page').on('click', function () {
                var confirmMessage = $(this).data('message');
                var id = $(this).data('id');
                if(confirm(confirmMessage)){
                    $('#AdminOptions').find('option[value="Delete"]').attr('selected', 'selected');
                    $('#id'+id).attr('checked', 'checked');
                    $('#adminPagesForm').submit();
                } else {
                    console.log('invalid_data');
                }
            });
        });

    </script>
@endsection
