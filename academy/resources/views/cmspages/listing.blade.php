<div class="tab-pane active show" id="new-packages" role="tabpanel" aria-labelledby="new-packages-tab">

<form id="adminPagesForm" method="POST" action="{{ route('cmspages.update.action') }}" >
    @csrf
    <div id="checkstatus" class="col-6">
        <div class="mb-3 form-inline">
            <select name="data[Admin][options]" class="options form-control form-control-sm" style="" id="AdminOptions">
                <option value="">{{ __('labels.SELECT_ACTION') }}</option>
                <option value="Active">{{ __('labels.ACTIVE') }}</option>
                <option value="Delete">{{ __('labels.DELETE') }}</option>
                <option value="Inactive">{{ __('labels.INACTIVE') }}</option>
            </select>
            <input id="submitbtn" class="submit btn btn-outline-primary btn-sm mb-2 mr-sm-2 mb-sm-0" type="submit" value="{{ __('labels.SUBMIT') }}">
            <label class="error" id="checkerr" style="float: left; margin: -10px 0 0;"></label>
            <div class="clear"></div>
        </div>
    </div>

    <div class="col-12 pt-3">
        <table class="table table-sm table-hover bg-white">
            <thead class="thead-light">
            <tr>
                <th scope="col"><input type="checkbox" name="data[Admin][check]" id="checkall" value="0"></th>
                <th scope="col">@sortablelink('name', __('labels.ADN_PAGE_NAME'))</th>
                <th scope="col">{{ __('labels.ADN_META_TITLE') }}</th>
                <th scope="col">@sortablelink('seourl', __('labels.ADN_SEO_URL'))</th>
                <th scope="col">@sortablelink('status', __('labels.ADN_STATUS'))</th>
                <th scope="col">@sortablelink('created_at', __('labels.ADN_CREATED'))</th>
                <th scope="col">{{ __('labels.ADN_ACTIONS') }}</th>
            </tr>
            </thead>
            @foreach ($cmspages as $page)
                <tr>
                    <td>
                        <input type="checkbox" name="data[id][{{ $page->id }}]" class="chk" value="{{ $page->id }}" id="id{{ $page->id }}">
                    </td>
                    <td>{{ $page->name }}</td>
                    <td>{{ $page->metatitle }}</td>
                    <td>{{ $page->seourl }}</td>
                    <td>{{ @($page->status) ? __('labels.ACTIVE') : __('labels.INACTIVE')}}</td>
                    <td>{{ $page->created_at }}</td>
                    <td>

                        <a href="{{ route('cmspages.edit', $page->id) }}" class="btn btn-light btn-sm edit-user"
                           title="Edit" ><i class="ion-edit"></i></a>

                        <a href="#" class="btn btn-danger btn-sm delete-page"
                           title="DELETE"
                           data-toggle="tooltip"
                           data-placement="bottom"
                           data-id="{{ $page->id }}"
                           data-message="{{ __('labels.ARE_YOU_SURE_TO_DELETE_RECORD', ['record' => $page->name]) }}"
                        ><i class="ion-close-round"></i></a>

                    </td>
                </tr>
            @endforeach
        </table>

        <div class="paging">

        </div>
    </div>
</form>
</div>
