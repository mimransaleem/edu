<style>
    .chosen-container-multi {
        width: 100% !important;
    }
    .chosen-container-multi .chosen-choices {
        padding: .375rem .75rem !important;
        border: 1px solid #ced4da !important;
        border-radius: 5px !important;
        box-shadow: 0px 5px 2px white inset !important;
    }
    .chosen-container-multi .chosen-choices li {
        float: right !important;
    }
    .chosen-container-multi .chosen-choices li.search-choice {
        font-size: 10pt !important;
        padding: 6px 20px 7px 12px !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
        font-size: 10pt !important;
        font-family: "Cairo",Arial,Tahoma,sans-serif !important;
    }
    .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
        top: 8px !important;
    }
</style>

<div class="tab-pane" id="new-package-features" role="tabpanel" aria-labelledby="new-package-features-tab">
    <form method="post" action="#">
        @if (in_array('admin', $currentUserRoles))
        <div class="p-3">
            <div class="form-row mxw-lg">
                <div class="form-group col">
                    <label><b><?php echo __('labels.ADN_PACKAGES'); ?></b></label>
                    <select class="form-control" id="roleName">
                        @foreach($packages as $package)
                            <option value="{{ $package->id }}">
                                {{ $package->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col">
                    <label><b><?php echo __('labels.ADN_FEATURES'); ?></b></label>
                    <select
                            id="select-abilities"
                            data-placeholder="<?php echo __('labels.SELECT_FEATURES'); ?>"
                            class="chosen-select"
                            multiple style="width:100% !important;"
                    >
                        @foreach($features as $feature)
                        <option value="{{ $feature['id'] }}">
                            {{ $feature['name'] }}
                        </option>
                        @endforeach
                    </select>

                    <select id="bundle_package_id"
                            class="form-control"
                            style="width:100% !important;" >
                        <option value="0">{{ __('labels.SELECT_BUNDLE_PACKAGE') }}</option>
                    @foreach($packages as $package)
                            @if ($package->is_bundle == 1)
                            <option value="{{ $package->id }}">
                                {{ $package->name }}
                            </option>
                            @endif
                        @endforeach
                    </select>


                </div>
                <div class="form-group">
                    <label class="invisible d-block"> - </label>
                    <button type="submit" class="btn btn-primary save-role-ability px-4">
                        <?php echo __('labels.SAVE'); ?>
                    </button>
                </div>
            </div>
        </div>
        @endif
        <div class="col-sm-12">
            <div class="card border border-dark">
                <div class="card-header bg-dark text-white">
                    <h5 class="mb-0"><?php echo __('labels.PACKAGE_FEATURES'); ?></h5>
                </div>
                <div class="card-body py-0">
                    <div class="row">
                        <table class="table table-sm td1-fluid mb-1 mt-1">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col"><?php echo __('labels.PACKAGE'); ?></th>
                                <th scope="col"><?php echo __('labels.ADN_FEATURES'); ?></th>
                                <th scope="col"><?php echo __('labels.BUNDLE'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($packages as $package):?>
                            <tr>
                                <td>
                                    <h5 class="px-3 py-2">{{ $package->name }}</h5>
                                </td>
                                <td class="pb-4">
                                    <?php foreach ($package->features as $feature){?>
                                    <span class="badge badge-light py-1 m-1 border">
                                        <span class="mx-1"><?php echo $feature['name']?></span>
                                        <button type="button"
                                                @if (in_array('admin', $currentUserRoles))
                                                data-toggle="modal" data-target="#confirmDelRoleAbilityModal"
                                                data-roleName="<?php echo $package->id?>"
                                                data-abilityName="<?php echo $feature['id']?>"
                                                data-abilityLabel="<?php echo $feature['name'];?>"
                                                class="btn btn-sm btn-danger text-white border-0 p-0 mb-0 remove-ability-modal"
                                                @else
                                                disabled="disabled"
                                                class="btn btn-sm btn-default text-white border-0 p-0 mb-0"
                                                @endif
                                                style="width: 20px;margin-bottom: 0 !important;"

                                        >
                                            <i class="fa fa-times"></i>
                                        </button>

                                    </span>
                                    <?php }?>
                                </td>
                                <td class="pb-4">
                                    {{ $package->bundlePackage['name'] }}
                                </td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $('.remove-ability-modal').bind('click', function(){
            // $(this).html('<img src="/images/spinner.gif" style="width: 18px; height: 19px; border-radius: 2px; margin-top: -3px;">');
            var rolename = $(this).data("rolename");
            var abilityname = $(this).data("abilityname");
            $('#rolename').val(rolename);
            $('#abilityname').val(abilityname);
            $('#selected_ability').html(abilityname);
        });
    });
</script>
