@extends('layouts.admin')

@section('content')

    <div class="col-12">
        <div class="card card-light border-light my-3 border-0 rounded-0">
            <div class="card-header">
                <h4 class="mb-0"><?php echo __('labels.EDIT_PACKAGE'); ?></h4>
            </div>
            <div class="card-body">
                <form method="POST" action="/packages/{{ $package->id }}">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="packageName"><?php echo __('labels.PACKAGE_NAME'); ?></label>
                                <input id="packageName" type="text" maxlength="50" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $package ? $package->name : old('name') }}" required autocomplete="name" >
                                @error('name')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="packageDesc"><?php echo __('labels.PACKAGE_DESC'); ?></label>
                                <textarea id="packageDesc" type="text" maxlength="7168" class="form-control @error('description') is-invalid @enderror" name="description" >{{ $package ? $package->description : old('description') }}</textarea>
                                @error('description')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="packageText"><?php echo __('labels.PACKAGE_TEXT'); ?></label>
                                <textarea id="packageText" type="text" maxlength="7168" class="form-control ckeditor @error('package_text') is-invalid @enderror" name="package_text" >{{ $package ? $package->package_text : old('package_text') }}</textarea>
                                @error('package_text')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="packagePrice"><?php echo __('labels.PACKAGE_PRICE'); ?></label>
                                <input id="packagePrice" type="text" maxlength="10" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $package ? $package->price : old('price') }}" >
                                @error('price')
                                <div class="error-message">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">

                                <label for="CategoryStatus">{{  __('labels.IS_BUNDLE') }}</label>
                                <input type="checkbox" name="is_bundle" class="class-checkbox" {{ $package->is_bundle == 1 ? 'checked="checked"' : '' }} value="1" id="is_bundle">

                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">

                                <label for="CategoryStatus">{{  __('labels.ACTIVE') }}</label>
                                <input type="checkbox" name="status" class="class-checkbox" {{ $package->status == 1 ? 'checked="checked"' : '' }} value="1" id="status">

                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">
                        {{ __('labels.SUBMIT') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('page-script')
    <script src="{{ asset('js/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script >
        $(document).ready(function () {
            $('.class-checkbox').bootstrapToggle();
        });</script>
@endsection
