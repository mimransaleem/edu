<div class="tab-pane active show" id="new-packages" role="tabpanel" aria-labelledby="new-packages-tab">

<form id="adminUsersForm" method="POST" action="/packages/update" >
    @csrf
    <div id="checkstatus" class="col-6 hide">
        <div class="mb-3 form-inline">
            <select name="data[Admin][options]" class="options form-control form-control-sm" style="" id="AdminOptions">
                <option value="">{{ __('labels.SELECT_ACTION') }}</option>
                <option value="Active">{{ __('labels.ACTIVE') }}</option>
                <option value="Delete">{{ __('labels.DELETE') }}</option>
                <option value="Inactive">{{ __('labels.INACTIVE') }}</option>
            </select>
            <input id="submitbtn" class="submit btn btn-outline-primary btn-sm mb-2 mr-sm-2 mb-sm-0" type="submit" value="{{ __('labels.SUBMIT') }}">
            <label class="error" id="checkerr" style="float: left; margin: -10px 0 0;"></label>
            <div class="clear"></div>
        </div>
    </div>

    <div class="col-12 pt-3">
        <table class="table table-sm table-hover bg-white">
            <thead class="thead-light">
            <tr>
                <th scope="col"><input type="checkbox" name="data[Admin][check]" id="checkall" value="0"></th>
                <th scope="col">{{ __('labels.PACKAGE_NAME') }}</th>
                <th scope="col">{{ __('labels.PACKAGE_DESC') }}</th>
                <th scope="col">{{ __('labels.BUNDLE') }}</th>
                <th scope="col">{{ __('labels.STATUS') }}</th>
                <th scope="col">{{ __('labels.ADN_CREATED') }}</th>
                <th scope="col">{{ __('labels.ADN_ACTIONS') }}</th>
            </tr>
            </thead>
            @foreach ($packages as $package)
                <tr>
                    <td>
                        <input type="checkbox" name="data[id][{{ $package->id }}]" class="chk" value="{{ $package->id }}" id="id{{ $package->id }}">
                    </td>
                    <td>{{ $package->name }}</td>
                    <td>{{ $package->description }}</td>
                    <td>{{ @($package->is_bundle) ? __('labels.YES') : __('labels.NO')}}</td>
                    <td>{{ @($package->status) ? __('labels.ACTIVE') : __('labels.INACTIVE')}}</td>
                    <td>{{ $package->created_at }}</td>
                    <td>
                        @if (in_array('admin', $currentUserRoles))
                        <a href="{{ route('packages.edit', $package->id) }}" class="btn btn-light btn-sm edit-user"
                           title="Edit" ><i class="ion-edit"></i></a>

                        <a href="#" class="btn btn-danger btn-sm delete-user"
                           title="DELETE"
                           data-toggle="tooltip"
                           data-placement="bottom"
                           data-id="{{ $package->id }}"
                           data-message="{{ __('labels.ARE_YOU_SURE_TO_DELETE_RECORD', ['record' => $package->name]) }}"
                        ><i class="ion-close-round"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>

        <div class="paging">

        </div>
    </div>
</form>
</div>
