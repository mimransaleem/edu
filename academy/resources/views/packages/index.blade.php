@extends('layouts.admin')

@section('content')

    <div class="heading col-12 bg-section mb-3 py-3">
        <form id="adminSearchForm" method="GET" action="/packages" >
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">
                        {{ __('labels.ADN_PACKAGES') }}
                        @if (in_array('admin', $currentUserRoles))
                        <a href="/packages/create" class="btn btn-primary btn-sm">{{ __('labels.ADN_ADD') }}</a>
                        @endif
                    </h4>
                </div>
                <div class="col-md-6">
                    {{--<div class="search-box form-inline float-left">
                        <input id="AdminSearchval" type="text" maxlength="50" placeholder="{{ __('labels.ADN_SEARCH_NAME_AND_TEXT') }}"
                               class="form-control form-control-sm mb-2 ml-sm-2 mb-sm-0" name="searchval" value="{{ old('searchval') }}" >

                        <input type="submit" id="submitbtn" class="btn btn-outline-primary btn-sm nomargin submitsearch" attr="search" value="{{ __('labels.ADN_SEARCH') }}">
                    </div>--}}
                </div>
            </div>
        </form>
    </div>

    <div class="col-12">
        <ul class="nav nav-tabs" id="packages-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="new-packages-tab" data-toggle="tab" href="#new-packages" role="tab" aria-controls="new-packages" aria-selected="true">
                    <?php echo __('labels.ADN_PACKAGES'); ?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="new-package-features-tab" data-toggle="tab" href="#new-package-features" role="tab" aria-controls="new-package-features" aria-selected="false">
                    <?php echo __('labels.ASSIGN_FEATURES_TO_PACKAGE'); ?>
                </a>
            </li>

        </ul>

        <div id="loading-div"></div>
        <div id="add-role-error-div" class="d-none bg-white pt-3 px-3 pb-1 border border-top-0 border-bottom-0">
            <div class="alert alert-danger d-flex">
                <i class="fa fa-times-circle fa-lg mt-1"></i>
                <ul class="mb-1 w-100"></ul>
            </div>
        </div>

        <div class="tab-content" id="newcoht-tabs-panes">

            @include('packages.packages_listing')
            @include('packages.features')

        </div>

    </div>
    <div class="modal fade" id="confirmDelRoleAbilityModal" tabindex="-1" role="dialog" aria-labelledby="confirmDelRoleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php  echo __('labels.DELETE_PACKAGE_FEATURE');?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning d-flex px-2">
                        <i class="fa fa-info-circle fa-lg py-2 px-1"></i>
                        <span><?php  echo __('labels.DELETE_PACKAGE_FEATURE_CONFIRM_MSG');?> </span>
                        <span id="selected_ability" ></span>
                        <input type="hidden" id="rolename" value="0" />
                        <input type="hidden" id="abilityname" value="0" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light px-4" data-dismiss="modal">
                        <?php  echo __('labels.CANCEL'); ?>
                    </button>&nbsp;
                    <button type="button" class="btn btn-light remove-ability">
                        <?php  echo __('labels.YES_SURE');?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="{{ asset('control/vendors/chosen/chosen.css') }}">
@endsection

@section('page-script')
    <script src="{{ asset('control/vendors/chosen/chosen.js') }}"></script>

    <script >
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".chosen-select").chosen();

            $("#select-abilities").next().css( "width", "50%" );

            $('.nav-tabs a').on('shown.bs.tab', function (e) {
                window.location.hash = e.target.hash;
            });

            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
            }

            $('.save-role-ability').on('click',function (e) {
                AddPackageFeature();
                return false;
            });

            $('.remove-ability').on('click',function (e) {
                e.preventDefault();
                RemovePackageFeature(
                    $('#rolename').val(),
                    $('#abilityname').val()
                );
                return false;
            });

            $('.remove-ability-modal').on('click', function (e) {
                var rolename = $(this).data("rolename");
                var abilityname = $(this).data("abilityname");
                var abilityLabel = $(this).data("abilitylabel");
                $('#rolename').val(rolename);
                $('#abilityname').val(abilityname);
                $('#selected_ability').html(abilityLabel);
            })

            $('.delete-user').on('click', function () {
                var confirmMessage = $(this).data('message');
                var id = $(this).data('id');
                if(confirm(confirmMessage)){
                    $('#AdminOptions').find('option[value="Delete"]').attr('selected', 'selected');
                    $('#id'+id).attr('checked', 'checked');
                    $('#adminUsersForm').submit();
                } else {
                    console.log('invalid_data');
                }
            });
        });

        function AddPackageFeature() {

            $('#add-role-error-div .alert-danger ul').empty();

            var features = $("#select-abilities").val();
            var bundle_package_id = $("#bundle_package_id").val();

            if(features.length <= 0 && bundle_package_id == 0){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SELECT_FEATURE_OR_BUNDLE_PACKAGE')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }

            var package = $('#roleName').val();
            if(package == ""){
                $('#add-role-error-div .alert-danger ul').html('<li><?php echo __('labels.SELECT_PACKAGE')?></li>');
                $('#add-role-error-div').addClass('d-block').removeClass('d-none');
                return false;
            }

            $.ajax({
                url: BASE_URL+"packages/addPackageFeatures",
                type: "POST",
                data: {
                    'package' : package,
                    'features' : features,
                    'bundle_package_id' : bundle_package_id
                },

                success: function(data) {
                    location.reload();
                },
                error : function(err) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function RemovePackageFeature(packageId,featureId) {

            $('#add-role-error-div .alert-danger ul').empty();

            $.ajax({
                url: BASE_URL+"packages/"+packageId+"/removePackageFeature/"+featureId,
                type: "POST",
                data: {},
                success: function(data) {

                    location.reload();
                },
                error : function(err, req) {
                    showErrorMessage(err);
                }
            });
            return false;
        }

        function showErrorMessage(response) {
            $errors = response.responseJSON;
            errorsHtml = '<div class="alert alert-danger"><ul>';

            $.each( $errors.errors, function( key, value ) {
                errorsHtml += '<li>' + value[0] + '</li>';
            });
            errorsHtml += '</ul></div>';

            $('#add-role-error-div').html( errorsHtml );
            $('#add-role-error-div').addClass('d-block').removeClass('d-none');

        }

    </script>
@endsection
