@extends('layouts.admin')

@section('content')

    <div class="heading col-12 bg-section mb-3 py-3">
        <form id="adminSearchForm" method="GET" action="/users" >
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">
                        Students
                        @can('isAdmin')
                        <a href="/users/create" class="btn btn-primary btn-sm">{{ __('labels.ADN_ADD') }}</a>
                        @endcan
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="search-box form-inline float-right">
                        <input id="AdminSearchval" type="text" maxlength="50" placeholder="{{ __('labels.ADN_SEARCH_USERNAME_ID_FIRST_LAST_NAME') }}"
                               class="form-control form-control-sm mb-2 ml-sm-2 mb-sm-0" name="searchval" value="{{ old('searchval') }}" >

                        <input type="submit" id="submitbtn" class="btn btn-outline-primary btn-sm nomargin submitsearch" attr="search" value="{{ __('labels.ADN_SEARCH') }}">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <form id="adminUsersForm" method="POST" action="/users/update" >
        @csrf
        @can('isAdmin')
        <div id="checkstatus" class="col-6">
            <div class="mb-3 form-inline">
                <select name="data[Admin][options]" class="options form-control form-control-sm" style="" id="AdminOptions">
                    <option value="">{{ __('labels.SELECT_ACTION') }}</option>
                    <option value="Active">{{ __('labels.ACTIVE') }}</option>
                    <option value="Delete">{{ __('labels.DELETE') }}</option>
                    <option value="Inactive">{{ __('labels.INACTIVE') }}</option>
                </select>
                <input id="submitbtn" class="submit btn btn-outline-primary btn-sm mb-2 mr-sm-2 mb-sm-0" type="submit" value="{{ __('labels.SUBMIT') }}">
                <label class="error" id="checkerr" style="float: left; margin: -10px 0 0;"></label>
                <div class="clear"></div>
            </div>
        </div>
        @endcan
        <div class="col-12">
            <table class="table table-sm table-hover bg-white">
                <thead class="thead-light">
                <tr>
                    <th scope="col"><input type="checkbox" name="data[Admin][check]" id="checkall" value="0"></th>
                    {{--<th scope="col">{{ __('labels.ADN_ID') }}</th>--}}
                    <th scope="col">{{ __('labels.USERNAME') }}</th>
                    <th scope="col">{{ __('labels.STATUS') }}</th>
                    <th scope="col">{{ __('labels.TYPE') }}</th>
                    <th scope="col">{{ __('labels.ADN_CREATED') }}</th>
                    <th scope="col">{{ __('labels.ADN_ACTIONS') }}</th>
                </tr>
                </thead>
                @foreach ($users as $user)
                <tr>
                    <td>
                        @if ( @Auth::user()->id != $user->id )
                            <input type="checkbox" name="data[id][{{ $user->id }}]" class="chk" value="{{ $user->id }}" id="id{{ $user->id }}">
                        @endif
                    </td>
                    {{--<td>{{ $user->id }}</td>--}}
                    <td><a href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></td>
                    <td>{{ @is_null($user->banned_at) ? __('labels.ACTIVE') : __('labels.INACTIVE')}}</td>
                    <td>
                        {{ @ucfirst($user->role) }}
                    </td>
                    <td>{{ $user->created_at }}</td>
                    <td>
                        @can('isAdmin')
                        @if ( @Auth::user()->id != $user->id)
                            <a href="#" class="btn btn-danger btn-sm delete-user"
                               title="DELETE"
                               data-toggle="tooltip"
                               data-placement="bottom"
                               data-id="{{ $user->id }}"
                               data-message="{{ __('labels.ARE_YOU_SURE_TO_DELETE_USER', ['email' => $user->email]) }}"
                            ><i class="ion-close-round"></i></a>

                            <a href="{{ route('login-as-user', $user->id) }}" class="btn btn-light btn-sm" target="_self"
                               title="LOGIN_AS_USER {{ $user->email }}"
                               data-toggle="tooltip"
                               data-placement="bottom"
                            ><i class="ion-key"></i></a>
                        @endif
                        @endcan
                    </td>
                </tr>
                @endforeach
            </table>

            <div class="paging">

            </div>
        </div>
    </form>
    <script >
        (function() {
            $('.delete-user').on('click', function () {
                var confirmMessage = $(this).data('message');
                var id = $(this).data('id');
                if(confirm(confirmMessage)){
                    $('#AdminOptions').find('option[value="Delete"]').attr('selected', 'selected');
                    $('#id'+id).attr('checked', 'checked');
                    $('#adminUsersForm').submit();
                } else {
                    console.log('invalid_data');
                }
            });
        })();

    </script>
@endsection
