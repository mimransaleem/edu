<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google" content="notranslate" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta name="csrf-token" content="{{ @csrf_token() }}" />

    <title>{{ __(config('app.name', 'Nadrus')) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Cairo:300,400,600,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('control/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/metismenu/2.7.1/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/assets/css/ltr/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/select2/dist/css/select2.min.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery.1.8.2.js') }}"></script>
    <script src="{{ asset('js/jquery.ui.js') }}"></script>
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/jwplayer/jwplayer.js') }}"></script>
    <script>
        var BASE_URL = "<?php echo route('base_url'); ?>/";
        var LANG = "{{ app()->getLocale() }}";
    </script>

</head>

<body>
    <div class="container-fluid">

        @include('layouts.admin.header')

        <div class="row row-offcanvas row-offcanvas-<?php echo (app()->getLocale()=='ar') ? 'right':'left'; ?>">
            @include('layouts.admin.left')
            <div class="col-md-9 col-lg-10 px-0">
                <div class="content-wrapper">
                    @include('layouts.admin.breadcrumb')
                    @include('layouts.admin.flash-message')
                    @yield('content')
                    {{-- dd(url()->current()) --}}

                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('control/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/validationmessages-ar.js') }}"></script>
    <script src="{{ asset('js/common_functions.js') }}"></script>
    @if (Route::currentRouteName() == 'courses.index')
    <script src="{{ asset('js/jquery.form.js') }}"></script>
    <script src="{{ asset('js/admin_functionality.js') }}"></script>
    @endif
    <script src="{{ asset('js/admin/validate.js') }}"></script>

    <link rel="stylesheet" href="/vendors/tooltip/dist/css/tooltip-theme-arrows.css" />

    <script src="{{ asset('vendors/tether/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('vendors/drop/dist/js/drop.min.js') }}"></script>
    <script src="{{ asset('vendors/tooltip/dist/js/tooltip.min.js') }}"></script>
    <script src="{{ asset('control/vendors/popper/popper.min.js') }}"></script>
    <script src="{{ asset('control/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('control/vendors/metismenu/2.7.1/metisMenu.min.js') }}"></script>

    <script>
        $("#sidebarMenu").metisMenu();
    </script>

    <script>

        (function() {
            'use strict';

            $('[data-toggle="tooltip"]').tooltip();

            $('[data-toggle="offcanvas"]').on('click', function () {
                $('.row-offcanvas').toggleClass('active');
                $('body').toggleClass('overflow-x');
            })

        })();

    </script>
    @yield('page-script')
</body>
</html>
