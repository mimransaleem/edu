<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Nadrus') }}  @yield('title')</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Cairo:300,400,600,700" >
    <link href="https://fonts.googleapis.com/css?family=El+Messiri:400,500,600,700&display=swap&subset=arabic" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('control/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/metismenu/2.7.1/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/bootstrap/css/bootstrap-rtl.css') }}" rel="stylesheet">
    <link href="{{ asset('control/assets/css/rtl/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery.1.8.2.js') }}"></script>
    <script src="{{ asset('js/jquery.ui.js') }}"></script>
</head>

<body class="funnel" cz-shortcut-listen="true">
<main class="container">

    <header class="row">
        <h1 class="col-12 text-center m-0 py-5" style="height: 150px;">
            <a href="/" title="إنتاجي" class="d-inline-block my-3" id="branding">
                <img src="control/assets/wizimg/intajy-logo.png" alt="إنتاجي">
            </a>
        </h1>
    </header>

    @yield('content')

    <footer class="row mt-5 pt-5">
        <div class="col-12 text-center">
            <p class="font-weight-light my-2 d-none">
                <span>تواصلوا معنا</span>: <span dir="ltr"><a href="call:+971-2-419-1042">+971-2-419-1042</a></span> |
                <span><a href="mailto:contactme@intajy.com">contactme@intajy.com</a></span>
            </p>
            <p class="copy font-psmaller-20 text-muted mb-4">
                <span>© 2019 جميع الحقوق محفوظة من قبل </span><strong>إنتاجي</strong>
            </p>
        </div>
    </footer>

</main>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="{{ asset('vendors/tooltip/dist/js/tooltip.min.js') }}"></script>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        $('[data-toggle="tooltip"]').tooltip();
    })();
</script>
@yield('page-script')

</body></html>