<div class="col-md-3 col-lg-2 px-0 sidebar-offcanvas bg-primary">
    <div class="sidebarNavbar px-0">
        <nav class="menu">
            <ul class="sidebar-menu metismenu list-unstyled list-group" id="sidebarMenu">
                <li class="{{ @(Route::currentRouteName() == 'users.index') ? 'active' : '' }}">
                    <a href="/users" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(Route::currentRouteName() == 'users.index') ? 'true' : 'false' }}" >
                        <i class="pl-2"></i>
                        <span class="section-title"> {{ @__('labels.ADN_USERS') }} </span>

                    </a>
                </li>
                <?php $roles = [];//Auth::user()->roles->pluck('slug')->toArray(); ?>
                @if (in_array('admin', $roles))
                <li class="{{ @(Route::currentRouteName() == 'deployment.index') ? 'active' : '' }}">
                    <a href="/deployments" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(Route::currentRouteName() == 'deployment.index') ? 'true' : 'false' }}" >
                        <i class="pl-2"></i>
                        <span class="section-title"> {{ @__('labels.ADN_DEPLOYMENTS') }} </span>

                    </a>
                </li>


                <li class="{{ @(Route::currentRouteName() == 'acl-manage') ? 'active' : '' }}">
                    <a href="javascript:void(0);" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(Route::currentRouteName() == 'acl-manage') ? 'true' : 'false' }}">
                        <i class="ion-key pl-2"></i>
                        <span class="section-title">{{ @__('labels.ACL_MANAGER') }}</span>
                        <i class="fa arrow ion-ios-arrow-right"></i>
                    </a>
                    <ul aria-expanded="{{ @(Route::currentRouteName() == 'acl-manage') ? 'true' : 'false' }}" class="collapse" style="{{ @(Route::currentRouteName() == 'acl-manage') ? '' : 'height: 0px;' }}">
                        <li><a href="/acl-manage">{{ @__('labels.ADD_AND_ASSIGN_ROLE_ABILITIES') }}</a></li>
                    </ul>
                </li>
                @endif
                <li class="{{ @(Route::currentRouteName() == 'packages.index') ? 'active' : '' }}">
                    <a href="/packages" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(Route::currentRouteName() == 'packages.index') ? 'true' : 'false' }}" >
                        <i class="pl-2"></i>
                        <span class="section-title"> {{ @__('labels.ADN_PACKAGES') }} </span>

                    </a>
                </li>
                @can('isSuperAdmin')
                    <li class="{{ @(Route::currentRouteName() == 'features.index') ? 'active' : '' }}">
                        <a href="/features" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(Route::currentRouteName() == 'features.index') ? 'true' : 'false' }}" >
                            <i class="pl-2"></i>
                            <span class="section-title"> {{ @__('labels.ADN_FEATURES') }} </span>

                        </a>
                    </li>
                    <li class="{{ @(Route::currentRouteName() == 'feature-settings.index') ? 'active' : '' }}">
                        <a href="/feature-settings" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(Route::currentRouteName() == 'feature-settings.index') ? 'true' : 'false' }}" >
                            <i class="pl-2"></i>
                            <span class="section-title"> {{ @__('labels.ADN_FEATURE_SETTINGS') }} </span>

                        </a>
                    </li>
                    <li class="{{ @( in_array(Route::currentRouteName(), ['emails.index', 'emails.custom'])) ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(in_array(Route::currentRouteName(), ['emails.index', 'emails.custom'])) ? 'true' : 'false' }}">
                            <i class="ion-at pl-2"></i>
                            <span class="section-title">{{ @__('labels.ADN_EMAILS') }}</span>
                            <i class="fa arrow ion-ios-arrow-right"></i>
                        </a>
                        <ul aria-expanded="{{ @(in_array(Route::currentRouteName(), ['emails.index', 'emails.custom'])) ? 'true' : 'false' }}" class="collapse" style="{{ @(in_array(Route::currentRouteName(), ['emails.index', 'emails.custom'])) ? '' : 'height: 0px;' }}">
                            <li><a href="{{ route('emails.custom') }}">{{ @__('labels.ADN_CUSTOMIZED_EMAILS') }}</a></li>
                            {{--</ul>--}}
                            {{--<ul aria-expanded="{{ @(Route::currentRouteName() == 'emails.index') ? 'true' : 'false' }}" class="collapse" style="{{ @(Route::currentRouteName() == 'emails.index') ? '' : 'height: 0px;' }}">--}}
                            <li><a href="/emails">{{ @__('labels.ADN_TEMPLATE_EMAILS') }}</a></li>
                        </ul>
                    </li>

                    <li class="{{ @( in_array(Route::currentRouteName(), ['migratefile', 'migrate-file'])) ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(in_array(Route::currentRouteName(), ['migratefile', 'migrate-file'])) ? 'true' : 'false' }}">
                            <i class="ion-help-buoy pl-2"></i>
                            <span class="section-title">{{ @__('labels.ADN_DEVELOPER') }}</span>
                            <i class="fa arrow ion-ios-arrow-right"></i>
                        </a>
                        <ul aria-expanded="{{ @(in_array(Route::currentRouteName(), ['migratefile', 'migrate-file'])) ? 'true' : 'false' }}" class="collapse" style="{{ @(in_array(Route::currentRouteName(), ['migratefile', 'migrate-file'])) ? '' : 'height: 0px;' }}">
                            <li><a href="{{ route('migratefile') }}">{{ @__('labels.ADN_MIGRATION') }}</a></li>

                        </ul>
                    </li>
                @endcan

                @can('isAdmin')
                    <li class="{{ @( in_array(Route::currentRouteName(), ['student.users', 'cmspages-1'])) ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(in_array(Route::currentRouteName(), ['student.users', 'cmspages-1'])) ? 'true' : 'false' }}">
                            <i class="ion-ios-list pl-2"></i>
                            <span class="section-title">{{ @__('labels.ADN_STUDENTS') }}</span>
                            <i class="fa arrow ion-ios-arrow-right"></i>
                        </a>
                        <ul aria-expanded="{{ @(in_array(Route::currentRouteName(), ['student.users', 'cmspages-1'])) ? 'true' : 'false' }}" class="collapse" style="{{ @(in_array(Route::currentRouteName(), ['student.users', 'cmspages-1'])) ? '' : 'height: 0px;' }}">
                            <li><a href="{{ route('student.users') }}">{{ @__('labels.ADN_USERS') }}</a></li>

                        </ul>
                    </li>

                    <li class="{{ @( in_array(Route::currentRouteName(), ['cmspages.index', 'cmspages-1'])) ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="list-group-item list-group-item-action border-0 rounded-0" aria-expanded="{{ @(in_array(Route::currentRouteName(), ['cmspages.index', 'cmspages-1'])) ? 'true' : 'false' }}">
                            <i class="ion-ios-gear pl-2"></i>
                            <span class="section-title">{{ @__('labels.ADN_SETTINGS') }}</span>
                            <i class="fa arrow ion-ios-arrow-right"></i>
                        </a>
                        <ul aria-expanded="{{ @(in_array(Route::currentRouteName(), ['cmspages.index', 'cmspages-1'])) ? 'true' : 'false' }}" class="collapse" style="{{ @(in_array(Route::currentRouteName(), ['cmspages.index', 'cmspages-1'])) ? '' : 'height: 0px;' }}">
                            <li><a href="{{ route('cmspages.index') }}">{{ @__('labels.ADN_STATIC_PAGES') }}</a></li>

                        </ul>
                    </li>
                @endcan
            </ul>
        </nav>
    </div>
</div>
