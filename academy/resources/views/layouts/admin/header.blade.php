<div class="row">
    <div class="col-12 px-0">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <div class="col-md-3 col-lg-2">
                <a class="navbar-brand p-0" title="<?php echo __('SITE_NAME')?>" href="#"><img src="{{ asset('images/nadrus_logo.png') }}" /></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <span class="float-left d-md-none">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas"><?php echo __('labels.MENU');?></button>
                </span>
            </div>
            <div class="col-md-9 col-lg-10">
             @php
                $resetToken = \session()->pull('admin_reset_token');
             @endphp
                {{--{{ $resetToken }}--}}
                <div class="collapse navbar-collapse pull-right" id="navbarHeader">
                    <div class="mr-auto">
                        <ul class="navbar-nav">
                            <li><a href="#" class="btn btn-outline-primary btn-sm ml-3 d-none d-sm-block" data-toggle="tooltip" data-placement="bottom" title="<?php echo __('labels.ADN_VIEW_PLATFORM');?>" target="_blank"><?php echo __('labels.ADN_PLATFORM');?> <i class="ion-link"></i></a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ion-person"></i> <span class="d-md-inline-block">{{ @Auth::user()->name }}</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right text-left rounded-0 mt-2" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item text-truncate" data-toggle="tooltip" data-placement="bottom" title="<?php echo __('labels.ADN_CLEAR_CACHE');?>" href="{{ route('clear-cache') }}"><i class="ion-nuclear pl-2"></i> <?php echo __('labels.ADN_CLEAR_CACHE');?></a>
                                    <a class="dropdown-item" href="{{ @route('password.reset', ['token' => $resetToken]) }}"><i class="ion-gear-a pr-2"></i> <?php echo __('labels.ADN_ACCOUNT');?></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><i class="ion-log-out pl-2"></i> <?php echo __('labels.LOGOUT');?></a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>