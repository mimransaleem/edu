<!--<nav aria-label="breadcrumb" role="navigation">
    <ol class="breadcrumb border-0 rounded-0 mb-0">
        @if (Route::currentRouteName() == 'acl-manage')
            <li class="breadcrumb-item"  aria-current="page"> {{ @__('labels.ACL_MANAGER') }} </li>
            <li class="breadcrumb-item"  aria-current="page"> <a href="/acl-manage"> {{ @__('labels.ADD_AND_ASSIGN_ROLE_ABILITIES') }} </a> </li>
        @elseif (Route::currentRouteName() == 'users.index')
            <li class="breadcrumb-item"  aria-current="page"> <a href="/users"> {{ @__('labels.ADN_USERS') }} </a> </li>
         @else
            <li class="breadcrumb-item"  aria-current="page"> <a href="{{Route::currentRouteName()}}">{{Route::currentRouteName()}}</a> </li>
        @endif
    </ol>
</nav>-->

@if (isset($id) && isset($featureId))
    {{ Breadcrumbs::render(Route::currentRouteName(), $id, $featureId) }}
@elseif (isset($id))
    {{ Breadcrumbs::render(Route::currentRouteName(), $id) }}
@elseif (isset($token))
    {{ Breadcrumbs::render(Route::currentRouteName(), $token) }}
@else
{{ Breadcrumbs::render(Route::currentRouteName()) }}
@endif

