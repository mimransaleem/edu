<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google" content="notranslate" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

    <title>{{ config('app.name', 'Nadrus') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Cairo:300,400,600,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('control/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/metismenu/2.7.1/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('control/vendors/bootstrap/css/bootstrap-ltr.css') }}" rel="stylesheet">
    <link href="{{ asset('control/assets/css/ltr/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery.1.8.2.js') }}"></script>
    <script src="{{ asset('js/jquery.ui.js') }}"></script>
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/jwplayer/jwplayer.js') }}"></script>


</head>

<body class="login-page" style="background: url(/images/bgs/learning_01.jpg) bottom center / cover no-repeat scroll;">

@yield('content')

<link rel="stylesheet" href="/vendors/tooltip/dist/css/tooltip-theme-arrows.css" />

<script src="{{ asset('vendors/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('vendors/drop/dist/js/drop.min.js') }}"></script>
<script src="{{ asset('vendors/tooltip/dist/js/tooltip.min.js') }}"></script>
<script src="{{ asset('control/vendors/popper/popper.min.js') }}"></script>
<script src="{{ asset('control/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('control/vendors/metismenu/2.7.1/metisMenu.min.js') }}"></script>

<script>
    $("#sidebarMenu").metisMenu();
</script>

<script>

    (function() {
        'use strict';

        $('[data-toggle="tooltip"]').tooltip();

        $('[data-toggle="offcanvas"]').on('click', function () {
            $('.row-offcanvas').toggleClass('active');
            $('body').toggleClass('overflow-x');
        })

    })();

</script>

</body>
</html>