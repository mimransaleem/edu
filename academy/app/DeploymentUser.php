<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nadrus\Package;

class DeploymentUser extends Model
{
    protected $table="deployment_users";

    protected $fillable = ['first_name', 'last_name', 'email', 'password'];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function package(){
        return $this->belongsTo(Package::class, 'package_id');
    }
}
