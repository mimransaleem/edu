<?php
/**
 * Created by PhpStorm.
 * User: contrive
 * Date: 11/8/19
 * Time: 8:41 PM
 */

namespace App\Nadrus\Services;


use App\Nadrus\Model\User;

class UserService
{
    /**
     * @var User
     */
    private $student;

    public function __construct(User $student)
    {

        $this->student = $student;
    }

    public function getAllStudents() {

        return $this->student::where('role', 'student')->get();
    }
}
