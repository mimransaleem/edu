<?php

namespace Nadrus;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nadrus\Feature\Setting;

/**
 * Class Feature
 * @package Nadrus
 */
class Feature extends Model {

    use SoftDeletes;

    /**
     * @var array
     */
    var $fillable = ['name', 'description', 'status'];

    public function settings()
    {
        return $this->hasMany(Setting::class);
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'package_features');
    }

    public function description(){
        return $this->description;

        $slug = str_replace(' ','_',$this->slug);
        $translationKey = strtoupper($slug).'_DESC';
        $translation = __d('features',$translationKey);
        return empty($translation) || $translation == $translationKey?$this->description:$translation;
    }

    public function name(){
        return $this->name;

        $slug = str_replace(' ','_',$this->slug);
        $translationKey = strtoupper($slug).'_NAME';
        $translation = __d('features',$translationKey);
        return empty($translation) || $translation == $translationKey ?$this->name:$translation;
    }

}