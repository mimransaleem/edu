<?php

namespace Nadrus\Feature;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nadrus\CakeAdapters\Model\Admin;
use Nadrus\Deployment\Configuration;
use Nadrus\Feature;
use Nadrus\FileUpload\Services\UpdatePathService;
use Nadrus\FileUpload\UploadableFile;
use Nadrus\FileUpload\UploadableImage;
use Nadrus\FileUpload\UploadableModel;
use Nadrus\Support\Facade;

/**
 * Class Setting
 * @package Nadrus\Feature
 */
class Setting extends Model implements UploadableModel {

    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = 'feature_settings';

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'has_default_value', 'default_value', 'required', 'can_view', 'can_edit', 'is_hidden_for_customer'];


    public function feature()
    {
        return $this->belongsTo(Feature::class);
    }
    
    public function configForDeployment($deployment_id){
       $config =  Configuration::where('feature_setting_id','=',$this->id)->where('deployment_id','=',$deployment_id);
       if($config->exists()){
           $config = $config->first();
           return $config->value;
       }
       return $this->has_default_value ? $this->default_value : NULL;
    }

    public function description(){
        $slug = str_replace(' ','_',$this->feature->slug.'_'.$this->name);
        $translationKey = strtoupper($slug).'_DESC';
        $translation = __d('features',$translationKey);
        return empty($translation) || $translation == $translationKey?$this->description:$translation;
    }

    public function name(){
        $slug = str_replace(' ','_',$this->feature->slug.'_'.$this->name);
        $translationKey = strtoupper($slug).'_NAME';
        $translation = __d('features',$translationKey);
        return empty($translation) || $translation == $translationKey ?$this->name:$translation;
    }
    public function getSelectOptions()
    {
        if($this->input_options =='admin_list')
            return (Admin::Where('supper',0)->pluck('username', 'username')->toArray());

        return json_decode($this->input_options,true);
    }

    public function getUploadableFileFor($deploymentId)
    {

        if($this->type == 'file' && $deploymentId ){
            $config =  Configuration::where('feature_setting_id','=',$this->id)->where('deployment_id','=',$deploymentId)->first();
            if(!$config){
                $config = new Configuration();
                $config->deployment_id = $deploymentId;
                $config->feature_setting_id = $this->id;
                $config->value = $this->has_default_value?$this->default_value:null;
                $config->save();
            }

            $updatePathService = new UpdatePathService($config, 'value', $config->id);
            $path = Facade::getBaseUploadFolder($deploymentId)."features/".$this->feature->slug."/".$this->name."/";
            $isImageExt = strtolower(pathinfo($this->default_value, PATHINFO_EXTENSION));

            if($this->default_value && in_array($isImageExt,['gif','jpg','jpeg','png','bmp']))
                return new UploadableImage($path,[$updatePathService]);

            return new UploadableFile($path,null,null,[$updatePathService]);
        }
        throw new \Exception('Could not create UploadableFile for Config!');
    }

}