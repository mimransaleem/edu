<?php

namespace Nadrus\Feature;

use Nadrus\Feature\Checker;

trait CheckerTrait {

    private $checker;
    private $activated;

    public function iniChecker()
    {
        $this->checker = new Checker($this->slug);
        $this->activated = $this->checker->isActivated();
    }
}