<?php

namespace Nadrus\Feature;

use Nadrus\Deployment;
use Nadrus\Deployment\Configuration;
use Nadrus\Traits\DeploymentSettingsTrait;

/**
 * Class Checker
 * @package Nadrus\Feature
 * This Class for checking and Getting feature state
 */
class Checker {

    use DeploymentSettingsTrait;
    /**
     * @var
     */
    protected $slug;

    /**
     * @var string this Will be constant|yaml|db
     */
    private $source = 'constant';

    /**
     * @var this will be used when source is yaml.
     */
    private $deploymentDomain;

    private $deployment;
    public static $deploymentConfig;

    /**
     * @param $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
        if(!intajy_admin) if (!self::$deploymentConfig) self::$deploymentConfig = static::getDeploymentSettings(getHost());
        //dd(self::$deploymentConfig);
    }

    /**
     * @param $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;

    }

    /**
     * @param string $source
     */
    private function setSource($source = 'constant')
    {
        $this->source = $source;
    }


    /**
     * @return bool
     */
    public function isActivated()
    {
        if ($this->deployment) return $this->isActivatedFromDB();

        return (isset(self::$deploymentConfig['deployment']['features'][$this->slug]['status']) && self::$deploymentConfig['deployment']['features'][$this->slug]['status']);
    }

    public function isActivatedFromDB()
    {
        return $this->deployment->isFeatureActive($this->slug);
    }


    /**
     * @param $domainName
     */
    public function loadFromYaml($domainName)
    {
        $this->setSource('yaml');
        $this->deploymentDomain = $domainName;
    }

    /**
     * @param $settingName
     * @return mixed|null
     */
    public function getSetting($settingName)
    {
        if (isset(self::$deploymentConfig['deployment']['features'][$this->slug]['setting'][$settingName])) return self::$deploymentConfig['deployment']['features'][$this->slug]['setting'][$settingName];

        return null;
    }

    /**
     * @param array $settingNames
     * @return array
     */
    public function getSettings(array $settingNames)
    {
        return array_map(function ($val) {
            return $this->getSetting($val);
        }, $settingNames);
    }

    /**
     * @param $settingName
     * @return bool
     */
    public function issetSetting($settingName)
    {
        return isset(self::$deploymentConfig['deployment']['features'][$this->slug]['setting'][$settingName]);
    }

    /**
     * @param array $settingNames
     * @return bool
     */
    public function issetSettings(array $settingNames)
    {
        return !in_array(false, $this->getSettings($settingNames));
    }

    /**
     * @param $settingNames
     * @return bool
     */
    public function issetSettingsAny($settingNames)
    {
        return !$this->issetSettings($settingNames);
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function isActivated__($slug)
    {
        $instance = new static($slug);

        return $instance->isActivated();
    }

    /**
     * @param $slug
     * @param $settingName
     * @return mixed
     */
    public static function getSetting__($slug, $settingName)
    {
        $instance = new static($slug);

        return $instance->getSetting($settingName);
    }

    /**
     * @param $slug
     * @param array $settingNames
     * @return mixed
     */
    public static function getSettings__($slug, array $settingNames)
    {
        $instance = new static($slug);

        return $instance->getSettings($settingNames);
    }

    /**
     * @param $slug
     * @param $settingName
     * @return mixed
     */
    public static function issetSetting__($slug, $settingName)
    {
        $instance = new static($slug);

        return $instance->issetSetting($settingName);
    }

    /**
     * @param $slug
     * @param array $settingNames
     * @return mixed
     */
    public static function issetSettings__($slug, array $settingNames)
    {
        $instance = new static($slug);

        return $instance->issetSettings($settingNames);
    }

    /**
     * @param $slug
     * @param $settingNames
     * @return mixed
     */
    public static function issetSettingsAny__($slug, $settingNames)
    {
        $instance = new static($slug);

        return $instance->issetSettingsAny($settingNames);
    }

    /**
     * @return null
     */
    public static function getDeploymentID()
    {

        if (isset(self::$deploymentConfig['deployment']['id'])) return self::$deploymentConfig['deployment']['id'];

        return null;
    }

    /**
     * @return mixed
     */
    public static function getDeployment()
    {
        return \Nadrus\Deployment::find(Checker::getDeploymentID());
    }


    /**
     * @return mixed
     */
    public function getFeature()
    {
        return \Nadrus\Feature::where('slug', '=', $this->slug)->first();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function getFeature__($slug)
    {
        $instance = new static($slug);

        return $instance->getFeature();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function getFeatureByQuery($slug)
    {
        return \Nadrus\Feature::where('slug', '=', $slug)->first();
    }


    public static function isActivatedOn($slug, Deployment $deployment)
    {
        $instance = new static($slug);
        $instance->setDeployment($deployment);

        return $instance->isActivated();
    }

    public static function getAllSettings($domain, $slug)
    {
        $config = \Symfony\Component\Yaml\Yaml::parse(file_get_contents(WWW_ROOT . "custom/" . strtolower($domain) . ".config.yml"));
        $allSettings = [];

        foreach ($config['deployment']['features'][$slug]['setting'] as $key => $setting)
        {
            $keys = explode('\\', $key);
            switch (count($keys))
            {
                case 1:
                    $allSettings[$keys[0]] = $setting;
                    break;
                case 2:
                    $allSettings[$keys[0]][$keys[1]] = $setting;
                    break;
                case 3:
                    $allSettings[$keys[0]][$keys[1]][$keys[2]] = $setting;
                    break;
                case 4:
                    $allSettings[$keys[0]][$keys[1]][$keys[2]] [$keys[3]] = $setting;
                    break;

            }
        }

        return $allSettings;

    }

}