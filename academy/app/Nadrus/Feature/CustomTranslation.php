<?php

namespace Nadrus\Feature;
use Nadrus\CakeAdapters\LangAdapter;
use Nadrus\CakeAdapters\Model\Language;
use Nadrus\Deployment\State;
use Nadrus\Deployment\Configuration;
use Nadrus\Feature\Checker;
use Nadrus\Translation;


/**
 * Class CustomTranslation
 * @package Nadrus\Feature
 */
class CustomTranslation extends Featureable{

    protected  $slug = 'custom_translation';
    /**
     * @var array
     */
    public $languageCode = ['ar','en','ur'];

    /**
     *
     */
    public function afterActivate(){
        parent::afterActivate();
        $this->generateTranslationFiles();
    }

    /**
     *
     */
    public function afterSaveConfiguration(){
        parent::afterSaveConfiguration();
        $this->generateTranslationFiles();
    }


    public function afterUnbind(){
        parent::afterBind();
        $this->deleteTranslationFiles();
    }

    protected function saveTranslationFile($fileContent,$lang){
        $fileContent = html_entity_decode(stripslashes($fileContent));
        $poFile = Translation::getPoFile($fileContent,LangAdapter::Code2Iso($lang));
        $moFile = Translation::getMoFile($fileContent,LangAdapter::Code2Iso($lang));
        $file = fopen('../Locale/'.$lang.'/LC_MESSAGES/custom'.$this->deployment->id.'.po', "w");
        fwrite($file, $poFile);
        fclose($file);

        $file = fopen('../Locale/'.$lang.'/LC_MESSAGES/custom'.$this->deployment->id.'.mo', "w");
        fwrite($file, $moFile);
        fclose($file);


    }

    private function generateTranslationFiles()
    {
        $configContent = $this->checker->getSetting('custom_ar');
        if ($configContent)
        {
            $this->saveTranslationFile($configContent, 'ara');
        }

        $configContent = $this->checker->getSetting('custom_en');
        if ($configContent)
        {
            $this->saveTranslationFile($configContent, 'eng');
        }

        $configContent = $this->checker->getSetting('custom_ur');
        if ($configContent)
        {
            $this->saveTranslationFile($configContent, 'urd');
        }
    }

    private function deleteTranslationFiles(){
        unlink('../Locale/ara/LC_MESSAGES/custom'.$this->deployment->id.'.po');
        unlink('../Locale/eng/LC_MESSAGES/custom'.$this->deployment->id.'.po');
        unlink('../Locale/urd/LC_MESSAGES/custom'.$this->deployment->id.'.po');
        unlink('../Locale/ara/LC_MESSAGES/custom'.$this->deployment->id.'.mo');
        unlink('../Locale/eng/LC_MESSAGES/custom'.$this->deployment->id.'.mo');
        unlink('../Locale/urd/LC_MESSAGES/custom'.$this->deployment->id.'.mo');
    }

}