<?php

namespace Nadrus\Feature;


use Nadrus\Feature;

class EDILogin extends Featureable {

    protected $slug = 'edi_login';

    public function isActivated()
    {
        return $this->activated;

    }

    public static function getLogoutLink()
    {
        $instance = new static();

        if (!$instance->isActivated()) return false;

        return $instance->checker->getSetting('logout_link');
    }

    public static function isActivated__()
    {
        $instance = new static();

        return $instance->isActivated();
    }

    public static function getSetting__($settingName)
    {
        $instance = new static();

        return $instance->checker->getSetting($settingName);

    }

}