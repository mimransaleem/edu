<?php

namespace Nadrus\Feature;



use Carbon\Carbon;
use Nadrus\CakeAdapters\Model\Course;
use Nadrus\Deployment\DBConnection;
use Nadrus\ScheduledTask;
use Nadrus\User;
use Nadrus\Webhook;
use Nadrus\Webhook\Request;

/**
 * Class OnUserCompleteCourseService
 * @package Nadrus\Feature
 */
class OnUserCompleteCourseService extends Featureable {

    /**
     * @var string
     */
    protected $slug = 'on_user_complete_course_service';

    public static function getMaxTrails(){return 100;}
    public static function getTrailIntervalInMinutes(){return 3;}
    /**
     * @param $courseId
     * @param $userId
     * @return bool|null
     */
    public function hitService($courseId, $userId)
    {
        $instance = new static();

        if (!$instance->activated) return;
        //ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
        ini_set("soap.wsdl_cache_enabled", 0);

        try
        {
            $user = User::find($userId);
            $registerId = (!$user->register_id) ? $user->id : $user->register_id;
            $course = Course::find($courseId);
            $now = new \DateTime();
            $attendTime = $now->format('Y-m-d\TH:i:s.0000000P');
            $languages = [1=>'Arabic',2=>'English',3=>'Urdu'];
            $language = $languages[$course->language_id];
            $param = ['regisNumber' => $registerId, 'lectureCode' => $course->title, 'lectureId' => $courseId, 'status' => 1, 'attendTime' => $attendTime, 'attendTimeSpecified' => false, 'lectureLanguage'=>$language];
            if($courseCodeWSField = OnUserCompleteCourseService::CourseCodeWs()){
                $param[$courseCodeWSField]=$course->course_code_ws;
            }
            $message_meta_info =  $instance->getMetaInfoMessage($param);
            $client = new \SoapClient($instance->checker->getSetting('service_end_point'));
            $result = $client->InsertLecure($param);
            $status = ($result->InsertLecureResult->Status);
            $message = "***Success($status)*** " .$message_meta_info;
            self::wslog($message);
            if(is_array($result)|| is_object($result)){
                $response_meta_info = json_encode($result);
                $messageResponse = "***Response($status)*** " .$response_meta_info;
                self::wslog($messageResponse);
            }else{
                self::wslog("Response: $result");
            }
            return $status;
        } catch (\Exception $exception)
        {
            $message = "XXXFailureXXX $status" .$message_meta_info."\n". $exception->getMessage();
            self::wslog($message);
            self::wslog("Exception: $exception");
        }

        return;

    }
    private function getMetaInfoMessage($params){
        $msg = "RequestData: ";
        foreach ($params as $key=>$value)
            $msg .= " [$key]=>".(empty($value)?'FALSE':$value)."  ,";
        return rtrim($msg,',');
    }

    public static function wslog($message)
    {
        \CakeLog::write('webservice', $message);
    }

    public static function wslog2($requestId,$message){ //for Cli
        $logPath = dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/tmp/logs/';
        $logFile = $logPath.'webservice.'.__G('deployment_domain').'.log';
        if(!file_exists($logFile)){
            mkdir($logPath, 0777, true);
        }
        file_put_contents($logFile, Carbon::now()->toDateTimeString()." Webservice($requestId): ".$message.PHP_EOL, FILE_APPEND);
    }

    public static function CourseCodeWs(){
        if(!empty($courseCode = trim(Checker::getSetting__('on_user_complete_course_service','course_code_ws'))))
            return $courseCode;
        return false;

    }

    public static function notificationMinutesTimer(){
        if(!empty($minutes = trim(Checker::getSetting__('on_user_complete_course_service','notification_timer_minutes'))))
            return $minutes;
        return false;

    }

    public static function adminEmail(){
        $email = trim(Checker::getSetting__('on_user_complete_course_service','admin_email'));
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
            return $email;
        return false;

    }

    public static function hitRequest($requestId)
    {
        $webhookRequest = Request::find($requestId);
        if(!$webhookRequest)
            throw  new \Exception('request not found!!');
        $webhookData = json_decode($webhookRequest->data);
        $data = $webhookData->data;
        $userId =$data->user_id;
        $courseId =  $data->course_id;

        if(!$userId || !$courseId )
                    throw new \Exception('param is missing');

        $instance = new static();

        if (!$instance->activated) return;
        //ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
        ini_set("soap.wsdl_cache_enabled", 0);

        try
        {
            $user = User::find($userId);
            $registerId = (!$user->register_id) ? $user->id : $user->register_id;
            $course = Course::find($courseId);
            $now = new \DateTime();
            $attendTime = $now->format('Y-m-d\TH:i:s.0000000P');
            $languages = [1=>'Arabic',2=>'English',3=>'Urdu'];
            $language = $languages[$course->language_id];
            $param = ['regisNumber' => $registerId, 'lectureCode' => $course->title, 'lectureId' => $courseId, 'status' => 1, 'attendTime' => $attendTime, 'attendTimeSpecified' => false, 'lectureLanguage'=>$language];
            if($courseCodeWSField = OnUserCompleteCourseService::CourseCodeWs()){
                $param[$courseCodeWSField]=$course->course_code_ws;
            }
            $message_meta_info =  $instance->getMetaInfoMessage($param);
            $wsUrl = $webhookRequest->webhook->url;
            $wsUrl = $wsUrl?$wsUrl:$instance->checker->getSetting('service_end_point');
            $client = new \SoapClient($wsUrl);
            $result = $client->InsertLecure($param);
            $status = ($result->InsertLecureResult->Status);

            $message =$status? "***Status:True*** ":"***Status:False*** " .$message_meta_info;

            if(is_array($result)|| is_object($result)){
                $response_meta_info = json_encode($result);
                $message .= "\nResponse: ".$response_meta_info;
                self::wslog2($requestId, $message);
            }else{
                $message .= "\nResponse: ".$result;
                self::wslog2($requestId,$message);
            }
            $return['result'] = $status?'success':'fail';
            $return['status'] = $status;
            $return['message'] = $message;
            $return['return'] = $result;
            return $return;
        } catch (\Exception $exception)
        {
            $message = "XXXExceptionXXX " .$message_meta_info."\nMessage: ". $exception->getMessage();
            self::wslog2($requestId,$message);
            $return['result'] = 'fail';
            $return['status'] = $status;
            $return['message'] = $message;
            return $return;
        }

    }


    public function afterUnbind()
    {
        parent::afterUnbind();
        $this->removeCronFromTaskTable();
    }

    public function afterSaveConfiguration(){
        parent::afterSaveConfiguration();
        $this->removeCronFromTaskTable();
        if(self::isActivatedForDeployment__($this->deployment))
            $this->addCronToTaskTable();

    }

    public function afterActivate()
    {
        parent::afterActivate();
        $this->addCronToTaskTable();
    }

    public function afterDeactivate()
    {
        parent::afterDeactivate();
        $this->removeCronFromTaskTable();
    }

    private function addCronToTaskTable(){
        $email = self::getSettingForDeployment__($this->deployment,'admin_email');
        $timer = self::getSettingForDeployment__($this->deployment,'notification_timer_minutes');
        //check if it's already exist just in case
        if(empty($email) || $timer<=0)
            return;
        $cronTask = $this->generateScheduledTask();
        if(ScheduledTask::Where('name',$cronTask->name)->first()){
            $this->removeCronFromTaskTable();
        }
        $cronTask->save();

    }

    private  function removeCronFromTaskTable(){
        $cronTask = $this->generateScheduledTask();

       ScheduledTask::Where('name',$cronTask->name)->delete();

    }

    private function generateScheduledTask(){
        $everyXminute = self::getSettingForDeployment__($this->deployment,'notification_timer_minutes');

        $scheduledTask = new ScheduledTask();
        $scheduledTask->name = 'email:on_user_complete_course_service_check_failed_requests:'.$this->deployment->domain;
        $consolePath = dirname(dirname(dirname(__FILE__))).DS.'bin'.DS .'console';
        $scheduledTask->run = 'bash '. $consolePath.' email:on_user_complete_course_service_check_failed_requests '.$this->deployment->domain;
        $scheduledTask->unit_key = 'minute';
        $scheduledTask->unit_value = $everyXminute;

        return $scheduledTask;
    }

    public static function checkFailedRequests(){
        $adminEmail = self::adminEmail();
        $wsWebhook = Webhook::Where('type','WSDL:OnUserCompleteCourseService')->first();
        if(!$wsWebhook)
            return;
        $requests = $wsWebhook->requests()->where('success',0)->get();
        if(!$requests->count())
            return;

        //prepare params
        $args  =  [
            "{failed_request_counter}"    => $requests->count(),
            "{WEBHOOK_REQUESTS_LINK}"     => __G('SITE_LINK').'admin/webhooks/requests/'.$wsWebhook->id.'?&status=0',
        ];
        if($adminEmail && $requests->count()>0)
            fire(new \Nadrus\Events\SendMailEvent($adminEmail, \Nadrus\Email\TemplateSlugs::on_user_complete_course_service_notification, $args));
    }


    public static function resendFailedRequests(){
        $wsWebhook = Webhook::Where('type','WSDL:OnUserCompleteCourseService')->first();
        if(!$wsWebhook)
            return;
        $requests = $wsWebhook->requests()->where('success',0)->get();
        if(!$requests->count())
            return;

        foreach ($requests as $request){
            $result[] = $request->send();
           sleep(3);
        }
        dd($result);
    }
}