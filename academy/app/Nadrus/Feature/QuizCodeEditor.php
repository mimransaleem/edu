<?php

namespace Nadrus\Feature;


class QuizCodeEditor extends Featureable{

    public $slug = 'quiz_code_editor';
    private $langs = ['html'=>'HTML','js'=>'JavaScript','css'=>'CSS','php'=>'PHP','py'=>'Python'];
    
    function get_supported_lang(){
       $supported =  \Nadrus\Feature\Checker::getSetting__($this->slug,'supported_lang');
       $result = [];
       $arr = explode(',', $supported);
       foreach ($arr as $k=>$v){
           $ext = trim($v);
           if(array_key_exists($ext, $this->langs)){
               $result[$ext] = $this->langs[$ext];
           }
       }
       return $result;
    }

    static function get_supported_lang__(){
        $instance = new self();
        return $instance->get_supported_lang();
    }
}

