<?php

namespace Nadrus\Feature;


class ReCAPTCHA extends Featureable {

    public $slug = 'recaptcha';


    private function validate($secret)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $paramaters = 'secret=' . $secret . '&response=' . $_REQUEST['g-recaptcha-response'] . '&remoteip=' . $_SERVER['HTTP_X_FORWARDED_FOR'];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramaters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        $response = json_decode($response);

        return (isset($response->success) && $response->success);
    }

    public function validatePage($pageName)
    {
        if (!$this->activated) return true;

        if (!$this->checker->getSetting($pageName)) return true;


        return $this->validate($this->checker->getSetting('secret_key'));
    }


    public static function validatePage__($pageName)
    {
        $instance = new static();

        return $instance->validatePage($pageName);
    }

}