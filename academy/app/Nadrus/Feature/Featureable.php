<?php

namespace Nadrus\Feature;


use Nadrus\Deployment;
use Nadrus\Deployment\Configuration;
use Nadrus\Feature;

/**
 * Class Featureable
 * @package Nadrus\Feature
 */
Abstract class Featureable {

    /**
     * @var Feature
     */
    protected $feature;

    /**
     * @var Deployment
     */
    protected $deployment;

    /**
     * @var Checker
     */
    protected $checker;
    /**
     * @var bool
     */
    protected $activated;


    /**
     *
     */
    public function __construct()
    {
        $this->checker = new Checker($this->slug);
        $this->activated = $this->checker->isActivated();
    }

    /**
     * @param Feature $feature
     */
    public function setFeature(Feature $feature)
    {
        $this->feature = $feature;
    }

    /**
     * @param Deployment $deployment
     */
    public function setDeployment(Deployment $deployment)
    {
        $this->deployment = $deployment;
        $this->checker->setDeployment($deployment);
        $this->activated = $this->checker->isActivated();
    }

    /**
     * @throws \Exception
     */
    public static function checkFeatureActivation()
    {
        $instance = new static();
        if (!$instance->isActivated()) throw new \Exception('Feature ' . $instance->slug . ' is not Activated ! please
        Activate it and Try Again.');
    }

    /**
     * this function will be used to force loading settings from yaml file directly.
     */
    public function loadFromYaml(){
        //$this->checker->loadFromYaml($this->deployment->domain);
    }


    /**
     * @return mixed
     */
    public function beforeBind()
    {

    }

    /**
     * @return mixed
     */
    public function afterBind()
    {

    }

    /**
     * @return mixed
     */
    public function beforeUnbind()
    {

    }

    /**
     * @return mixed
     */
    public function afterUnbind()
    {

    }

    /**
     * @return mixed
     */
    public function beforeActivate()
    {

    }

    /**
     * @return mixed
     */
    public function afterActivate()
    {

    }

    /**
     * @return mixed
     */
    public function beforeDeactivate()
    {

    }

    /**
     * @return mixed
     */
    public function afterDeactivate()
    {

    }

    /**
     * @return mix
     */
    public function afterSaveConfiguration()
    {

    }

    /**
     * @return bool
     */
    public function isActivated(){
        return $this->activated;
    }

    /**
     * @return mixed
     */
    public static function  isActivated__(){
    return (new static())->isActivated();
    }

    //TODO This method temporary and need to removed and integrated with isActivated function
    public static function isActivatedForDeployment__(Deployment $deployment){
            $instance = new static();
            $instance->setDeployment($deployment);
            $instance->checker = new Checker($instance->slug);
            return $instance->checker->isActivated();

    }
    //TODO This method temporary and need to removed and integrated with isActivated function
    public static function getSettingForDeployment__(Deployment $deployment,$setting_name){
            $instance = new static();
            $instance->setDeployment($deployment);
            $instance->checker = new Checker($instance->slug);
            $instance->loadFromYaml();
        return $instance->checker->getSetting($setting_name);
    }

    public static function getSetting($settingName = '')
    {
        if(!$settingName) return null;

        return (new static())->checker->getSetting($settingName);
    }

}