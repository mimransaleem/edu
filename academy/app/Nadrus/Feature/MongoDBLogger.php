<?php

namespace Nadrus\Feature;


use MongoDB\Client;

class MongoDBLogger extends Featureable {

    public $slug = 'mongodb_logger';

    public $host;
    public $database;
    private $port;
    private $username;
    private $password;
    public $client;

    public function __construct()
    {
        parent::__construct();
        if($this->checker->isActivated()){
            $this->host = $this->checker->getSetting('host');
            $this->database = $this->checker->getSetting('database');
            $this->port = $this->checker->getSetting('port');
            $this->username = $this->checker->getSetting('username');
            $this->password = $this->checker->getSetting('password');
            $this->iniClient();
        }

    }

    public function iniClient()
    {
        if ($this->username && $this->password)
            $authString = $this->username . ':' . $this->password . '@';
        else
            $authString = '';
        $this->client = new Client('mongodb://'.$authString.$this->host.':'.$this->port.'/', ['connectTimeoutMS'=>5000,'socketTimeoutMS'=>5000]);
    }

    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

}