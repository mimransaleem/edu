<?php

namespace Nadrus\Feature;



class ElasticSearchLogger extends Featureable {

    public $slug = 'elastic_logger';

    public $host;
    public $client;

    public function __construct()
    {
        parent::__construct();
        if($this->checker->isActivated()){
            $this->host = $this->checker->getSetting('host');
            $this->iniClient();
        }

    }

    public function iniClient()
    {
        $this->client = \Elasticsearch\ClientBuilder::create()
            ->setHosts([$this->host])
            ->build();
    }

    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

}