<?php

namespace Nadrus\Feature;

use Nadrus\User;
use Nadrus\UserOtpLogin;

/**
 * Class OTPLogin
 * @package Nadrus\Feature
 */
class OTPLogin extends Featureable {

    /**
     * @var string
     */
    public $slug = 'otp_login';
    public $settings =[];

    private $registerId;
    private $userId;
    private $msg='';
    private $isDebug= false;

    const SETTINGS = ['endpoint','expire','service_type','soap_method','soap_hash_key','locale_key','per_session'];


    private function getSettings(){
        if (!$this->activated){
            throw new \Exception('OTP Login Not Available');
        }
        $this->settings =  array_combine(self::SETTINGS,$this->checker->getSettings(self::SETTINGS));
    }


    private function setIds($registerId){

        $user = User::whereRegister($registerId)->first();
        if(!$user){
            throw new \Exception(__d('labels','REGISTER_ID_NOT_FOUND'));
        }else{
            $this->userId = $user->id;
            $this->registerId=$registerId;
        }

    }

    private function setLoginCode(){

        UserOtpLogin::where('register_id', $this->registerId)->delete();

        $code = rand(100000,999999);
        $this->code=$code;
        $this->msg = __d('messages',$this->settings['locale_key'],[$code]);

        $otp = new UserOtpLogin();
        $otp->register_id = $this->registerId;
        $otp->login_code = $code;
        $otp->expire_at = $this->settings['expire'];
        $otp->save();
    }

    private function requestSoapService(){
        $this->setLoginCode();

        $func =  $this->settings['soap_method'];
        $resKey =  $this->settings['soap_method'].'Result';
        $param = ['regisNumber'=>$this->registerId,'smsText'=>$this->msg,'hashKey'=>$this->settings['soap_hash_key']];

        try{
            if($this->isDebug){
                $serviceResult = [];
            }else{
                ini_set("soap.wsdl_cache_enabled", 0);
                $client = new \SoapClient($this->settings['endpoint']);
                $serviceResult = $client->$func($param);

                if( is_object($serviceResult)){
                    $serviceResult = json_decode(json_encode($serviceResult), true);
                }

            }
            if(is_array($serviceResult)){
                if(!$this->isDebug && !$serviceResult[$resKey]['Status']){
                    throw new \Exception($serviceResult[$resKey]['Message']);
                }
                $result = ['expire'=>$this->settings['expire'],'per_session'=>$this->settings['per_session']];
                if($this->isDebug){
                    $result['code'] = $this->code;
                    $result['msg'] = $this->msg;
                }
                return $result;

            }else{
                throw new \Exception(__d('labels','UNEXPECTED_ERROR_TRY_AGAIN'));
            }
        }catch(\SoapFault $exception){
            throw new \Exception($exception->getMessage());
        }

    }


    static function requestCode($registerId,$isDebug=false){

        $instance = new static();
        $instance->getSettings();
        $instance->setIds($registerId);
        $instance->isDebug = $isDebug;
        if($instance->settings['service_type']=='soap'){
            return $instance->requestSoapService();
        }else{
            throw new \Exception('OTP Service Type Not ready yet.');
        }


    }

    public static function login($registerId,$code){
        $instance = new static();
        $instance->getSettings();
        $instance->setIds($registerId);
        $otp = UserOtpLogin::login($registerId,$code)->first();
        if($otp){
            return $otp->user;
        }else{
            throw new \Exception(__d('labels','OTP_LOGIN_CODE_INCORRECT'));
        }
    }

}