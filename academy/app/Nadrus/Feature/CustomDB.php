<?php

namespace Nadrus\Feature;

use Nadrus\Deployment\DBCloner;
use Nadrus\Feature\Checker;

class CustomDB extends Featureable {

    protected $slug = 'custom_db';

    public function afterActivate()
    {
        parent::afterActivate();
        $dbName = $this->checker->getSetting('db_name');
        $dbUser = $this->checker->getSetting('db_user');
        $dbPassword = $this->checker->getSetting('db_password');
        
        //$cloner = new DBCloner($dbName, $dbUser, $dbPassword);
        //$cloner->doClone();
    }
}