<?php

namespace Nadrus;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'package_text', 'price', 'status','is_bundle'];

    public $timestamps = true;

    public function features()
    {
        return $this->belongsToMany(Feature::class, 'package_features')->withTimestamps();
    }

    public function bundlePackage(){
        return $this->belongsTo(Package::class, 'bundle_package_id');
    }
}