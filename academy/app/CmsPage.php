<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class CmsPage extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table="cms_pages";
    protected $fillable = ['name', 'content', 'metatitle', 'seourl', 'metadesc', 'metakeyword', 'status', 'showinfooter', 'showinleft'];
    public $sortable = ['name', 'metatitle', 'seourl', 'status', 'created_at'];

}
