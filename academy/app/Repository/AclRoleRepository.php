<?php

namespace App\Repository;

use Silber\Bouncer\Bouncer;

/**
 * Class AclRoleRepository
 * @package App\Repository
 */
class AclRoleRepository
{
    /**
     * @var Bouncer
     */
    private $__bouncer;

    public function __construct(Bouncer $bouncer)
    {

        $this->__bouncer = $bouncer;
    }

    /**
     * @param $parameters
     * @return mixed
     */
    public function create($parameters)
    {
        $role = $this->__bouncer->role()->firstOrCreate([
            'name' => $parameters['name'],
            'title' => $parameters['title'],
        ]);
        $role->slug = $parameters['slug'];
        $role->save();
        return $role;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Role[]
     */
    public function getAll()
    {
        return $this->__bouncer->role()->all();
    }

    /**
     * @param $parameters
     * @return \Silber\Bouncer\Conductors\Lazy\ConductsAbilities|null
     */
    public function addRoleAbilities($parameters){

        return $this->__bouncer->allow($parameters['role'])->to($parameters['abilities']);
    }

    /**
     * @param $admin
     * @param $role
     * @return mixed
     */
    public function addUserRole($admin, $role){

        return $admin->assign($role);
    }

    /**
     * @param $roleId
     * @return mixed
     */
    public function removeRole($roleId){

        $role = $this->__bouncer->role()->find($roleId);
        return $role->delete();
    }

    /**
     * @param $parameters
     * @return mixed
     */
    public function editRole($parameters)
    {
        $role = $this->__bouncer->role()->updateOrCreate(
            [
                'id' => $parameters['id']
            ],
            [
                'name'  => $parameters['name'],
                'title' => $parameters['title']
            ]);
        $role->slug = $parameters['slug'];
        $role->save();
        return $role;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function checkSlugExists($slug){

        return Bouncer::role()->where('slug', '=', $slug)->first();
    }

    /**
     * @param $parameters
     * @return bool|\Silber\Bouncer\Conductors\Lazy\ConductsAbilities
     */
    public function removeRoleAbility($parameters){

        return $this->__bouncer->disallow($parameters['roleName'])->to($parameters['abilityName']);
    }

    /**
     * @param $admin
     * @param $role
     * @return mixed
     */
    public function removeUserRole($admin, $role){

        return $admin->retract($role);
    }
}
