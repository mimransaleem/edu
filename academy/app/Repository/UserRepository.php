<?php

namespace App\Repository;

use App\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository
{
    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get(){
        return \App\User::whereNotIn('role', ['student'])->get();
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function getUsers($ids){
        return \App\User::whereIn('id', $ids)->get();
    }

    /**
     * @param $id
     */
    public function getUserById($id){
        return \App\User::where('id', $id)->first();
    }

    public function getUsersBySearchValue($searchVal){
        return \App\User::where('name', 'like', "%$searchVal%")
            ->orWhere('email', 'like', "%$searchVal%")
            ->get();
    }

    public function getUsersWithRole($slug='demo_admin'){
        $result = [];
        $users = $this->get();
        foreach ($users as $user) {
            $slugArr = $user->roles->pluck('slug')->toArray();
            if(in_array($slug, $slugArr)) {
                $result[$user->id] = $user;
            }

        }
        return $result;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteUser($id)
    {
        return User::withBanned()->where('id', $id)->delete();
    }
}
