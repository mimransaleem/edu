<?php

namespace App\Repository;


use Silber\Bouncer\Bouncer;

class AclAbilityRepository
{
    /**
     * @var Bouncer
     */
    private $__bouncer;

    public function __construct(Bouncer $bouncer)
    {

        $this->__bouncer = $bouncer;
    }

    public function getAll()
    {
        return $this->__bouncer->ability()->all();
    }
}
