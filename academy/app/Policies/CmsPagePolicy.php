<?php

namespace App\Policies;

use App\CmsPage;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CmsPagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any cms pages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the cms page.
     *
     * @param  \App\User  $user
     * @param  \App\CmsPage  $cmsPage
     * @return mixed
     */
    public function view(User $user, CmsPage $cmsPage)
    {
        //
    }

    /**
     * Determine whether the user can create cms pages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role == 'admin';
    }

    /**
     * Determine whether the user can update the cms page.
     *
     * @param  \App\User  $user
     * @param  \App\CmsPage  $cmsPage
     * @return mixed
     */
    public function update(User $user, CmsPage $cmsPage)
    {
        //
    }

    /**
     * Determine whether the user can delete the cms page.
     *
     * @param  \App\User  $user
     * @param  \App\CmsPage  $cmsPage
     * @return mixed
     */
    public function delete(User $user, CmsPage $cmsPage)
    {
        //
    }

    /**
     * Determine whether the user can restore the cms page.
     *
     * @param  \App\User  $user
     * @param  \App\CmsPage  $cmsPage
     * @return mixed
     */
    public function restore(User $user, CmsPage $cmsPage)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the cms page.
     *
     * @param  \App\User  $user
     * @param  \App\CmsPage  $cmsPage
     * @return mixed
     */
    public function forceDelete(User $user, CmsPage $cmsPage)
    {
        //
    }
}
