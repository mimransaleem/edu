<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "countries";

    protected $fillable = ['name', 'name_ar', 'name_ur', 'code', 'status'];

    function deploymentUser(){
        return $this->hasMany(DeploymentUser::class);
    }
}
