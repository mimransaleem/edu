<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddEditRole;
use App\Services\AclAbilityService;
use App\Services\AclRoleService;
use App\Services\UserService;
use Illuminate\Http\Request;

class AclController extends Controller
{
    protected $__aclRoleService;
    protected $__aclAbilityService;
    protected $__userService;
    /**
     * @var AclAbilityService
     */
    private $aclAbilityService;

    public function __construct(
        AclRoleService $aclRoleService,
        AclAbilityService $aclAbilityService,
        UserService $userService
    )
    {
        $this->__aclRoleService = $aclRoleService;
        $this->__aclAbilityService = $aclAbilityService;
        $this->__userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->__aclRoleService->getAll();
        $abilities = $this->__aclAbilityService->getAll();
        $admins = $this->__userService->getAll();

        return view('acl.index',[
            'roles' => $roles,
            'abilities' => $abilities,
            'admins' => $admins,
        ]);
//        dd($roles, $abilities, $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/acl-manage')->with('success','Role created successfully!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->__aclRoleService->create($data);
        return redirect('/acl-manage')->with('success','Role created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addRole(AddEditRole $request)
    {

        //try {
//            dd('sdfsdf');
//            $data = $request->validated();
            $data = $request->all();
            $this->__aclRoleService->create($data);

            return response()->json([
                'status' => 1,
                'message' => 'Role created successfully!'
            ]);

        /*} catch (\Exception $exception) {

            return response()->json([
                'status'  => 0,
                'message' => $exception->getMessage()
            ]);
        }*/
    }

    public function editRole(AddEditRole $request)
    {

            $data = $request->validated();
            $this->__aclRoleService->editRole($data);

            session()->flash('success' , 'Role updated successfully!');
            return response()->json([
                'message' => 'Role updated successfully!'
            ]);

    }

    public function removeRole(Request $request)
    {
        $data = $request->validate([
            'id' => 'required'
        ]);

        $this->__aclRoleService->removeRole($data);

        session()->flash('success' , 'Role deleted successfully!');
        return response()->json([
            'message' => 'Role deleted successfully!'
        ]);
    }

    public function addUserRole(Request $request)
    {
        $data = $request->all();
        $this->__aclRoleService->addUserNewRole($data);

        session()->flash('success' , 'Role added for the user successfully!');
        return response()->json([
            'status' => 1,
            'message' => 'Role added for the user successfully!'
        ]);
    }

    public function removeUserRole(Request $request)
    {
        $data = $request->all();
        $this->__aclRoleService->removeUserRole($data);

        session()->flash('success' , 'User role removed successfully!');
        return response()->json([
            'status' => 1,
            'message' => 'User role removed successfully!'
        ]);
    }

    public function addRoleAbilities(Request $request)
    {
        $data = $request->all();
        $this->__aclRoleService->addRoleAbilities($data);

        session()->flash('success' , 'Ability added for the role '.$data['role'].' successfully!');
        return response()->json([
            'status' => 1,
            'message' => 'Ability added for the role '.$data['role'].' successfully!'
        ]);
    }

    public function removeRoleAbility(Request $request)
    {
        $data = $request->all();
        $this->__aclRoleService->removeRoleAbility($data);

        session()->flash('success' , 'Ability removed for the role successfully!');
        return response()->json([
            'status' => 1,
            'message' => 'Ability removed for the role successfully!'
        ]);
    }
}
