<?php

namespace App\Http\Controllers;

use App\Services\FeatureService;
use App\Services\PackageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nadrus\Feature;
use Nadrus\Package;

class PackagesController extends Controller
{
    /**
     * @var PackageService
     */
    protected $__packageService;

    public function __construct(PackageService $packageService)
    {
        $this->__packageService = $packageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = $this->__packageService->getAll();
        $features = app(FeatureService::class)->getAll();
        $roles = Auth::user()->roles->pluck('slug')->toArray();

        return view('packages.index', [
            'packages' => $packages,
            'features' => $features,
            'currentUserRoles' => $roles
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('packages.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = $request->all();
        unset($package['_token']);
        $this->__packageService->save($package);
        return redirect('/packages')->with('success','Package created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = $this->__packageService->getById($id);
        return view('packages.edit', [
            'id' => $id,
            'package' => $package
        ]);
    }

    public function feature($id)
    {
        $packages = $this->__packageService->getAll();
        $features = app(FeatureService::class)->getAll();

        $package = $this->__packageService->getById($id);
        return view('packages.features', [
            'packages' => $packages,
            'features' => $features,
            'package' => $package,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = $request->all();

        unset($package['_token']);
        unset($package['_method']);
        $this->__packageService->update($package, $id);
        return redirect('/packages')->with('success','Package updated successfully!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateAction(Request $request)
    {
        $data = $request->get('data');
        if ( isset($data['Admin']['options']) ) {
            if (isset($data['id']) && count($data['id']) > 0) {
                if ($data['Admin']['options'] == 'Delete') {
                    foreach ($data['id'] as $id):
                        $this->__packageService->delete($id);
                    endforeach;
                } elseif ($data['Admin']['options'] == 'Active') {
                    foreach ($data['id'] as $id):
                        $this->update($id, [
                            'status' => 0
                        ]);
                    endforeach;
                } elseif ($data['Admin']['options'] == 'Inactive') {
                    foreach ($data['id'] as $id):
                        $this->update($id, [
                            'status' => 1
                        ]);
                    endforeach;
                }
            } else {
                return redirect('/packages')->with('error',__('labels.INVALID_USER_SELECTION'));
            }

        } else {
            return redirect('/packages')->with('error',__('labels.INVALID_USER_SELECTION'));
        }

        return redirect('/packages')->with('success',__('labels.PACKAGE_ACTION_SUCCESS', ['action' => $data['Admin']['options']]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->__packageService->delete($id);
        return redirect('/packages')->with('success',__('labels.PACKAGE_ACTION_SUCCESS', ['action' => 'deleted']));
    }

    public function addPackageFeatures(Request $request)
    {
        $data = $request->all();
        $package = $this->__packageService->addPackageFeatures($data);

        session()->flash('success' , 'Feature added for the package '.$package['name'].' successfully!');
        return response()->json([
            'status' => 1,
            'message' => 'Feature added for the package '.$package['name'].' successfully!'
        ]);
    }

    public function removePackageFeature(Package $package, Feature $feature)
    {

        $this->__packageService->removePackageFeature($package,$feature);

        session()->flash('success' , 'Feature removed from the package '.$package['name'].' successfully!');
        return response()->json([
            'status' => 1,
            'message' => 'Feature removed from the package '.$package['name'].' successfully!'
        ]);
    }
}
