<?php

namespace App\Http\Controllers;

use App\Mail\DeploymentWizardMail;
use App\Services\DeploymentUserService;
use App\Services\PackageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DeploymentWizardController extends Controller
{
    /**
     * @var DeploymentUserService
     */
    private $__deploymentUserService;
    /**
     * @var PackageService
     */
    private $__packageService;

    public function __construct(DeploymentUserService $deploymentUserService, PackageService $packageService)
    {

        $this->__deploymentUserService = $deploymentUserService;
        $this->__packageService = $packageService;
    }

    public function index()
    {
        $id = \request()->get('id');
        $title = 'حساب إنتاجي جديد';
        if(isset($id)){
            $step = \request()->get('step');
            $deploymentUser = $this->__deploymentUserService->getById($id);
            $bundles = $this->__packageService->getAllBundles();
            $countries = $this->__deploymentUserService->getCountriesList();

            return view('deployment_wizard.index-'.$step, [
                'id' => $id,
                'title' => $title,
                'deploymentUser' => $deploymentUser,
                'bundles' => $bundles,
                'countries' => $countries
            ]);
        } else {

            return view('deployment_wizard.index', ['title' => $title]);
        }
    }

    public function store(Request $request)
    {
        $deploymentUser = $request->all();
        unset($deploymentUser['_token']);
        $deploymentUser = $this->__deploymentUserService->saveSignUpForm($deploymentUser);
        return redirect('/deployment-wizard?id='.$deploymentUser->id.'&step=2')->with('success','Sign up successfully!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deploymentUser = $request->all();
        $step = $deploymentUser['step'];

        unset($deploymentUser['_token']);
        unset($deploymentUser['_method']);
        unset($deploymentUser['step']);

        $this->__deploymentUserService->update($deploymentUser,$id,$step);

        if($step == 4) {

            $deploymentUser = $this->__deploymentUserService->getById($id);

//            Mail::to([$deploymentUser->email])
            Mail::to([$deploymentUser->email, 'admin@intajy.com', 'sales@intajy.com'])
                ->send(new DeploymentWizardMail($deploymentUser, 'Thanks for register on intajy.com!'));

            return redirect('/deployment-wizard?id='.$id.'&step=4')->with('success','Thanks for register on intajy.com!');
        }

        return redirect('/deployment-wizard?id='.$id.'&step='.($step+1));
    }
}
