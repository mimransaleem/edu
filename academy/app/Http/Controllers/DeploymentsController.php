<?php

namespace App\Http\Controllers;

use App\Services\DeploymentService;
use App\Services\FeatureService;
use App\Services\PackageService;
use Illuminate\Http\Request;
use Nadrus\Deployment\Redis;
use Nadrus\Deployment\Yaml;

/**
 * Class DeploymentsController
 * @package App\Http\Controllers
 */
class DeploymentsController extends Controller
{

    /**
     * @var DeploymentService
     */
    private $__deploymentService;
    /**
     * @var FeatureService
     */
    private $__featureService;
    /**
     * @var PackageService
     */
    private $__packageService;

    /**
     * DeploymentsController constructor.
     * @param DeploymentService $deploymentService
     * @param PackageService $packageService
     */
    public function __construct(DeploymentService $deploymentService, PackageService $packageService, FeatureService $featureService)
    {
        $this->__deploymentService = $deploymentService;
        $this->__packageService = $packageService;
        $this->__featureService = $featureService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//        dd(WWW_ROOT);
        return view('deployments.index',[
            'deployments' => $this->__deploymentService->getAll(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('deployments.create',[
            'packages' => $this->__packageService->getAll(),
        ]);

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view('deployments.edit',[
            'id' => $id,
            'deployment' => $this->__deploymentService->get($id),
            'packages' => $this->__packageService->getAll(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $deploymentAtrr = $request->get('data')['Deployment'];
        $this->__deploymentService->update($deploymentAtrr);
        return redirect('/deployments')->with('success','Deployment has been updated successfully!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $deploymentAtrr = $request->get('data')['Deployment'];
        $this->__deploymentService->create($deploymentAtrr);
        return redirect('/deployments')->with('success','Deployment has been created successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function activate($id)
    {
        $result =  $this->__deploymentService->activate($id);
        return $result ? redirect('/deployments')->with('success','Deployment has been activated successfully')
                       : redirect('/deployments')->with('failed','Deployment was already activated!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deactivate($id)
    {
        $result = $this->__deploymentService->deactivate($id);
        return $result ? redirect('/deployments')->with('success','Deployment has been deactivated successfully')
                       : redirect('/deployments')->with('failed','Deployment was already deactivated!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function features($id)
    {
        $deployment = $this->__deploymentService->get($id);

        return view('deployments.features',[
            'id' => $id,
            'deployment' => $deployment,
            'states' =>$deployment->states,
            'newFeatures' =>  $deployment->newFeatures(FALSE),
        ]);
    }

    /**
     * @param $id
     * @param $featureId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function config($deploymentId, $featureId)
    {
        $deployment = $this->__deploymentService->get($deploymentId);
        $state   = $deployment->getStateOfFeature($featureId);
        $configs = $deployment->getConfigsOfFeature($featureId);
        $feature    = $this->__featureService->get($featureId);

        $icons = [
            'text'     => 'fa-cog',
            'textarea' => 'file-text-o',
            'number'   => 'fa-cog',
            'file'     => 'fa-file-o',
            'color'    => 'fa-eyedropper',
            'html'     => 'fa-code',
            'select'   => 'fa-list',
            'checkbox' => 'fa-cog',
            'email'    => 'fa-email'
        ];
        $supported_image = array('gif', 'jpg', 'jpeg', 'png');

        return view('deployments.config',[
            'id' => $deploymentId,
            'featureId' => $featureId,
            'deployment' => $deployment,
            'feature' => $feature,
            'state' => $state,
            'configs' => $configs,
            'icons' => $icons,
            'supported_image' => $supported_image,
        ]);
    }

    /**
     * @param Request $request
     * @param $deploymentId
     * @param $featureId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateConfig(Request $request, $deploymentId, $featureId)
    {
        $configurations = $request->get('data')['Configurations'];

        if( request()->get('reset_config')){

            $deployment = $this->__deploymentService->resetConfig($deploymentId, $featureId);

        } else {

            $deployment = $this->__deploymentService->updateConfig($configurations, $deploymentId, $featureId);
        }

        Yaml::dump($deployment);

        if($deployment)
            return redirect()->route('deployments.features', $deploymentId)->with('success', __('messages.configurations_saved'));
        else
            return redirect()->route('deployments.config', $deploymentId, $featureId)->with('error', __('messages.configurations_saving_error'));

    }

    public function featureBind($deployment_id,$feature_id)
    {
        $result = $this->__deploymentService->featureBind($deployment_id,$feature_id);
        return $result ? redirect("/deployments/$deployment_id/features")->with('success','Feature has been bound successfully')
                       : redirect("/deployments/$deployment_id/features")->with('failed','Feature could not be bound!');
    }

    public function featureUnbind($deployment_id,$feature_id)
    {
        $result = $this->__deploymentService->featureUnbind($deployment_id,$feature_id);
        return $result ? redirect("/deployments/$deployment_id/features")->with('success','Feature has been deactivated successfully')
                       : redirect("/deployments/$deployment_id/features")->with('failed','Feature could not be activated!');
    }

    public function featureActivate($deployment_id,$feature_id)
    {
        $result = $this->__deploymentService->featureActivate($deployment_id,$feature_id);
        return $result ? redirect("/deployments/$deployment_id/features")->with('success','Feature has been activated successfully')
                       : redirect("/deployments/$deployment_id/features")->with('failed','Feature could not be activated!');
    }

    public function featureDeactivate($deployment_id,$feature_id)
    {
        $result = $this->__deploymentService->featureDeactivate($deployment_id,$feature_id);
        return $result ? redirect("/deployments/$deployment_id/features")->with('success','Feature has been deactivated successfully')
                       : redirect("/deployments/$deployment_id/features")->with('failed','Feature could not be activated!');
    }

    /**
     * @param $id
     */
    public function getDeploymentSettingFromRedis($id)
    {
        $deployment = $this->__deploymentService->get($id);

        $redisObj = new Redis($deployment);
        $redisSettings = $redisObj->getSettings();
        echo $deployment->domain;
        if ($redisSettings) {

            dd($redisSettings);
        } else {
            echo "<br><br><strong>Please update deployment configurations and check it again!</strong>";
        }
    }
}
