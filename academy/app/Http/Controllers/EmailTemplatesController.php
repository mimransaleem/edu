<?php

namespace App\Http\Controllers;

use App\Services\DeploymentService;
use App\Services\EmailTemplateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Nadrus\Email;
use Nadrus\Email\Custom;
use Nadrus\Email\Template;

class EmailTemplatesController extends Controller
{
    /**
     * @var EmailTemplateService
     */
    private $__emailTemplateService;
    private $__deploymentService;

    public function __construct(EmailTemplateService $emailTemplateService, DeploymentService $__deploymentService)
    {

        $this->__emailTemplateService = $emailTemplateService;
        $this->__deploymentService = $__deploymentService;
    }

    public function index()
    {
        $rows = $this->__emailTemplateService->getPage(10);
        $rows->withPath('emails');
        $defaultSender = '';
        if (!Session::get('admin.Admin.supper')) {

            $defaultSender = env('MAIL_SENDER');
        }

        return view('email_templates.index', [
            'rows' => $rows,
            'defaultSender' => $defaultSender,
            'page' => request()->query('page')
        ]);
    }

    public function create()
    {
        $page = request()->query('page');
        $languages = $this->__emailTemplateService->getLanguages()->pluck('title', 'iso')->toArray();

        return view('email_templates.add', [

            'email' => null,
            'varList' => null,
            'languages' => $languages,
            'templates' => Template::getTemplateOptions(),
            'page' => $page
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $package = $request->all();
        unset($package['_token']);
        $this->__emailTemplateService->save($package);
        return redirect('/emails')->with('success','Email template created successfully!');
    }

    public function edit(Email $email)
    {
        $page = request()->query('page');
        $languages = $this->__emailTemplateService->getLanguages()->pluck('title', 'iso')->toArray();

        return view('email_templates.edit', [
            'id' => $email->id,
            'email' => $email,
            'varList' => $email->getVariablesList(),
            'languages' => $languages,
            'templates' => Template::getTemplateOptions(),
            'page' => $page,
            'customize' => false
        ]);
    }

    public function customize(Email $email)
    {
        //check if there is customized version of this email
        $customized = Custom::findCustomizedBy($email->id);
//        dd($email->id, $customized);
        if($customized)
            return redirect('emails/custom/edit/' . $customized->id);

        $page = request()->query('page');
        $languages = $this->__emailTemplateService->getLanguages()->pluck('title', 'iso')->toArray();

        return view('email_templates.edit', [
            'id' => $email->id,
            'email' => $email,
            'varList' => $email->getVariablesList(),
            'languages' => $languages,
            'templates' => Template::getTemplateOptions(),
            'page' => $page,
            'customize' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);

        if($data['customize']){
            $deployment = $this->__deploymentService->getByDomain(env('APP_DEPLOYMENT_DOMAIN'));
            $data['deployment_id'] = $deployment['id'];

            Custom::create($data);

            return redirect('/emails')->with('success','Email template customized successfully!');
        }

        $this->__emailTemplateService->update($data, $id);
        return redirect('/emails')->with('success','Email template updated successfully!');
    }

    public function custom(){

        $rows = $this->__emailTemplateService->getCustomPage(10);
//        $rows = Custom::with('template')->where('deployment_id',deployment_id)->get();
        $rows->withPath('emails.custom');
        $defaultSender = '';
        if (!Session::get('admin.Admin.supper')) {
            $defaultSender = env('MAIL_SENDER');
        }

        return view('email_templates.custom', [
            'rows' => $rows,
            'defaultSender' => $defaultSender,
            'page' => request()->query('page')
        ]);
    }

    public function customEdit(Custom $email){

        if(!$email) $this->redirect('/admin/emails/custom');
        if(\request()->get('custom_edit')){
            if(isset($this->data['reset'])){
                $customizedTemplateEmail = $email->findTemplateEmail();
                $email->delete();
                $this->redirect('/admin/emails/customize/'.$customizedTemplateEmail->id);
            }
        }

        $page = request()->query('page');
        $languages = $this->__emailTemplateService->getLanguages()->pluck('title', 'iso')->toArray();

        return view('email_templates.custom_edit', [
            'id' => $email->id,
            'email' => $email,
            'varList' => $email->getVariablesList(),
            'languages' => $languages,
            'templates' => Template::getTemplateOptions(),
            'page' => $page,
            'custom_update' => true
        ]);
    }

    public function customUpdate(Request $request, $id)
    {
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);

        if($data['custom_update']){

            unset($data['custom_update']);
            $data['deployment_id'] = deployment_id;
            $email = Custom::find($id);

            $email->subject = $data['subject'];
            $email->body = $data['body'];
            $email->save();

            return redirect('/emails')->with('success','Updated successfully!');
        }
    }

    public function settings(Feature $feature)
    {
        return view('features.settings', [
            'id' => $feature->id,
            'feature' => $feature,
            'settings' => $feature->settings,
        ]);
    }
}
