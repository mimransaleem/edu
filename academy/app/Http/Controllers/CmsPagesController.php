<?php

namespace App\Http\Controllers;

use App\CmsPage;
use App\Services\CmsPageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CmsPagesController extends Controller
{
    /**
     * @var CmsPageService
     */
    protected $__cmsPagesService;

    public function __construct(CmsPageService $cmsPageService)
    {
        $this->__cmsPagesService = $cmsPageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\request('searchval')){

            $searchValue = \request('searchval');
            $cmspages = $this->__cmsPagesService->getBySearchValue($searchValue);
        } else {
            $cmspages = $this->__cmsPagesService->getAll();
        }
        return view('cmspages.index', [
            'cmspages' => $cmspages
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', CmsPage::class)) {

            $languages = $this->__cmsPagesService->getLanguages()->pluck('title', 'id')->toArray();
            return view('cmspages.add', [
                'languages' => $languages,
            ]);

        } else {

            return redirect('/cmspages')->with('error','Not Authorized!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = $request->all();
        unset($package['_token']);
        $this->__cmsPagesService->save($package);
        return redirect('/cmspages')->with('success','Page created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->__cmsPagesService->getById($id);
        $languages = $this->__cmsPagesService->getLanguages()->pluck('title', 'id')->toArray();
        return view('cmspages.edit', [
            'id' => $id,
            'page' => $page,
            'languages' => $languages,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = $request->all();
        if (Gate::allows('isAdmin')) {
            unset($package['_token']);
            unset($package['_method']);
            $this->__cmsPagesService->update($package, $id);
            return redirect('/cmspages')->with('success', 'Page updated successfully!');
        } else {
            return redirect('/cmspages')->with('error', 'You are not allowed to update this page!');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateAction(Request $request)
    {
        $data = $request->get('data');
        if ($this->authorize('isAdmin')) {

            if (isset($data['Admin']['options'])) {

                if (isset($data['id']) && count($data['id']) > 0) {

                    if ($data['Admin']['options'] == 'Delete') {
                        foreach ($data['id'] as $id):
                            $this->__cmsPagesService->delete($id);
                        endforeach;
                    } elseif ($data['Admin']['options'] == 'Active') {
                        foreach ($data['id'] as $id):
                            $this->__cmsPagesService->updateFieldValue([
                                'status' => 1
                            ], $id);
                        endforeach;
                    } elseif ($data['Admin']['options'] == 'Inactive') {
                        foreach ($data['id'] as $id):
                            $this->__cmsPagesService->updateFieldValue([
                                'status' => 0
                            ], $id);
                        endforeach;
                    }

                } else {

                    return redirect('/cmspages')->with('error', __('labels.INVALID_PAGE_SELECTION'));
                }

            } else {
                return redirect('/cmspages')->with('error', __('labels.INVALID_PAGE_SELECTION'));
            }

            return redirect('/cmspages')->with('success', __('labels.PAGE_ACTION_SUCCESS', ['action' => $data['Admin']['options']]));

        } else {

            return redirect('/cmspages')->with('error', __('labels.ACCESS_DENIED', ['action' => $data['Admin']['options']]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->authorize('isAdmin')) {

            $this->__cmsPagesService->delete($id);
            return redirect('/cmspages')->with('success', __('labels.PAGE_ACTION_SUCCESS', ['action' => 'deleted']));
        } else {
            return redirect('/cmspages')->with('error', __('labels.ACCESS_DENIED', ['action' => 'deleted']));
        }

    }
}
