<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Nadrus\Deployment;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /**
     * @var UserService
     */
    private $__userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->__userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\request('searchval')){

            $searchValue = \request('searchval');
            $users = $this->__userService->getUsersBySearchValue($searchValue);
        } else {

            $users = $this->__userService->getAll();
        }
        $roles = [];//  Auth::user()->roles->pluck('slug')->toArray();

        return view('users.index', [
            'users' => $users,
            'currentUserRoles' => $roles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = [
            'name' => $request->get('first_name'). ' ' . $request->get('last_name'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];

        $this->__userService->save($user);
        return redirect('/users')->with('success','User created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     */
    public function updateAction(Request $request)
    {
        $data = $request->get('data');
        if ( isset($data['Admin']['options']) ) {
            if (isset($data['id']) && count($data['id']) > 0) {
                if ($data['Admin']['options'] == 'Delete') {
                    $this->__userService->deleteUsers($data['id']);
                } elseif ($data['Admin']['options'] == 'Active') {
                    foreach ($data['id'] as $id):
                        $this->revoke($id);
                    endforeach;
                } elseif ($data['Admin']['options'] == 'Inactive') {
                    foreach ($data['id'] as $id):
                        $this->inactive($id);
                    endforeach;
                }
            } else {
                return redirect('/users')->with('error',__('labels.INVALID_USER_SELECTION'));
            }

        } else {
            return redirect('/users')->with('error',__('labels.INVALID_USER_SELECTION'));
        }

        return redirect('/users')->with('success',__('labels.USER_ACTION_SUCCESS', ['action' => $data['Admin']['options']]));
    }

    /**
     * @param $id
     */
    public function inactive($id)
    {
        if(!empty($id)){
            $user = $this->__userService->getUserById($id);
            return $user->ban([
                'expired_at' => '+1 month',
                'comment' => 'Ban User '.$user->name
            ]);
        }
    }

    /**
     * @param $id
     */
    public function revoke($id)
    {
        if(!empty($id)){
            $user = $this->__userService->getUserById($id);
            return $user->unban();
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword(){

        dd('change dd');
        return view('users.changepassword');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginAsAdmin($id)
    {
        if (auth()->user()) {

            if (auth()->loginUsingId($id)) {

                return redirect()->route('home')->with('success', 'Login successfully!');
            } else {
                return redirect()->route('users.index')->with('error', 'This User is inactve!');
            }
        }
        return redirect()->route('login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearCache()
    {
        Artisan::call('cache:clear');

        if(function_exists('apc_clear_cache')){
            apc_clear_cache();
            apc_clear_cache('user');
        }
        return back()->with('success', 'Cache has been cleared successfully.');
    }

    public function test()
    {
        $deployment = Deployment::where('id',1)->first();
        Deployment\Redis::populateFor($deployment);
        dd(Deployment\Redis::getConfigFor($deployment));

        Redis::set('name',json_encode(['bla'=>'yes']));
        $bla =Redis::get('name');
        dd(json_deecode($bla));
    }

    public function migratefile($file,$deployment = NULL){
        if(!empty($file)){
            $commandPath = app_path("../../app/Plugin/Nadrus/bin/console");
            $command ="bash ". $commandPath . " migrate:file $file $deployment";
            exec($command.' 2>&1', $outputAndErrors, $return_value);
        }else{
            $files = [];
            $directory = app_path("../../app/Plugin/Nadrus/lib/migration");
            $dir = new \FilesystemIterator($directory);

            foreach ($dir as $fileinfo) {
                if($fileinfo->isDir() || !(strpos($fileinfo->getFilename(),'.php'))) continue;
                $files[rtrim($fileinfo->getFilename(),'.php')] = $fileinfo->getMTime();
            }
            //rsort will sort in reverse order
            arsort($files);
        }

        return view('users.migratefile', [
            'migrationFiles' => isset($files) ? $files : null,
            'command' => isset($command) ? $command : '',
            'outputAndErrors' => isset($outputAndErrors) ? $outputAndErrors : '',
            'return_value' => isset($return_value) ? $return_value : '',
            ]);
    }
}
