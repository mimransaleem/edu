<?php

namespace App\Http\Controllers;

use App\Services\FeatureService;
use App\Services\FeatureSettingService;
use Illuminate\Http\Request;

class FeatureSettingsController extends Controller
{
    /**
     * @var FeatureService
     */
    private $__featureSettingService;

    public function __construct(FeatureSettingService $featureSettingService)
    {
        $this->__featureSettingService = $featureSettingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featureSettings = $this->__featureSettingService->getPage(10);
        $featureSettings->withPath('feature-settings');
        return view('features.settings.index', [
            'featureSettings' => $featureSettings,
            'page' => request()->query('page')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id int
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $featureSetting = $this->__featureSettingService->get($id);
        $page = request()->query('page');
        return view('features.settings.edit', [
            'id' => $id,
            'featureSetting' => $featureSetting,
            'page' => $page
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $page = request()->query('page');
        unset($data['_token']);
        unset($data['_method']);
        $this->__featureSettingService->update($data, $id);

        $redirectTo = $page ? '/feature-settings?page='.$page : '/feature-settings';
        return redirect($redirectTo)->with('success','Feature setting updated successfully!');
    }
}
