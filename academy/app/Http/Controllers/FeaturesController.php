<?php

namespace App\Http\Controllers;

use App\Services\FeatureService;
use Illuminate\Http\Request;
use Nadrus\Feature;

class FeaturesController extends Controller
{
    /**
     * @var FeatureService
     */
    private $__featureService;

    public function __construct(FeatureService $featureService)
    {

        $this->__featureService = $featureService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = $this->__featureService->getPage(10);
        $features->withPath('features');
//        dd($features);
        return view('features.index', [
            'features' => $features,
            'page' => request()->query('page')
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Feature $feature
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Feature $feature)
    {
        $page = request()->query('page');
        return view('features.edit', [
            'id' => $feature->id,
            'feature' => $feature,
            'page' => $page
        ]);
    }

    /**
     * @param Feature $feature
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings(Feature $feature)
    {
        return view('features.settings', [
            'id' => $feature->id,
            'feature' => $feature,
            'settings' => $feature->settings,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feature = $request->all();
        $page = request()->query('page');
        unset($feature['_token']);
        unset($feature['_method']);
        $this->__featureService->update($feature, $id);
//        return redirect()->back()->with('success','Feature updated successfully!');
        $redirectTo = $page ? '/features?page='.$page : '/features';
        return redirect($redirectTo)->with('success','Feature updated successfully!');
    }

    public function updateSetting(Request $request, $id)
    {
        $feature = $request->all();

        $this->__featureService->updateSetting($feature, $id);
        return redirect()->route('features.settings', $id)->with('success','Feature setting updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
