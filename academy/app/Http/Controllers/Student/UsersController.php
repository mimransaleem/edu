<?php
/**
 * Created by PhpStorm.
 * User: contrive
 * Date: 11/8/19
 * Time: 8:39 PM
 */

namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use App\Nadrus\Services\UserService;

class UsersController extends Controller
{
    /**
     * @var UserService
     */
    private $__userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {

        $this->__userService = $userService;
    }

    public function index() {

        $users = $this->__userService->getAllStudents();
        return view('student.users.index', [
            'users' => $users,
        ]);
    }
}
