<?php
/**
 * Created by PhpStorm.
 * User: Rohan
 * Date: 7/25/2019
 * Time: 3:12 PM
 */

namespace App\Services;


use Nadrus\FileUpload\UploadableFile;

class UploadFileService
{

    public function getEndPoint()
    {
        return '/uploadFile';
    }

    public function getFileServerUrl(){
        return 'https://cdn.intajy.com/api';
    }

    public function getParams(UploadableFile $uploadableFile)
    {
        return [
            'deployment_id' => 'test_server',
            'deployment_token' => 'test12345678',
            'url'           => $uploadableFile->path,
            'upload_to_s3'    => $uploadableFile->uploadToS3
        ];

    }

    /**
     * @param $filePath
     * @param $distPath
     * @return bool
     * @throws \Exception
     */
    public static function uploadDirectFile($filePath, $distPath)
    {
        if(!file_exists($filePath))
            throw new \Exception('file not found to upload!');

        $uploadableFile = new UploadableFile($filePath);
        $uploadableFile->parameters = $filePath;
        $uploadableFile->path = $distPath;
        $uploadableFile->uploadToS3 = true;

        $instance = new static();
        $response = $instance->hitService($uploadableFile);
        $response = json_decode($response);

        $fileUrl = ($response->status_code == 200) ? $response->path  : false;
        return $fileUrl;
    }

    /**
     * @param UploadableFile $uploadableFile
     * @return bool|string
     */
    public function hitService(UploadableFile $uploadableFile){
        $endPointUrl = $this->getEndPoint();
        $url =  $this->getFileServerUrl() . $endPointUrl;
        $data = $this->getParams($uploadableFile);

        // file to upload
        $files = file_get_contents($uploadableFile->parameters);
        $fileName = basename($uploadableFile->parameters);
        $curl = curl_init();

        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;

        $post_data = $this->build_data_files($boundary, $data, $files, $fileName);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 1000,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)
            ),
        ));

        $response = curl_exec($curl);
        return $response;
    }

    function build_data_files($boundary, $fields, $files, $fileName){
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }

        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="upload"; filename="' .$fileName. $eol
            . 'Content-Transfer-Encoding: binary'.$eol
        ;

        $data .= $eol;
        $data .= $files . $eol;

        $data .= "--" . $delimiter . "--".$eol;

        return $data;
    }

}