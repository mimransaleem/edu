<?php

namespace App\Services;


use Illuminate\Support\Facades\Validator;
use Nadrus\Feature\Setting;

/**
 * Class FeatureSettingService
 * @package App\Services
 */
class FeatureSettingService
{
    /**
     * @var Setting
     */
    private $__featureSetting;

    /**
     * FeatureSettingService constructor.
     * @param Setting $featureSetting
     */
    public function __construct(Setting $featureSetting)
    {
        $this->__featureSetting = $featureSetting;
    }

    public function get($id)
    {
        return $this->__featureSetting->find($id);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->__featureSetting->get();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getPage($limit=15)
    {
        return $this->__featureSetting->paginate($limit);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($params, $id)
    {
        $canView  = isset($params['can_view']) ? '1' : '0';
        $canEdit  = isset($params['can_edit']) ? '1' : '0';
        $isHidden = isset($params['is_hidden_for_customer']) ? '1' : '0';

        return $this->__featureSetting->where('id',$id)->update([
            'can_view' => $canView,
            'can_edit' => $canEdit,
            'is_hidden_for_customer' => $isHidden
        ]);
    }
}
