<?php
/**
 * Created by PhpStorm.
 * User: Rohan Jalil
 * Date: 7/17/2019
 * Time: 10:47 AM
 */

namespace App\Services;


use App\Language;
use Illuminate\Support\Facades\Validator;
use Nadrus\Email;
use Nadrus\Email\Custom;

/**
 * Class EmailTemplateService
 * @package App\Services
 */
class EmailTemplateService
{
    /**
     * @var EmailTemplate
     */
    private $__email;
    /**
     * @var Custom
     */
    private $__custom;

    /**
     * EmailTemplateService constructor.
     * @param EmailTemplate $email
     */
    public function __construct(Email $email, Custom $__custom)
    {
        $this->__email = $email;
        $this->__custom = $__custom;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->__email->find($id);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->__email->get();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getPage($limit=15)
    {
        return $this->__email->paginate($limit);
    }

    public function getCustomPage($limit=15)
    {
        return $this->__custom->paginate($limit);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function save($params)
    {
        Validator::make($params,[
            'lang'          => ['required'],
            'template_id'   => ['required'],
            'from'          => ['required'],
            'subject'       => ['required'],
            'body'          => ['required']
        ])->validate();

        return $this->__email->create($params);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($params, $id)
    {
        $validated = Validator::make($params,[
            'from'          => ['required'],
            'subject'       => ['required'],
            'body'          => ['required']
        ])->validate();

        return $this->__email->where('id',$id)->update($validated);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function updateSetting($params, $id)
    {
        Validator::make($params,[
            'id' => ['required'],
        ])->validate();

        $canView  = isset($params['can_view']) ? '1' : '0';
        $canEdit  = isset($params['can_edit']) ? '1' : '0';
        $isHidden = isset($params['is_hidden_for_customer']) ? '1' : '0';

        $email = $this->__email->find($id);
        return $email->settings()->where('id', '=',$params['id'])
            ->update([
                'can_view' => $canView,
                'can_edit' => $canEdit,
                'is_hidden_for_customer' => $isHidden
            ]);
    }

    public function getLanguages(){
        return Language::all()->where('status', '1');
    }
}
