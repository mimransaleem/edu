<?php
/**
 * Created by PhpStorm.
 * User: Rohan Jalil
 * Date: 7/17/2019
 * Time: 10:47 AM
 */

namespace App\Services;


use Illuminate\Support\Facades\Validator;
use Nadrus\Feature;

/**
 * Class FeatureService
 * @package App\Services
 */
class FeatureService
{
    /**
     * @var Feature
     */
    private $__feature;

    /**
     * FeatureService constructor.
     * @param Feature $feature
     */
    public function __construct(Feature $feature)
    {
        $this->__feature = $feature;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->__feature->find($id);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->__feature->get();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getPage($limit=15)
    {
        return $this->__feature->paginate($limit);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function update($params, $id)
    {
        $validated = Validator::make($params,[
            'name'         => ['required', 'string', 'max:255'],
            'description'    => ['max:500'],
        ])->validate();

        return $this->__feature->where('id',$id)->update($validated);
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    public function updateSetting($params, $id)
    {
        Validator::make($params,[
            'id' => ['required'],
        ])->validate();

        $canView  = isset($params['can_view']) ? '1' : '0';
        $canEdit  = isset($params['can_edit']) ? '1' : '0';
        $isHidden = isset($params['is_hidden_for_customer']) ? '1' : '0';

        $feature = $this->__feature->find($id);
        return $feature->settings()->where('id', '=',$params['id'])
            ->update([
                'can_view' => $canView,
                'can_edit' => $canEdit,
                'is_hidden_for_customer' => $isHidden
            ]);
    }
}