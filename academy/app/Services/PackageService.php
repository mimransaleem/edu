<?php

namespace App\Services;


use Illuminate\Support\Facades\Validator;
use Nadrus\Feature;
use Nadrus\Package;

class PackageService
{
    /**
     * @var Package
     */
    private $package;

    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    public function getAll()
    {
        return $this->package->get();
    }

    public function getById($id)
    {
        return $this->package->find($id);
    }

    public function getAllBundles()
    {
        return $this->package->where('is_bundle', 1)->get();
    }

    public function save($params)
    {
        Validator::make($params,[
            'name'           => ['required', 'string', 'max:256'],
            'description'    => ['max:7168'],
            'package_text'   => ['max:7168'],
            'price'          => ['required']
        ])->validate();
        $params['is_bundle'] = isset($params['is_bundle']) ? '1' : '0';

        return $this->package->create($params);
    }

    public function update($params,$id)
    {
        Validator::make($params,[
            'name'           => ['required', 'string', 'max:256'],
            'description'    => ['max:7168'],
            'package_text'   => ['max:7168'],
            'price'          => ['required']
        ])->validate();
        $params['status'] = isset($params['status']) ? '1' : '0';
        $params['is_bundle'] = isset($params['is_bundle']) ? '1' : '0';
        return $this->package->where('id',$id)->update($params);
    }

    public function delete($id)
    {
        $this->package->find($id)
            ->delete();
    }

    public function addPackageFeatures($params){
        Validator::make($params,[
            'package'     => 'required',
            'features'    => 'required_without:bundle_package_id',
            'bundle_package_id' => 'required_without:features',
        ])->validate();

        $package = $this->package->find($params['package']);

        $bundleId = $params['bundle_package_id'];
        if (!empty($bundleId) && $bundleId != 0) {

            $this->package->where('id',$params['package'])
                ->update(['bundle_package_id' => $bundleId]);

            $packageBundle = $this->package->find($bundleId);
            $packageBundleFeatures = $packageBundle->features;
            $featuresArr = [];
            foreach ($packageBundleFeatures as $packageBundleFeature) {
                $featuresArr[] = [
                    'feature_id' => $packageBundleFeature['id'],
                    'package_bundle' => $bundleId
                ];
            }
            $package->features()->syncWithoutDetaching($featuresArr);
        }

        if(isset($params['features'])) {
            $package->features()->syncWithoutDetaching($params['features']);
        }

        return $package;
    }

    public function removePackageFeature($package, $feature){
        if (is_object($package) && is_object($feature))
            return $package->features()->detach($feature);

        return false;
    }
}
