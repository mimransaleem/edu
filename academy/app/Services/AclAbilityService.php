<?php

namespace App\Services;


use App\Repository\AclAbilityRepository;

class AclAbilityService
{
    protected $__aclAbilityRepository;

    public function __construct(AclAbilityRepository $aclAbilityRepository)
    {
        $this->__aclAbilityRepository = $aclAbilityRepository;
    }

    public function getAll()
    {
        return $this->__aclAbilityRepository->getAll();
    }

}
