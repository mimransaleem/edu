<?php

namespace App\Services;


use App\CmsPage;
use App\Language;
use Illuminate\Support\Facades\Validator;

class CmsPageService
{
    private $cmsPage;

    public function __construct(CmsPage $cmsPage)
    {
        $this->cmsPage = $cmsPage;
    }

    public function getAll()
    {
//        $this->cmsPage::onlyTrashed()->restore();
        return $this->cmsPage::sortable()->get();
    }

    public function getBySearchValue($searchVal)
    {
        return $this->cmsPage->where('name', 'like', "%$searchVal%")
            ->orWhere('content', 'like', "%$searchVal%")
            ->get();
    }

    public function getById($id)
    {
        return $this->cmsPage->find($id);
    }

    public function getAllActive()
    {
        return $this->cmsPage->where('status', 1)->get();
    }

    public function save($params)
    {
        Validator::make($params,[
            'name'       => ['required', 'string', 'max:256'],
            'content'    => ['required'],
        ])->validate();
        $params['status'] = isset($params['status']) ? '1' : '0';
        $params['showinfooter'] = isset($params['showinfooter']) ? '1' : '0';
        $params['showinleft'] = isset($params['showinleft']) ? '1' : '0';

        return $this->cmsPage->create($params);
    }

    public function update($params,$id)
    {
        Validator::make($params,[
            'name'           => ['required', 'string', 'max:256'],
            'content'    => ['required'],
        ])->validate();
        $params['status'] = isset($params['status']) ? '1' : '0';
        $params['showinfooter'] = isset($params['showinfooter']) ? '1' : '0';
        $params['showinleft'] = isset($params['showinleft']) ? '1' : '0';
        return $this->cmsPage->where('id',$id)->update($params);
    }

    public function updateFieldValue($params,$id) {
        return $this->cmsPage->where('id',$id)->update($params);
    }

    public function delete($id)
    {
        $this->cmsPage->find($id)
            ->delete();
    }

    public function getLanguages(){
        return Language::all()->where('status', '1');
    }
}
