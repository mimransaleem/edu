<?php

namespace App\Services;


use App\Country;
use App\DeploymentUser;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DeploymentUserService
{
    /**
     * @var DeploymentUser
     */
    private $deploymentUser;
    /**
     * @var Country
     */
    private $country;

    public function __construct(DeploymentUser $deploymentUser, Country $country)
    {
        $this->deploymentUser = $deploymentUser;
        $this->country = $country;
    }

    public function getById($id)
    {
        return $this->deploymentUser->find($id);
    }

    public function saveSignUpForm($params)
    {
        Validator::make($params,[
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            'email'      => ['required', 'email', 'max:191', 'unique:deployment_users'],
            'password'   => ['required', 'min:8']
        ])->validate();

        return $this->deploymentUser->create($params);
    }

    public function update($params,$id,$step)
    {
        if ($step == 2) {

            Validator::make($params, [
                'institute_name' => ['required', 'max:255'],
                'description'   => ['max:255'],
                'sub_domain'    => ['required', 'max:255', 'unique:deployment_users'],
                'logo'          => ['required']
            ])->validate();

            if(request()->hasFile('logo')) {

                $file = request()->file('logo');
                $fileName = time() . '_' . str_replace(' ','_',$file->getClientOriginalName());
                $filePath= 'deploymentWizard'. DS .$id;

                Storage::putFileAs($filePath, $file, $fileName);
                $params['logo'] = 'app'. DS . $filePath . DS . $fileName;

//                Storage::disk('public')->putFileAs($filePath, $file, $fileName);

            }

        } elseif ($step == 3) {

            Validator::make($params, [
                'learning_content_type' => ['required'],
                'country_id' => ['required'],
            ])->validate();

        } elseif ($step == 4) {

            Validator::make($params, [
//                'platform_type' => ['required'],
                'package_id'    => ['required'],
            ])->validate();
            $params['platform_type'] = isset($params['platform_type']) ? 'public' : 'private';
        }

        return $this->deploymentUser->where('id',$id)->update($params);
    }

    public function getCountriesList(){

        return $this->country->get();
    }

}
