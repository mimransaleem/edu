<?php

namespace App\Services;


use App\Repository\AclRoleRepository;
use Illuminate\Support\Facades\Validator;

class AclRoleService
{
    protected $__aclRoleRepository;

    public function __construct(AclRoleRepository $aclRoleRepository)
    {
        $this->__aclRoleRepository = $aclRoleRepository;
    }

    public function getAll()
    {
        return $this->__aclRoleRepository->getAll();
    }

    public function create($params)
    {
        return $this->__aclRoleRepository->create($params);
    }

    public function editRole($params)
    {
        return $this->__aclRoleRepository->editRole($params);
    }

    public function removeRole($params)
    {
        return $this->__aclRoleRepository->removeRole($params['id']);
    }

    public function addUserNewRole($params){

        Validator::make($params,[
            'role'    => 'required|max:100|min:3',
            'adminId' => 'required|numeric'
        ])->validate();

        $user = app(UserService::class)->getUserById($params['adminId']);
        return $this->__aclRoleRepository->addUserRole($user, $params['role']);
    }

    public function removeUserRole($params)
    {
        Validator::make($params,[
            'role'    => 'required|max:100|min:3',
            'adminId' => 'required|numeric'
        ])->validate();

        $user = app(UserService::class)->getUserById($params['adminId']);
        return $this->__aclRoleRepository->removeUserRole($user, $params['role']);
    }

    public function addRoleAbilities($params){
        Validator::make($params,[
            'role'      => 'required|max:100|min:3',
            'abilities' => 'required'
        ])->validate();

        return $this->__aclRoleRepository->addRoleAbilities($params);
    }

    public function removeRoleAbility($params){
        Validator::make($params,[
            'roleName'    => 'required',
            'abilityName' => 'required'
        ])->validate();

        return $this->__aclRoleRepository->removeRoleAbility($params);
    }
}
