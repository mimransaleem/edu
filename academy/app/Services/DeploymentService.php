<?php

namespace App\Services;


use Illuminate\Support\Facades\Validator;
use Nadrus\Deployment;
use Nadrus\Package;
use Nadrus\Support\Facade;

class DeploymentService extends AppService
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|Deployment[]
     */
    public function getAll()
    {
        return Deployment::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return Deployment::find($id);
    }

    /**
     * @param $domain
     * @return mixed
     */
    public function getByDomain($domain)
    {
        return Deployment::where('domain', $domain)->first();
    }

    /**
     * @param $deploymentAtrr
     * @return bool
     */
    public function create($deploymentAtrr)
    {
        Validator::make($deploymentAtrr,[
            'name'        => ['required', 'max:255'],
            'domain'      => ['required', 'unique:deployments'],
            'package_id'  => ['required'],
        ])->validate();

        $packageFeatures = Package::find($deploymentAtrr['package_id'])->features;
        $deployment = Deployment::create($deploymentAtrr);
        foreach ($packageFeatures as $packageFeature)
        {
            $deploymentFeature = [
                'feature_id' => $packageFeature->id,
                'status' => true
            ];
            $deployment->states()->create($deploymentFeature);
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function activate($id)
    {
       $deployment =  Deployment::find($id);
       if($deployment->status) return false; // deployment already was activated
       $deployment->status = 1;
       $deployment->save();
       return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deactivate($id)
    {
       $deployment =  Deployment::find($id);
       if(!$deployment->status) return false; // deployment already was deactivated
       $deployment->status = 0;
       $deployment->save();
       return true;

    }

    /**
     * @param $deploymentAtrr
     * @return mixed
     */
    public function update($deploymentAtrr)
    {

        $deployment =  Deployment::find($deploymentAtrr['id']);
        $deployment->name = $deploymentAtrr['name'];
        $deployment->domain = $deploymentAtrr['domain'];
        $deployment->type = $deploymentAtrr['type'];

        return $deployment->save();
    }

    /**
     * @param $configs
     * @param $deploymentId
     * @param $featureId
     * @return mixed
     * @throws \Exception
     */
    public function updateConfig($configs,$deploymentId,$featureId){

        $deployment = $this->get($deploymentId);
        $configsOfFeature = $deployment->getConfigsOfFeature($featureId);
        foreach ($configsOfFeature as $feature) {
            $settingId = $feature['id'];

            if($feature['type'] == 'file') {
                if(request()->hasFile('data.Configurations.'.$settingId)) {

                    $file = request()->file('data.Configurations.' . $settingId);
                    $fileName = time() . '_' . str_replace(' ','_',$file->getClientOriginalName());

                    $tempFilePath = $file->getPath(). DS .$file->getBasename();
                    $filePath= $file->getPath(). DS . $fileName;
                    move_uploaded_file($tempFilePath,$filePath);

                    $fileServerPath = UploadFileService::uploadDirectFile($filePath,
                        Facade::getBaseUploadFolder($deploymentId, $deployment->domain) .'features'. DS . $featureId. DS);

                    $value = $fileServerPath ? $fileServerPath : '';
                } else {
                    continue;
                }

            } else {
                $value = isset($configs[$settingId]) ? $configs[$settingId] : '0';
            }
            $deployment->configurations()
                ->updateOrCreate(
                    ['feature_setting_id' => $settingId],
                    [
                        'feature_setting_id' => $settingId,
                        'value' => $value
                    ]
                );
        }
        return $deployment;
    }

    public function resetConfig($deploymentId,$featureId)
    {
        $deployment = $this->get($deploymentId);
        $featureSettingId = request()->get('reset_config');
        $configs = $deployment->getConfigsOfFeature($featureId);
        $featureConfig = Deployment\Configuration::where('feature_setting_id', '=', $featureSettingId)->where('deployment_id', '=', $deployment->id)->get()->first();
        $response = false;
        if ($featureConfig) {
            foreach ($configs as $config) {
                if ($config->id == $featureSettingId) {
                    $default_value = $config->has_default_value ? $config->default_value : "";

                    $deployment->configurations()
                        ->updateOrCreate(
                            ['feature_setting_id' => $featureSettingId],
                            [
                                'feature_setting_id' => $featureSettingId,
                                'value' => $default_value
                            ]
                        );
                    $response = true;
                    break;
                }
            }
        }
        return $response ? $deployment : $response;
    }

    public function featureBind($deployment_id,$feature_id)
    {
        $deployment =  Deployment::find($deployment_id);
        return $deployment->bindFeature($feature_id);

    }

    public function featureUnbind($deployment_id,$feature_id)
    {
        $deployment =  Deployment::find($deployment_id);
        return $deployment->unBindFeature($feature_id);

    }


    public function featureActivate($deployment_id,$feature_id)
    {
        $deployment =  Deployment::find($deployment_id);
        return $deployment->activateState($feature_id);

    }

    public function featureDeactivate($deployment_id,$feature_id)
    {
        $deployment =  Deployment::find($deployment_id);
        return $deployment->deactivateState($feature_id);

    }



}
