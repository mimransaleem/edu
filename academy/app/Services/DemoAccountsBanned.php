<?php

namespace App\Services;


use App\Repository\UserRepository;

class DemoAccountsBanned
{
    /**
     * @var UserService
     */
    private $userRepository;

    public function __construct()
    {

        $this->userRepository = new UserRepository();
    }

    public function __invoke()
    {

        $demoUsers = $this->userRepository->getUsersWithRole('demo_admin');

        foreach ($demoUsers as $id => $demoUser) {
            $days = date('d', time() - strtotime($demoUser->created_at));
            echo $demoUser->name . ' created at ' .$demoUser->created_at . ' and days:' .$days . PHP_EOL;
            if ($days > TRIAL_ACCOUNT_PERIOD) {
                $demoUser->ban([
                    'expired_at' => '+1 year',
                    'comment' => 'Demo Account Deactivate '.$demoUser->name
                ]);
            }
        }
        echo 'Account Deactivate Successfully!'.PHP_EOL;
    }

}
