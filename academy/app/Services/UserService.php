<?php

namespace App\Services;

use App\Repository\UserRepository;
use Illuminate\Support\Facades\Validator;

class UserService
{

    /**
     * @var UserRepository
     */
    protected $__userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->__userRepository = $userRepository;
    }

    /**
     * @return UserRepository[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll(){
        return $this->__userRepository->get();
    }

    public function getUserById($id){
        return $this->__userRepository->getUserById($id);
    }

    public function getUsersBySearchValue($value){
        return $this->__userRepository->getUsersBySearchValue($value);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function save(array $params){

        Validator::make($params,[
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ])->validate();

        return $this->__userRepository->save($params);
    }

    /**
     * @param $userIds
     * @return bool
     */
    public function deleteUsers($userIds)
    {
        if (is_array($userIds)) {
            foreach ($userIds as $userId) {
                $this->__userRepository->deleteUser($userId);
            }
        } else if (is_numeric($userIds)) {
            $this->__userRepository->deleteUser($userIds);
        }

        return true;
    }

}