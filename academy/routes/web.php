<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
})->name('base_url');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/post-login', 'Auth\LoginController@postLogin')->name('post-login');

Route::get('/cmspages', 'CmsPagesController@index')->name('cmspages.index');
Route::get('/cmspages/add', 'CmsPagesController@create')->name('cmspages.add');
Route::post('/cmspages/store', 'CmsPagesController@store')->name('cmspages.store');
Route::get('/cmspages/edit/{id}', 'CmsPagesController@edit')->name('cmspages.edit');
Route::patch('/cmspages/update/{id}', 'CmsPagesController@update')->name('cmspages.update');
Route::post('/cmspages/update', 'CmsPagesController@updateAction')->name('cmspages.update.action');

Route::resources(['users' => 'UsersController']);
Route::get('/login-as-user/{id}', 'UsersController@loginAsAdmin')->name('login-as-user');
Route::get('/clear-cache', 'UsersController@clearCache')->name('clear-cache');
Route::get('/users/change-password', 'UsersController@changePassword')->name('change-password');

Route::middleware('auth')->group(function() {

    Route::post('/users/update', 'UsersController@updateAction');

    Route::resources(['acl' => 'AclController']);

    Route::resources(['packages' => 'PackagesController']);
    Route::post('/packages/update', 'PackagesController@updateAction');
    Route::post('/packages/addPackageFeatures', 'PackagesController@addPackageFeatures')->name('add-package-features');
    Route::post('/packages/{package}/removePackageFeature/{feature}', 'PackagesController@removePackageFeature')->name('remove-package-feature');

    Route::get('/student/users', 'Student\UsersController@index')->name('student.users');
});
