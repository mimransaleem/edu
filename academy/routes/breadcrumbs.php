<?php
/**
 * Created by PhpStorm.
 * User: Rohan
 * Date: 7/18/2019
 * Time: 3:12 PM
 */
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > ACL Manage
Breadcrumbs::for('acl-manage', function ($trail) {
    $trail->parent('home');
    $trail->push('ACL Manage', route('acl-manage'));
});

// Home > Users
Breadcrumbs::for('users.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Users', route('users.index'));
});
// Home > Users > Create
Breadcrumbs::for('users.create', function ($trail) {
    $trail->parent('users.index');
    $trail->push('Create', route('users.create'));
});

Breadcrumbs::for('change-password', function ($trail) {
    $trail->parent('users.index');
    $trail->push('Change Password', route('change-password'));
});
// Home > Users > Edit
Breadcrumbs::for('users.edit', function ($trail,$package) {
    $trail->parent('users.index');
    $trail->push('Edit', route('users.edit',$package));
});
// Home > Users > Change Password
Breadcrumbs::for('password.reset', function ($trail,$token) {
    $trail->parent('users.index');
    $trail->push('Change Password', route('password.reset',$token));
});

// Home > Packages
Breadcrumbs::for('packages.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Packages', route('packages.index'));
});
// Home > Packages > Create
Breadcrumbs::for('packages.create', function ($trail) {
    $trail->parent('packages.index');
    $trail->push('Create', route('packages.create'));
});
// Home > Packages > Edit
Breadcrumbs::for('packages.edit', function ($trail,$package) {
    $trail->parent('packages.index');
    $trail->push('Edit', route('packages.edit',$package));
});

// Home > Features
Breadcrumbs::for('features.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Features', route('features.index'));
});
// Home > Features > Create
Breadcrumbs::for('features.create', function ($trail) {
    $trail->parent('features.index');
    $trail->push('Create', route('features.create'));
});
// Home > Features > Edit
Breadcrumbs::for('features.edit', function ($trail, $id) {
    $trail->parent('features.index');
    $trail->push('Edit', route('features.edit', $id));
});
// Home > Features > Settings
Breadcrumbs::for('features.settings', function ($trail, $id) {
    $trail->parent('features.index');
    $trail->push('Settings', route('features.settings', $id));
});

// Home > Feature Settings
Breadcrumbs::for('feature-settings.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Feature Settings', route('feature-settings.index'));
});
// Home > Feature Settings > Edit
Breadcrumbs::for('feature-settings.edit', function ($trail, $id) {
    $trail->parent('feature-settings.index');
    $trail->push('Edit', route('feature-settings.edit', $id));
});

// Home > Deployments
Breadcrumbs::for('deployments.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Deployments', route('deployments.index'));
});
// Home > Deployments > Create
Breadcrumbs::for('deployments.create', function ($trail) {
    $trail->parent('deployments.index');
    $trail->push('Create', route('deployments.create'));
});
// Home > Deployments > Edit
Breadcrumbs::for('deployments.edit', function ($trail, $id) {
    $trail->parent('deployments.index');
    $trail->push('Edit', route('deployments.edit', $id));
});
// Home > Deployments > Features
Breadcrumbs::for('deployments.features', function ($trail, $id) {
    $trail->parent('deployments.index');
    $trail->push('Features', route('deployments.features', $id));
});

// Home > Deployments > Features > Config
Breadcrumbs::for('deployments.config', function ($trail, $id, $featureId) {
    $trail->parent('deployments.features', $id);
    $trail->push('Config', route('deployments.config', [$id, $featureId]));
});

// Home > Settings > Static Pages
Breadcrumbs::for('cmspages.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Static Pages', route('cmspages.index'));
});

// Home > Static Pages > Create
Breadcrumbs::for('cmspages.add', function ($trail) {
    $trail->parent('cmspages.index');
    $trail->push('New', route('cmspages.add'));
});

// Home > Static Pages > Edit
Breadcrumbs::for('cmspages.edit', function ($trail,$id) {
    $trail->parent('cmspages.index');
    $trail->push('Edit', route('cmspages.edit',$id));
});
